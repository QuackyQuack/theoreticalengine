#include "CButton.h"
#include "CGameClass.h"
#include "CTextLabel.h"

CButton::CButton()
{
}

CButton::CButton(CShapes* _myShape, CTextLable* _text, std::function<void(int)> _myAction)
{
	myShape = _myShape;
	myAction = _myAction;
	myText = _text;
}

CButton::CButton(std::pair<float, float> centre, float xSize, float ySize, std::string text, std::function<void(int)> _action )
{
	myShape = CGameClass::getInstance().createShapeSquare(centre, xSize, ySize, Shader_TextureOnly, true, "Button1.png");
	CTextLable *textLbl = new CTextLable(text, "Resources/Fonts/Arial.ttf", glm::vec2(centre.first-100, centre.second), glm::vec3(COLOUR_WHITE[0], COLOUR_WHITE[1], COLOUR_WHITE[2]));
	CGameClass::getInstance().addTextLabel(textLbl);
	myText = textLbl;
	myAction = _action;
}

CButton::CButton(std::pair<float, float> centre, float xSize, float ySize, std::string text, std::function<void(int)> _action, std::string ButtonTexture)
{
	myShape = CGameClass::getInstance().createShapeSquare(centre, xSize, ySize, Shader_TextureOnly, true, ButtonTexture);
	CTextLable *textLbl = new CTextLable(text, "Resources/Fonts/Arial.ttf", glm::vec2(centre.first - 100, centre.second), glm::vec3(COLOUR_WHITE[0], COLOUR_WHITE[1], COLOUR_WHITE[2]));
	CGameClass::getInstance().addTextLabel(textLbl);
	myText = textLbl;
	myAction = _action;
}

CButton::~CButton()
{
}

// Function Name   : setMyShape
// Description     : changs the buttons shape
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CButton::setMyShape(CShapes* _myShape)
{
	myShape = _myShape;
}
// Function Name   : getMyShape
// Description     : returns buttons currentshape
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
CShapes* CButton::getMyShape()
{
	return myShape;
}
// Function Name   : setMyAction
// Description     : Takes a function for the gameManager to execute once the button is clicked
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CButton::setMyAction(std::function<void(int)> _myAction)
{
	myAction = _myAction;
}
// Function Name   : getMyFunction
// Description     : returns the buttons function
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
std::function<void(int)> CButton::getMyFunction()
{
	return myAction;
}

void CButton::doAction()
{
	//Problematic
}
// Function Name   : setMyText
// Description     : Set the textlabel for the button
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CButton::setMyText(CTextLable * _myText)
{
	myText = _myText;
}
// Function Name   : getMyText
// Description     : Returns the text label
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
CTextLable * CButton::getMyText()
{
	return myText;
}
// Function Name   : changeMyText
// Description     : Alter the text displayed by the text label
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CButton::changeMyText(std::string _string)
{
	myText->setText(_string);
}
// Function Name   : mouseOnButton
// Description     : Checks if the mouse cursor is currently on the shape of button
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
bool CButton::mouseOnButton(int _x, int _y)
{
	std::vector<std::pair<int, int>> TempHold = myShape->GetCardinalPoints();
	
	if ((TempHold[0].first<_x && TempHold[0].second > _y) && (TempHold[1].first > _x && TempHold[1].second > _y) && (TempHold[2].first > _x && TempHold[2].second < _y) && (TempHold[3].first < _x && TempHold[3].second < _y))
	{
		return(true);
	}

	return false;
}

void CButton::moveButton(float xPos, float yPos)
{
	myShape->setShapePosition(glm::vec3(xPos, yPos, 0));
	myText->setPosition(glm::vec2(xPos, yPos));
}
