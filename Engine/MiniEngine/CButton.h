#include <functional>
//#include "CGameClass.h"

#include "CTextLabel.h"
#include "CShapes.h"

//Foreward decleration
class CGameClass;

#pragma once
class CButton 
{
public:
	CButton();

	CButton(CShapes* _myShape, CTextLable* _text, std::function<void(int)> _myAction);
	CButton(std::pair<float, float> centre, float xSize, float ySize, std::string text, std::function<void(int)> _action);
	CButton(std::pair<float, float> centre, float xSize, float ySize, std::string text, std::function<void(int)> _action,std::string ButtonTexture);
	~CButton();

	void setMyShape(CShapes* _myShape);
	CShapes* getMyShape();


	void setMyAction(std::function<void(int)> _myAction);
	std::function<void(int)> getMyFunction();

	void doAction();
	void setMyText(CTextLable* _myText);
	CTextLable* getMyText();
	void changeMyText(std::string _string);
	bool mouseOnButton(int _x, int _y);
	void moveButton(float xPos, float yPos);


private:
	CShapes* myShape;
	std::function<void(int)> myAction; //Level Changer



	//std::function<

	CTextLable* myText;
};