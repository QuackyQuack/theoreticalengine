//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : CCamera.cpp
// Description     : Class for OpenGL camera's
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//

#include <freeglut.h>

#include "CCamera.h"

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "gtx/rotate_vector.hpp"


CCamera::CCamera()
{
}

// Function Name   : CCamera constructor
// Description     : Creates an orthographic camera
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
CCamera::CCamera( float _near, float _far, bool centrecentre) 
// near - cut off of objects close to camera
// fat - furthest distance an object can be
// centrecentre - if true camera 0,0 is centre of screen, else top left
{
	if (centrecentre)
	{
		projection = glm::ortho(-1.f * (((float)WINDOW_WIDTH) * 0.5f), (((float)WINDOW_WIDTH) * 0.5f), (((float)WINDOW_HIEGHT) * 0.5f) * -1.f, (((float)WINDOW_HIEGHT) * 0.5f), _near, _far);
	}
	else
	{
		projection = glm::ortho(0.0f, (float)WINDOW_WIDTH, 0.0f, (float)WINDOW_HIEGHT, _near, _far);
	}
}


// Function Name   : CCamera constructor
// Description     : Creates an perspective camera
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
CCamera::CCamera(float fov,  float _near, float _far)
// fov - Camera field of view
// near - cut off of objects close to camera
// fat - furthest distance an object can be

{
	projection = glm::perspective(fov, (float)WINDOW_WIDTH / (float)WINDOW_HIEGHT, _near, _far);
}

//Returns camera view matrix
glm::mat4 CCamera::getViewMatr()
{
	return (viewMatr);
}

//Returns camera projection matrix
glm::mat4 CCamera::getProjMatr()
{
	return (projection);
}
//Returns camera position
glm::vec3 CCamera::getCamPos()
{
	return camPos;
}
glm::vec3 CCamera::getCamUp()
{
	return camUpDir;
}
glm::vec3 CCamera::getCamLookDir()
{
	return camLookDir;
}
//Moves the camera
void CCamera::MoveCamera(float x, float y, float z)
{
	camPos[0] += x;
	camPos[1] += y;
	camPos[2] += z;

	viewMatr = glm::lookAt(camPos, camPos + camLookDir, camUpDir);	
}

void CCamera::setCameraPos(float x, float y, float z)
{
	camPos[0] = x;
	camPos[1] = y;
	camPos[2] = z;

	viewMatr = glm::lookAt(camPos, camPos + camLookDir, camUpDir);
}

void CCamera::setCameraPos(glm::vec3 _pos)
{
	camPos[0] = _pos.x;
	camPos[1] = _pos.y;
	camPos[2] = _pos.z;

	viewMatr = glm::lookAt(camPos, camPos + camLookDir, camUpDir);
}

void CCamera::RotateCamera(float x, float y, float z)
{
	pitch += x;
	yaw += y;
	updateCameraAxis();
}

void CCamera::setCameraAxis(float roll, float pitch, float yaw)
{
	this->roll = roll;
	this->pitch = pitch;
	this->yaw = yaw;
	updateCameraAxis();
}

void CCamera::updateCameraAxis()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glm::mat4 matroll = glm::rotate(glm::mat4(1.0), -roll, glm::vec3(0.0f, 0.0f, 1.0f));
	glm::mat4 matpitch = glm::rotate(glm::mat4(1.0), -pitch, glm::vec3(1.0f, 0.0f, 0.0f));
	glm::mat4 matyaw = glm::rotate(glm::mat4(1.0), -yaw, glm::vec3(0.0f, 1.0f, 0.0f));

	glm::mat4 mattranslate = glm::translate(glm::mat4(1.0f), -camPos);

	viewMatr = matroll * matpitch * matyaw * mattranslate;

	glMultMatrixf((float*)glm::value_ptr(viewMatr));

	camLookDir = glm::vec3(viewMatr[0][2], viewMatr[1][2], viewMatr[2][2]);
	camLookDir *= -1;

}

 