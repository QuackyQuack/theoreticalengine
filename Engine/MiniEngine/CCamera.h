

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"

#include "Tools.h"
#pragma once

class CCamera
{
public:
	CCamera();
	CCamera(float near, float far,bool centrecentre); // Create orthographic Camera
	CCamera(float fov,  float near,float far); // Create perspective Camera
	~CCamera() {};

	glm::mat4 getViewMatr(); 
	glm::mat4 getProjMatr();

	glm::vec3 getCamPos();
	glm::vec3 getCamLookDir();
	glm::vec3 getCamUp();

	void MoveCamera(float x, float y, float z); 
	void setCameraPos(float x, float y, float z);
	void setCameraPos(glm::vec3);
	void RotateCamera(float x, float y, float z);
	void setCameraAxis(float roll, float pitch, float yaw);
	//bool TimeTrack(int &move);

private:

	//Applies changes of roll pitch and yaw to the camera view matrix
	void updateCameraAxis();


	//First person camera rotations
	float roll = 0;
	float pitch = 0;
	float yaw = 0;



	glm::vec3 camPos = glm::vec3(0.0f,0.0f,3.0f);
	glm::vec3 camLookDir = glm::vec3(0.0f,0.0f,-1.0f);
	glm::vec3 camUpDir = glm::vec3(0.0f, 1.0f, 0.0f);

	glm::mat4 viewMatr = glm::lookAt(camPos, camPos + camLookDir, camUpDir);

	glm::mat4 projection;

};