//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : CGameClass.cpp
// Description     : Creates object to handle the game loop
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
#include <cmath> 
#include <functional>


#include "ModelLoader.h"
#include "CGameClass.h"
#include "CSound.h"
#include "shapeHandler.h"
#include "CInput.h"

//Components
#include "playerLogic.h"

// GAMELOOP FUNCS
#pragma region GAME_LOOP_FUNCS

CGameClass & CGameClass::getInstance()
{
	static CGameClass myClass;
	return(myClass);
}

CGameClass::~CGameClass()
{

}

// Function Name   : shutdown
// Description     : calls for game class singleton to be deleted
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CGameClass::shutdown()
{

	SafeDestroyAllShapes();
	SafeDestroyAllText();
	SafeDestroyAllButtons();
	SafeDestroyAllModels();
	SafeDestroyAllGameObjects();
	SafeDestroyAllLights();
	//ShaderLoader::getInstance()->ShutDown();
	
	CSound::getInstance()->destroySounds();
	shapeHandler::getInstance().shutDown();


	delete ShaderLoader::getInstance();
	delete CSound::getInstance();
}

// Function Name   : InitializeGame
// Description     : Setup befor entering game loop
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CGameClass::InitializeGame()
{
	//Initialise the game class
	getInstance();
	CSound::getInstance();
	UICamera = CCamera(0.1f, 100.f, true);
	gameCamera = CCamera(45.f, 0.1f,20000.f);

	gameCamera.setCameraAxis(0, 0, 0);
	gameCamera.setCameraPos(0, 0, 0);

	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	glutWarpPointer(WINDOW_WIDTH / 2, WINDOW_HIEGHT / 2);

	CSound::getInstance(); //make sure instance is safe to delete later
	levelLoader(0);
}

// Function Name   : Update
// Description     : Update function
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Called from main
void CGameClass::Update()
{
	//Update time
	int currentTime = (glutGet(GLUT_ELAPSED_TIME));
	static int lastTime = (glutGet(GLUT_ELAPSED_TIME));
	deltaTime = (currentTime - lastTime)/1000.f;
	lastTime = currentTime;


	for (int x = 0; x < activeGameObjects; x++)
	{
		gameObjects[x]->updateComponents();
	}



	CSound::getInstance()->getAudioSys()->update();
	ProcessInput();
}

// Function Name   : ProcessInput
// Description     : Do stuff with the players input
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CGameClass::ProcessInput()
{
	//UPDATE keystates MUsT BE LasT
	CInput::getInstance().Update();
}

#pragma endregion



// Function Name   : Render
// Description     : Render function
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Called from main
void CGameClass::Render()
{
	//Set Culling mode
	//Bind frame buffer here
	/*if (renderFramBuffer)
	{
		TestBuffer.renderFirst();
	}*/

	//World shape rendering
	for (int x = 0; x < activeShapes; x++)
	{
		if (!dynamic_cast<particalSystem*>(ShapeList[x]) || !dynamic_cast<GPUParticleSystem*>(ShapeList[x]))
		{
			ShapeList[x]->Render();
		}


	}
	for (int x = 0; x < activeModels; x++)
	{
		ModelList[x]->Render();
	}

	glDepthFunc(GL_ALWAYS);

	//Text rendering
	for (int x = 0; x < activeTextLabels; x++)
	{
		TextLabels[x]->Render();
	}

	
	//Game object sprite rendering
	/*for (int x = 0; x < activeGameObjects; x++)
	{
		if (gameObjects[x]->getMysprite())
		{
			gameObjects[x]->updateShape();
			glUseProgram(gameObjects[x]->getMysprite()->getMyProgram());                        //Fetch current shapes program

			if (!dynamic_cast<CShapeSquare*>(gameObjects[x]->getMysprite()) && !dynamic_cast<CShapeTriangle*>(gameObjects[x]->getMysprite()) && !dynamic_cast<CShapeLine*>(gameObjects[x]->getMysprite())) //Render 2d objects (used in UI) with orthagraphic camera
			{
				glDepthFunc(GL_LESS);
				gameObjects[x]->getMysprite()->PrepareGraphics(gameCamera);							//Update textures ect..
			}
			else
			{
				glDepthFunc(GL_ALWAYS);
				gameObjects[x]->getMysprite()->PrepareGraphics(UICamera);							//Update textures ect..
			}
		}

		if (!dynamic_cast<particalSystem*>(gameObjects[x]->getMysprite()) || !dynamic_cast<GPUParticleSystem*>(gameObjects[x]->getMysprite()))
		{
			glBindVertexArray(gameObjects[x]->getMysprite()->GetVertexAtrib(CShapes::VAT_VAO)); // Bind vertex
			//Geometry shaders
			if (dynamic_cast<CGeometryObject*>(gameObjects[x]->getMysprite()))
			{
				glDrawArrays(GL_POINTS, 0, 1);
			}
			else
			{
				if (gameObjects[x]->getMysprite()->isTesselated())
				{
					glDrawArrays(GL_PATCHES, 0, gameObjects[x]->getMysprite()->GetIndicesSize());
				}
				else
				{
					glDrawElements(gameObjects[x]->getMysprite()->GetDrawMode(), gameObjects[x]->getMysprite()->GetIndicesSize(), GL_UNSIGNED_INT, 0); // Draw shape
				}
			}
		}

		//Clear and prepair for next draw
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_BLEND);
		glDisable(GL_STENCIL_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
		glBlendEquation(GL_FUNC_ADD);
		glUseProgram(0);
	}*/


//	glDepthFunc(GL_LESS);

/*	if (renderFramBuffer)
	{
		TestBuffer.renderSecond();
	}*/
}

// Function Name   : levelLoader
// Description     : Takes an integer and loads level through switch
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
bool CGameClass::levelLoader(int _levelNum) //Loads levels by placing objects into world
//_levelNum : Level to load
{
	CShapes* cubeShape = createShapeCube(0, 0, -4, 1, 1, 1, Shader_PureVertex, false);
	//cubeShape->setRotationPosition(glm::vec3(1,1,0));
	
	CGameObject* playerObj = new CGameObject(glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1));
	Player* playerComponent = new Player();
	playerObj->addComponent(playerComponent);
	addGameObject(playerObj);

	
	walkableTerrain = new CShapeTerrain(0, 0, 0, 600, 50, 600, "TerrainTextures/Grass_Texture.jpg","TerrainTextures/Rocky_texture.jpg",80,42069);
	//walkableTerrain = new CShapeTerrain(0, 0, 0, "Resources/Images/HeightMaps/ChicagoHeightMap.raw", "maptrexture.png");
	addShape(walkableTerrain);

	std::vector<std::string> fileNameList
	{
			"CubeMap/midnight-silence_rt.png",
			"CubeMap/midnight-silence_lf.png",
			"CubeMap/midnight-silence_up.png",
			"CubeMap/midnight-silence_dn.png",
			"CubeMap/midnight-silence_bk.png",
			"CubeMap/midnight-silence_ft.png",
	};

	CurrentCubemape = new CShapesCubemap(&gameCamera, fileNameList);
	addShape(CurrentCubemape);

	CGeometryObject* grassObj = new CGeometryObject();
	walkableTerrain->generateGeometryOnTerrain(*grassObj, 400);
	grassObj->init(glm::vec3(0, 0, 0), glm::vec3(1, 2, 1), "Resources/ShaderFiles/shad GeometryBasic.vs", "Resources/ShaderFiles/shad GeometryTextured.frg", "Resources/ShaderFiles/shad GeometrySquare.gs", true, "grass.png");

	addShape(grassObj);
	//createShapeCube(0, 0, 0, 100, 100, 100, Shader_PureVertex, false);

	//Once all objects and components are loaded, initialize them
	for (int x = 0; x < activeGameObjects; x++)
	{
		gameObjects[x]->initializeComponents();
	}

	return(true);
}

// Function Name   : Quitter
// Description     : Closes aplication
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Called from main
void CGameClass::Quitter(int _val)
{
	glutLeaveMainLoop();
}

glm::vec3 CGameClass::createRayCastFromScreen(std::pair<float, float> NDCoords, CCamera _camera)
{
	//Convert screen to proj space
	glm::vec4 projCoords = glm::vec4(NDCoords.first, NDCoords.second, -1.0f, 1.0f);

	//Projection to eye space
	glm::mat4 invProjMat = glm::inverse(_camera.getProjMatr());
	glm::vec4 eyeCoords = invProjMat * projCoords;
	eyeCoords = glm::vec4(eyeCoords.x, eyeCoords.y, -1.0f, .0f);

	//eyespace to world space
	glm::mat4 invViewMat = glm::inverse(_camera.getViewMatr());
	glm::vec4 worldRay = invViewMat * eyeCoords;

	glm::vec3 rayCast = glm::normalize(glm::vec3(worldRay));

	return glm::vec3(rayCast);
}

bool CGameClass::shapeOnMousePicker(CShapes * _target, glm::vec3 &intersectionPoint)
{
	// create a raycast from the input class ndcoordinates
	glm::vec3 rayCast = createRayCastFromScreen(CInput::getInstance().getNormalizedDeviceCoordinates(), gameCamera);
	//rayCast.z = abs(rayCast.z);
	// determin which shape is being used
	// Check collision based on shape

	if (dynamic_cast<CShapeSphere*>(_target))
	{
		//Assumes perfect sphere
		float radius = _target->getScalePosition().x;
		glm::vec3 v = _target->getShapePosition() - gameCamera.getCamPos();
		float a = glm::dot(rayCast, rayCast);
		float b = 2 * glm::dot(v, rayCast);
		float c = glm::dot(v, v) - radius * radius;
		float d = b * b - 4 * a* c;

		if (d > 0) {
			/*float x1 = (-b - sqrt(d)) / 2;
			float x2 = (-b + sqrt(d)) / 2;
			if (x1 >= 0 && x2 >= 0) return true;

			std::cout << "a: " << a << std::endl << "b: " << b << std::endl << "c: " << c << std::endl << "d: " << d << std::endl;

			std::cout << x1 << ", " << x2 << std::endl;
			// intersects
			if (x1 < 0 && x2 >= 0) return true;
			// intersects */
			return true;
		}
		else if (d <= 0)
		{
			return false;// no intersection
		}
	}
	else if (dynamic_cast<CShapeCube*>(_target))
	{

		float tmin = ((_target->getShapePosition().x - _target->getScalePosition().x) - gameCamera.getCamPos().x) / rayCast.x;
		float tmax = ((_target->getShapePosition().x + _target->getScalePosition().x) - gameCamera.getCamPos().x) / rayCast.x;

		if (tmin > tmax) swap(tmin, tmax);

		float tymin = ((_target->getShapePosition().y - _target->getScalePosition().y) - gameCamera.getCamPos().y) / rayCast.y;
		float tymax = ((_target->getShapePosition().y + _target->getScalePosition().y) - gameCamera.getCamPos().y) / rayCast.y;

		if (tymin > tymax) swap(tymin, tymax);

		if ((tmin > tymax) || (tymin > tmax))
			return false;

		if (tymin > tmin)
			tmin = tymin;

		if (tymax < tmax)
			tmax = tymax;

		float tzmin = ((_target->getShapePosition().z - _target->getScalePosition().z) - gameCamera.getCamPos().z) / rayCast.z;
		float tzmax = ((_target->getShapePosition().z + _target->getScalePosition().z) - gameCamera.getCamPos().z) / rayCast.z;

		if (tzmin > tzmax) swap(tzmin, tzmax);

		if ((tmin > tzmax) || (tzmin > tmax))
			return false;

		if (tzmin > tmin)
			tmin = tzmin;

		if (tzmax < tmax)
			tmax = tzmax;

		return true;
	}
	else if (dynamic_cast<CShapeTerrain*>(_target))
	{
		/*//Algorithm is theoritically quick but not very good
		CShapeTerrain* myTerrain = dynamic_cast<CShapeTerrain*>(_target);
		const float maxDistance = 100.0f; //Max distance when checking collisions
		const float accuracy = 1.0f; //Room for error when checking the Y distance

		glm::vec3 currPoint = maxDistance * rayCast; // initial point to check
		*/

		//This algorithm is still too slow,
		/*//Get distance to triangle...
		// apply distance in ray direction,
		//if dist between points is close enough try normal triangle, ray intersection
		CShapeTerrain* myTerrain = dynamic_cast<CShapeTerrain*>(_target);
		//Get triangle from the mesh
		int terrainIndices = myTerrain->getNoOfIndices();
		glm::vec3 position0;
		glm::vec3 position1;
		glm::vec3 position2;
		glm::vec3 pointOfIntersection;
		glm::vec3 camPos = gameCamera.getCamPos();
		float currDist = 0.0f;
		std::vector<GLfloat> vertices = myTerrain->returnVertices();
		std::vector<GLuint> indices = myTerrain->returnIndices();

		for (int x = 0; x < terrainIndices; x += 3)
		{
			//fetch positions of current triangle
			position0[0] = vertices[(indices[x] * 11)];
			position0[1] = vertices[(indices[x] * 11) + 1];
			position0[2] = vertices[(indices[x] * 11) + 2];

			position1[0] = vertices[(indices[x+1] * 11)];
			position1[1] = vertices[(indices[x + 1] * 11) + 1];
			position1[2] = vertices[(indices[x + 1] * 11) + 2];

			position2[0] = vertices[(indices[x + 2] * 11)];
			position2[1] = vertices[(indices[x + 2] * 11) + 1];
			position2[2] = vertices[(indices[x + 2] * 11) + 2];

			position0 = glm::vec3(0, 0, 0);
			position1 = glm::vec3(0, 0, 0);
			position2 = glm::vec3(0, 0, 0);

			float aveX = 0;
			float aveY = 0;
			float aveZ = 0;

			float aveX = position0[0] + position1[0] + position2[0];
			aveX *= 0.333;
			float aveY = position0[1] + position1[1] + position2[1];
			aveY *= 0.333;
			float aveZ = position0[2] + position1[2] + position2[2];
			aveZ *= 0.333;

			//currDist = sqrt( pow(camPos[0] - aveX,2) + pow(camPos[1] - aveY,2) + pow(camPos[2] - aveZ,2));

			//apply distance to ray direction
			//glm::vec3 rayAtDist = rayCast * currDist;
			

			//Check if this point is within range to collide with the section of the mesh
		}*/

		//This algorithm gives perfect coordinate but is slow to process, need to make a faster one somehow
		/*CShapeTerrain* myTerrain = dynamic_cast<CShapeTerrain*>(_target);
		//Get triangle from the mesh
		int terrainIndices = myTerrain->getNoOfIndices();

		glm::vec3 position0;
		glm::vec3 position1;
		glm::vec3 position2;

		//glm::vec3 pointOfIntersection;

		const float EPSILON = 0.0000001;
		//Vector3D vertex0 = inTriangle->vertex0;
		//Vector3D vertex1 = inTriangle->vertex1;
		//Vector3D vertex2 = inTriangle->vertex2;
		glm::vec3 edge1, edge2, h, s, q;
		float a, f, u, v;

		for (int x = 0; x < terrainIndices; x += 3) //very accurate but slow
		//for (int x = 0; x < terrainIndices; x += 30) //increasing size of triangles shoud theoritically speed up process but keep accuracy
		{
			//fetch positions of current triangle
			position0[0] = myTerrain->getVertices((myTerrain->getIndices(x) * 11));
			position0[1] = myTerrain->getVertices((myTerrain->getIndices(x) * 11) + 1);
			position0[2] = myTerrain->getVertices((myTerrain->getIndices(x) * 11) + 2);

			position1[0] = myTerrain->getVertices((myTerrain->getIndices(x+1) * 11));
			position1[1] = myTerrain->getVertices((myTerrain->getIndices(x+1) * 11) + 1);
			position1[2] = myTerrain->getVertices((myTerrain->getIndices(x+1) * 11) + 2);

			position2[0] = myTerrain->getVertices((myTerrain->getIndices(x+2) * 11));
			position2[1] = myTerrain->getVertices((myTerrain->getIndices(x+2) * 11) + 1);
			position2[2] = myTerrain->getVertices((myTerrain->getIndices(x+2) * 11) + 2);

			//https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorith
			
			edge1 = position1 - position0;
			edge2 = position2 - position0;
			h = glm::cross(rayCast, edge2); //rayCast.crossProduct(edge2);
			a = glm::dot(edge1, h);  //edge1.dotProduct(h);
			if (a > -EPSILON && a < EPSILON)
			{
				continue;    // This ray is parallel to this triangle.
			}
			f = 1.0 / a;
			s = gameCamera.getCamPos() - position0;
			u = f * glm::dot(s, h);//s.dotProduct(h);
			if (u < 0.0 || u > 1.0)
			{
				continue;
			}
			q = glm::cross(s, edge1); //s.crossProduct(edge1);
			v = f * glm::dot(rayCast, q); //rayVector.dotProduct(q);
			if (v < 0.0 || u + v > 1.0)
			{
				continue;
			}
			// At this stage we can compute t to find out where the intersection point is on the line.
			float t = f * glm::dot(edge2, q); //edge2.dotProduct(q);
			if (t > EPSILON && t < 1 / EPSILON) // ray intersection
			{
				intersectionPoint = gameCamera.getCamPos() + rayCast * t;
		
				return true;
			}
		}*/


		//Only two triangles
		CShapeTerrain* myTerrain = dynamic_cast<CShapeTerrain*>(_target);
		//Get triangle from the mesh
		int terrainIndices = myTerrain->getNoOfIndices();

		int cols = myTerrain->getNumCols();
		int rows = myTerrain->getNumCols();

		cols -= 1;
		rows -= 1;

		// (numRows - 1) *(numCols - 1) * 2
		/*
				NOTES FOR THIS BECAUSE IT GOT CONFUSING
					size if indices is (cols-1) * (rows-1) * 2 *3
					When getting an indicie using the { (row * columns) + column } formula
					we actually do { ((row * (columns)) + column) * 6 } to get the correct position.
					it is a little confusing.
		*/
			

		struct triangle
		{
		public:
			glm::vec3 position0;
			glm::vec3 position1;
			glm::vec3 position2;

			//Returns point halfway on line opposite to current node
			glm::vec3 getHalfwayPoint()
			{
				return glm::vec3((position1[0] + position2[0]) * 0.5f, (position1[1] + position2[1]) * 0.5f, (position1[2] + position2[2]) * 0.5f);
			}
			
		};

		triangle triangle1;
		triangle triangle2;
		triangle currentTriangle;

		//create first triangle
		triangle1.position0[0] = myTerrain->getVertices((myTerrain->getIndices(0) * 11));
		triangle1.position0[1] = myTerrain->getVertices((myTerrain->getIndices(0) * 11) + 1);
		triangle1.position0[2] = myTerrain->getVertices((myTerrain->getIndices(0) * 11) + 2);

		triangle1.position1[0] = myTerrain->getVertices((myTerrain->getIndices( ((0 * (cols)) + (cols-1)) *6)    * 11)      );
		triangle1.position1[1] = myTerrain->getVertices((myTerrain->getIndices( ((0 * (cols)) + (cols-1)) *6)    * 11)   + 1);
		triangle1.position1[2] = myTerrain->getVertices((myTerrain->getIndices( ((0 * (cols)) + (cols-1)) *6)    * 11)   + 2);

		triangle1.position2[0] = myTerrain->getVertices((myTerrain->getIndices((((rows-1) * (cols)) + 0) * 6) * 11));
		triangle1.position2[1] = myTerrain->getVertices((myTerrain->getIndices((((rows-1) * (cols)) + 0) * 6) * 11) + 1);
		triangle1.position2[2] = myTerrain->getVertices((myTerrain->getIndices((((rows-1) * (cols)) + 0) * 6) * 11) + 2);

		//createShapeCube(triangle1.position0[0], triangle1.position0[1], triangle1.position0[2], 1, 1, 1, Shader_TextureOnly, false,"he_stand.jpg");
		//createShapeCube(triangle1.position1[0], triangle1.position1[1], triangle1.position1[2], 1, 1, 1, Shader_TextureOnly, false, "he_stand.jpg");
		//createShapeCube(triangle1.position2[0], triangle1.position2[1], triangle1.position2[2], 1, 1, 1, Shader_TextureOnly, false, "he_stand.jpg");

		//Second triangle
		triangle2.position0[0] = myTerrain->getVertices((myTerrain->getIndices(((0 * (cols)) + (cols - 1)) * 6) * 11));
		triangle2.position0[1] = myTerrain->getVertices((myTerrain->getIndices(((0 * (cols)) + (cols - 1)) * 6) * 11) + 1);
		triangle2.position0[2] = myTerrain->getVertices((myTerrain->getIndices(((0 * (cols)) + (cols - 1)) * 6) * 11) + 2);

		triangle2.position1[0] = myTerrain->getVertices((myTerrain->getIndices((((rows - 1) * (cols)) + 0) * 6) * 11));
		triangle2.position1[1] = myTerrain->getVertices((myTerrain->getIndices((((rows - 1) * (cols)) + 0) * 6) * 11) + 1);
		triangle2.position1[2] = myTerrain->getVertices((myTerrain->getIndices((((rows - 1) * (cols)) + 0) * 6) * 11) + 2);

		triangle2.position2[0] = myTerrain->getVertices((myTerrain->getIndices((((rows - 1) * (cols)) + (cols - 1)) * 6) * 11));
		triangle2.position2[1] = myTerrain->getVertices((myTerrain->getIndices((((rows - 1) * (cols)) + (cols - 1)) * 6) * 11) + 1);
		triangle2.position2[2] = myTerrain->getVertices((myTerrain->getIndices((((rows - 1) * (cols)) + (cols - 1)) * 6) * 11) + 2);

		//createShapeCube(triangle2.position0[0], triangle2.position0[1], triangle2.position0[2], 1, 1, 1, Shader_PureVertex, false);
		//createShapeCube(triangle2.position1[0], triangle2.position1[1], triangle2.position1[2], 1, 1, 1, Shader_PureVertex, false);
		//createShapeCube(triangle2.position2[0], triangle2.position2[1], triangle2.position2[2], 1, 1, 1, Shader_PureVertex, false);

		int maxRepetitions = 1000;// How many loops until we choose a suitable triangle region. More loops = greater accuracy on hills

		while (maxRepetitions)
		{
			//Ray is somewhere in this triangle. Now we divide it in half
			if (terrainPickingLoop(triangle1.position0, triangle1.position1, triangle1.position2, rayCast, intersectionPoint))
			{
				currentTriangle = triangle1;
			}
			else if ((terrainPickingLoop(triangle2.position0, triangle2.position1, triangle2.position2, rayCast, intersectionPoint)))
			{
				currentTriangle = triangle2;
			}
			//POSSIBLE IMPROVEMENTS
			//Could check again to see if both triangles fail create triangles using the highest and lowest points on the mesh
			//That way can check hieght more accuratly.
			else
			{
				//return(false);
				//break;
			}

			bool breaker;//needed to check terrain height

			triangle1.position0 = currentTriangle.getHalfwayPoint();
			//Make sure height is correct
			triangle1.position0.y = myTerrain->getHeight(triangle1.position0.x, triangle1.position0.z,breaker);
			triangle1.position1 = currentTriangle.position1;
			triangle1.position2 = currentTriangle.position0;

			triangle2.position0 = currentTriangle.getHalfwayPoint();
			//Make sure height is correct
			triangle2.position0.y = myTerrain->getHeight(triangle2.position0.x, triangle2.position0.z, breaker);
			triangle2.position1 = currentTriangle.position0;
			triangle2.position2 = currentTriangle.position2;

			maxRepetitions--;
		}

		return(true);

	}
	else
	{
		Tools::ErrorMessage("Shape chosen for mouse picking is currently not supported! sorry ");
	}

	return false;
}

bool CGameClass::terrainPickingLoop(glm::vec3 position0, glm::vec3 position1, glm::vec3 position2, glm::vec3 Ray, glm::vec3 & intersectionPoint)
{
	//https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorith

	const double EPSILON = 0.0000001;
	//Vector3D vertex0 = inTriangle->vertex0;
	//Vector3D vertex1 = inTriangle->vertex1;
	//Vector3D vertex2 = inTriangle->vertex2;
	glm::vec3 edge1, edge2, h, s, q;
	float a, f, u, v;

	edge1 = position1 - position0;
	edge2 = position2 - position0;
	h = glm::cross(Ray, edge2); //rayCast.crossProduct(edge2);
	a = glm::dot(edge1, h);  //edge1.dotProduct(h);
	if (a > -EPSILON && a < EPSILON)
	{
		return(false);    // This ray is parallel to this triangle.
	}
	f = 1.0f / a;
	s = gameCamera.getCamPos() - position0;
	u = f * glm::dot(s, h);//s.dotProduct(h);
	if (u < 0.0 || u > 1.0)
	{
		return(false);
	}
	q = glm::cross(s, edge1); //s.crossProduct(edge1);
	v = f * glm::dot(Ray, q); //rayVector.dotProduct(q);
	if (v < 0.0 || u + v > 1.0)
	{
		return(false);
	}
	// At this stage we can compute t to find out where the intersection point is on the line.
	float t = f * glm::dot(edge2, q); //edge2.dotProduct(q);
	if (t > EPSILON && t < 1 / EPSILON) // ray intersection
	{
		intersectionPoint = gameCamera.getCamPos() + Ray * t;

		return true;
	}
	return false;
}

glm::vec3 CGameClass::getGameCamPosition()
{
	return gameCamera.getCamPos();
}

CCamera& CGameClass::getGameCam()
{
	return gameCamera;
}

CCamera& CGameClass::getUICam()
{
	return UICamera;
}

CShapeTerrain * CGameClass::getWalkableTerrain()
{
	return walkableTerrain;
}

CGameClass::GameState CGameClass::checkGameState()
{
	return CurrentState;
}


#pragma region Graphics


bool CGameClass::addGameObject(CGameObject * _newObject)
{
	for (int x = 0; x < MAX_GAMEOBJECTS; x++)
	{
		if (gameObjects[x] == nullptr)
		{
			gameObjects[x] = _newObject;
			activeGameObjects++;
			return(true);
		}
	}
	return(false);
}

glm::mat4 CGameClass::getOrthoProjections()
{
	return UICamera.getProjMatr();
}

//Safely destroys a shape object, without disrupting the rest of the shapes
void CGameClass::SafeDestroyShape(CShapes * _target)
{
	for (int x = 0; x < MAX_SHAPES; x++)
	{
		if (ShapeList[x] == _target && _target != nullptr)
		{
			delete _target;
			activeShapes--;
			while (ShapeList[x + 1] != nullptr)
			{
				ShapeList[x] = ShapeList[x + 1];
				x++;
			}
			ShapeList[x] = nullptr;
		}
	}
}

//Safely destroys a text object, without disrupting the rest of the texts
void CGameClass::SafeDestroyText(CTextLable * _target)
{
	for (int x = 0; x < MAX_TEXT_LABELS; x++)
	{
		if (TextLabels[x] == _target && _target != nullptr)
		{
			delete _target;
			activeTextLabels--;
			while (TextLabels[x + 1] != nullptr)
			{
				TextLabels[x] = TextLabels[x + 1];
				x++;
			}
			TextLabels[x] = nullptr;
		}
	}
}
//Safely destroys a button object, without disrupting the rest of the buttons
void CGameClass::SafeDestroyButton(CButton * _target)
{
	for (int x = 0; x < MAX_BUTTONS; x++)
	{
		if (ButtonList[x] == _target && _target != nullptr)
		{

			SafeDestroyText(_target->getMyText());
			SafeDestroyShape(_target->getMyShape());
			delete _target;
			activeButtons--;
			while (ButtonList[x + 1] != nullptr)
			{
				ButtonList[x] = ButtonList[x + 1];
				x++;
			}
			ButtonList[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyModel(Model * _target)
{
	for (int x = 0; x < MAX_MODELS; x++)
	{
		if (ModelList[x] == _target && _target != nullptr)
		{
			delete _target;
			activeModels--;
			while (ModelList[x + 1] != nullptr)
			{
				ModelList[x] = ModelList[x + 1];
				x++;
			}
			ModelList[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyGameObject(CGameObject * _target)
{
	for (int x = 0; x < MAX_GAMEOBJECTS; x++)
	{
		if (_target != nullptr && gameObjects[x] == _target)
		{
			//Kill me
			_target->shutdown();

			delete _target;
			activeGameObjects--;
			while (gameObjects[x + 1] != nullptr)
			{
				gameObjects[x] = gameObjects[x + 1];
				x++;
			}
			gameObjects[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyLight(CLighting * _target)
{
	for (int x = 0; x < MAX_WORLD_LIGHTS; x++)
	{
		if (LightLists[x] == _target && _target != nullptr)
		{
			delete _target;
			activeLights--;
			while (LightLists[x + 1] != nullptr)
			{
				LightLists[x] = LightLists[x + 1];
				x++;
			}
			LightLists[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyAllShapes()
{
	for (int x = 0; x < MAX_SHAPES; x++)
	{
		if (ShapeList[x] != nullptr)
		{
			//add imporant shutdown calls here
			if (dynamic_cast<CShapeTerrain*>(ShapeList[x]))
			{
				dynamic_cast<CShapeTerrain*>(ShapeList[x])->shutDown();
			}

			delete ShapeList[x];
			activeShapes--;

			ShapeList[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyAllText()
{
	for (int x = 0; x < MAX_TEXT_LABELS; x++)
	{
		if (TextLabels[x] != nullptr)
		{
			delete TextLabels[x];
			activeTextLabels--;
			TextLabels[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyAllButtons()
{
	for (int x = 0; x < MAX_BUTTONS; x++)
	{
		if (ButtonList[x]  != nullptr)
		{

			SafeDestroyText(ButtonList[x]->getMyText());
			SafeDestroyShape(ButtonList[x]->getMyShape());
			delete ButtonList[x];
			activeButtons--;
			
			ButtonList[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyAllModels()
{
	for (int x = 0; x < MAX_MODELS; x++)
	{
		if (ModelList[x] != nullptr)
		{
			delete ModelList[x];
			activeModels--;
		
			ModelList[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyAllGameObjects()
{
	for (int x = 0; x < MAX_GAMEOBJECTS; x++)
	{
		if (gameObjects[x] != nullptr )
		{
			//Kill me
			gameObjects[x]->shutdown();

			delete gameObjects[x];
			activeGameObjects--;

			gameObjects[x] = nullptr;
		}
	}
}

void CGameClass::SafeDestroyAllLights()
{
	for (int x = 0; x < MAX_WORLD_LIGHTS; x++)
	{
		if (LightLists[x] != nullptr)
		{
			delete LightLists[x];
			activeLights--;
			LightLists[x] = nullptr;
		}
	}
}

//Takes parameters to create a new textlabel object and add to draw list
bool CGameClass::addTextLabel(CTextLable * _label)
{
	for (int x = 0; x < MAX_TEXT_LABELS; x++)
	{
		if (TextLabels[x] == nullptr)
		{
			TextLabels[x] = _label;
			activeTextLabels++;
			return(true);
		}
	}
	return(false);

}
//Takes parameters to create a new button object and add to draw list
bool CGameClass::addButton(CButton * _button)
{
	for (int x = 0; x < MAX_BUTTONS; x++)
	{
		if (ButtonList[x] == nullptr)
		{
			ButtonList[x] = _button;
			activeButtons++;
			return(true);
		}
	}
	return(false);
}

bool CGameClass::addModel(Model * _model)
{
	for (int x = 0; x < MAX_MODELS; x++)
	{
		if (ModelList[x] == nullptr)
		{
			ModelList[x] = _model;
			activeModels++;
			return(true);
		}
	}
	return(false);
}


bool CGameClass::addLight(CLighting * _light)
{
	for (int x = 0; x < MAX_WORLD_LIGHTS; x++)
	{
		if (LightLists[x] == nullptr)
		{
			LightLists[x] = _light;
			activeLights++;
			return(true);
		}
	}
	return(false);
}

//Takes a shape and adds to draw list
bool CGameClass::addShape(CShapes* newShape)
{
	for (int x = 0; x < MAX_SHAPES; x++)
	{
		if (ShapeList[x] == nullptr)
		{
			activeShapes++;
			ShapeList[x] = newShape;
			return(true);
		}
	}


	return false;
}

CShapesCubemap* CGameClass::getCurrentSkyBox()
{
	return CurrentCubemape;
}

//Takes parameters to create a new line object and add to draw list
CShapeLine* CGameClass::createShapeLine(float startX, float startY, float endX, float endY)
{
	CShapeLine* newLine(new CShapeLine(startX, startY, endX, endY));

	if (addShape(newLine))
	{
		return (newLine);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE LINE FAILED");
	}
}

//Takes parameters to create a new square object and add to draw list
CShapeSquare* CGameClass::createShapeSquare(std::pair<float, float> CentrePos, float newXSize, float newYSize, shaderFiles _myShader, bool Blending, std::string Texturepath)
{
	//CShapeSquare *newSquare = new CShapeSquare( CentrePos,newXSize, newYSize, Textured, Blending, Texturepath);
	CShapeSquare* newSquare(new CShapeSquare(CentrePos, newXSize, newYSize, _myShader, Blending, Texturepath));
	if (addShape(newSquare))
	{
		return (newSquare);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE SQUARE FAILED");
	}
}
//Takes parameters to create a new Triangle object and add to draw list
CShapeTriangle* CGameClass::createShapeTriangle(std::pair<float, float> CentrePos, int xDist, int yDist, shaderFiles _myShader, bool Blending, std::string Texturepath)
{
	//CShapeTriangle *newTri = new CShapeTriangle(CentrePos, xDist, yDist, Textured, Blending, Texturepath);
	CShapeTriangle* newTri(new CShapeTriangle(CentrePos, xDist, yDist, _myShader, Blending, Texturepath));
	if (addShape(newTri))
	{
		return (newTri);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE TRIANLGE FAILED");
	}
}

//Takes parameters to create a new pyramid object and add to draw list
CShapePyrmid * CGameClass::createShapePyrmid(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath)
{
	CShapePyrmid* newPyrmid(new CShapePyrmid(CentreX, CentreY, CentreZ, newXSize, newYSize, newZSize, _myShader, Blending, Texturepath));
	if (addShape(newPyrmid))
	{
		return (newPyrmid);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE PYRAMID FAILED");
	}
}
//Takes parameters to create a new Cube object and add to draw list
CShapeCube * CGameClass::createShapeCube(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath)
{
	CShapeCube* newCube(new CShapeCube(CentreX, CentreY, CentreZ, newXSize, newYSize, newZSize, _myShader, Blending, Texturepath));
	if (addShape(newCube))
	{
		return (newCube);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE Cube FAILED");
	}
}

CShapeCube * CGameClass::createShapeCube(glm::vec3 Colour)
{
	CShapeCube* newCube(new CShapeCube(Colour));
	if (addShape(newCube))
	{
		return (newCube);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE Cube FAILED");
	}
}

//Takes parameters to create a new Sphere object and add to draw list
CShapeSphere * CGameClass::createShapeSphere(float CentreX, float CentreY, float CentreZ, float sizeX, float sizeY, float sizeZ, shaderFiles _myShader, bool blending, std::string texturePath)
{
	CShapeSphere* newSphere(new CShapeSphere(CentreX, CentreY, CentreZ, sizeX, sizeY, sizeZ, _myShader, blending, texturePath));
	if (addShape(newSphere))
	{
		return (newSphere);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING SHAPE Shpere FAILED");
	}
}
Model * CGameClass::createModel(float xPos, float yPos, float zPos, float xScale, float yScale, float zScale, std::string path, bool isTessalated)
{
	//Check if model already exists
	 //= ModelLoad::getInstance()->checkModelPath(path);
	if (!ModelLoad::getInstance()->checkModelPath(path))
	{
		Model *newModel;
		newModel = (new Model(xPos, yPos, zPos, xScale, yScale, zScale, path, &gameCamera, isTessalated));
		ModelLoad::getInstance()->addModel(path, *newModel);

		if (addModel(newModel))
		{
			return (newModel);
		}
		else
		{
			return(nullptr);
			Tools::ErrorMessage("ADDING MODEL FAILED");
		}
	}
	else
	{
		Model *newModel = new Model(ModelLoad::getInstance()->getModel(path));
		newModel->setPos(glm::vec3(xPos, yPos, zPos));
		newModel->setScale(glm::vec3(xScale, yScale, zScale));

		if (addModel(newModel))
		{
			return (newModel);
		}
		else
		{
			return(nullptr);
			Tools::ErrorMessage("ADDING MODEL FAILED");
		}
	}

}
//Takes parameters to create a new textlabel object and add to textlabel list
CTextLable * CGameClass::createTextLabel(std::string _text, std::string _font, glm::vec2 _pos, glm::vec3 _color, float _scale)
{
	CTextLable* newLabel = new CTextLable(_text, _font, _pos, _color, _scale);
	if (addTextLabel(newLabel))
	{
		return(newLabel);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING TEXT LABEL FAILED");
	}
	return nullptr;
}


CButton * CGameClass::createButton(std::pair<float, float> centre, float xSize, float ySize, std::string text, std::function<void(int)> _action, std::string texturePath)
{
	CButton* new_Field = new CButton(centre, xSize, ySize, text, _action, texturePath);

	if (addButton(new_Field))
	{
		return(new_Field);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING _Button_ FAILED");
	}
	return nullptr;
}

CLighting * CGameClass::createLight(float _ambientStr, glm::vec3 _ambientColor, glm::vec3 _lightColor, glm::vec3 _lightPos, float _lightSpecStr, float _shininess)
{
	CLighting* new_Field = new CLighting(_ambientStr, _ambientColor, _lightColor, _lightPos, _lightSpecStr, _shininess);

	if (addLight(new_Field))
	{
		return(new_Field);
	}
	else
	{
		return(nullptr);
		Tools::ErrorMessage("ADDING LIGHT FAILED");
	}
	return nullptr;
}

//Funcs used for objects with fog shader
float CGameClass::getFogstart()
{
	return linearFogStartDist;
}

float CGameClass::getFogDepth()
{
	return linearFogDepth;
}
glm::vec3 CGameClass::getWorldLightColour()
{
	return(worldLightColour);
}
glm::vec3 CGameClass::getWorldLightPosition()
{
	return(worlrdLightPosition);
}

float CGameClass::getWorldLightStrength()
{
	return worldLightStrength;
}

#pragma endregion

#pragma region ButtonFunctions

void CGameClass::launchGame(int _number)
{
	CurrentState = PLAY;
	levelLoader(PLAY);
}
void CGameClass::settings(int _number)
{
	CurrentState = SETTINGS;
	levelLoader(SETTINGS);
}

void CGameClass::returnToMainMenu(int _number)
{
	CurrentState = MAINMENU;
	levelLoader(MAINMENU);
}

#pragma endregion

float CGameClass::getDeltaTime()
{
	return deltaTime;
}

