#pragma once

#include <fmod.hpp>

//General includes
#include "Tools.h"
#include "CCamera.h"

//Game loop includes
#include "CAi.h"
#include "CTextLabel.h"
#include "CButton.h"

//#include "CInput.h"
//#include "UIBuildItems.h"

//Shape includes
#include "CShapes.h"
#include "CShapeLine.h"
#include "CShapeSquare.h"
#include "CShapePyramid.h"
#include "CShapeCube.h"
#include "CShapeSphere.h"
#include "CShapesTriangle.h"
#include "CShapeTerrain.h"
#include "CGeometryObject.h"
#include "tessalationQuad.h"

#include "particalSystem.h"
#include "particle.h"

#include "GPUParticleSystem.h"

#include "CShapesCubeMap.h"
#include "CLighting.h"
#include "Model.h"
#include "animatedModel.h"
#include "frameBuffer.h"

//game objects
#include "gameObject.h"


//Network includes
#include "consoletools.h"
#include <functional>

#define MAX_GAMEOBJECTS 2000
#define MAX_SHAPES      40000		//NUMBER OF SHAPES THAT CAN BE DRAWN
#define MAX_TEXT_LABELS 2000     //NUMBER OF TEXT LABELS THAT CAN EXCIST
#define MAX_BUTTONS     2000      //NUMBER OF BUTTONS THAT CAN EXIST OH THATS HOW YOU SPELL IT 
#define MAX_MODELS		2000
#define MAX_WORLD_LIGHTS 100

//foreward de

class CGameClass
{
public:
	enum GameState
	{
		MAINMENU,
		PLAY,
		SETTINGS,
		PAUSED,
		LOBBY,
	};

	static CGameClass& getInstance();
	~CGameClass();

	void shutdown();

	//GAME LOOP RELATED FUNCS
	void InitializeGame();
	void Update();
	void ProcessInput();          //Process that input
	void Render();
	void Quitter(int _val);
	bool levelLoader(int _levelNum);

	//GetUIcamera for UI rendering
	glm::mat4 getOrthoProjections();
	GameState checkGameState();

#pragma region Graphics
	//GRAPHICS RELATED FUNCS
	bool addShape(CShapes* newShape);																//General

	CShapesCubemap* getCurrentSkyBox();
	
	//2D shapes
	CShapeLine* createShapeLine(float startX, float startY, float endX, float endY);			//Line Type
	CShapeSquare* createShapeSquare(std::pair<float, float> CentrePos, float newXSize, float newYSize, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n"); //Square Type
	CShapeTriangle* createShapeTriangle(std::pair<float, float> CentrePos, int xDist, int yDist, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n"); //Square Type

	//3D shapes
	CShapePyrmid* createShapePyrmid(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n"); //Add a pyramid
	CShapeCube* createShapeCube(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n"); //Add a cube
	CShapeCube* createShapeCube(glm::vec3 Colour); //Add a cube, only initialising the colour
	CShapeSphere* createShapeSphere(float CentreX, float CentreY, float CentreZ, float sizeX, float sizeY, float sizeZ, shaderFiles _myShader, bool blending, std::string texturePath = "\n"); //Add a Sphere

	//Game
	Model* createModel(float xPos, float yPos, float zPos, float xScale, float yScale, float zScale, std::string path, bool isTessalated);
	CTextLable* createTextLabel(std::string _text, std::string _font, glm::vec2 _pos, glm::vec3 _color, float _scale = 1);

	CButton* createButton(std::pair<float, float> centre, float xSize, float ySize, std::string text, std::function<void(int)> _action, std::string TexturePath);
	CLighting* createLight(float _ambientStr, glm::vec3 _ambientColor, glm::vec3 _lightColor, glm::vec3 _lightPos, float _lightSpecStr, float _shininess);

	//Adding game objects
	bool addGameObject(CGameObject* _newObject);
	bool addTextLabel(CTextLable* _label);
	bool addButton(CButton * _button);
	bool addModel(Model* _model);
	bool addLight(CLighting* _light);

	//SafeDestoryers - destory item and adjust arrays 
	void SafeDestroyShape(CShapes* _target);
	void SafeDestroyText(CTextLable* _target);
	void SafeDestroyButton(CButton* _target);
	void SafeDestroyModel(Model* _target);
	void SafeDestroyGameObject(CGameObject* _target);
	void SafeDestroyLight(CLighting* _target);

	void SafeDestroyAllShapes();
	void SafeDestroyAllText();
	void SafeDestroyAllButtons();
	void SafeDestroyAllModels();
	void SafeDestroyAllGameObjects();
	void SafeDestroyAllLights();

	//World lights
	CLighting* LightLists[MAX_WORLD_LIGHTS] = { nullptr };
	int activeLights = 0;

	//Fog information
	float getFogstart();
	float getFogDepth();

	glm::vec3 getWorldLightColour();
	glm::vec3 getWorldLightPosition();
	float getWorldLightStrength();


	/*Returns a raycast from a normalised device coordinate. 
	//Function currently used explicitly for mouse picking
	//Takes the point to cast the ray from and the camera the point is originating from*/
	glm::vec3 createRayCastFromScreen(std::pair<float,float> NDCoords, CCamera _camera);

	//Mouse picking check!
	//Takes an object to see if the mouse if on it via mouse picking!
	bool shapeOnMousePicker(CShapes* _target, glm::vec3 &intersectionPoint = glm::vec3(0, 0, 0));
	//Function used by shapeOnMousePicker to loop through terrain effectivly
	bool terrainPickingLoop(glm::vec3 pointA, glm::vec3 pointB, glm::vec3 pointC,  glm::vec3 Ray, glm::vec3 &intersectionPoint = glm::vec3(0, 0, 0));


	//Returns game camera pos
	glm::vec3 getGameCamPosition();
	CCamera& getGameCam();
	CCamera& getUICam();

	CShapeTerrain* getWalkableTerrain();

#pragma endregion

#pragma region ButtonFunctions
	void launchGame(int _number);
	void settings(int _number);
	void returnToMainMenu(int _number);
#pragma endregion

	//GAME STUFF
	float getDeltaTime();


private:
	//GAME STUFF
	GameState CurrentState = MAINMENU;	

	bool renderFramBuffer = false;
	//FrameBuffer TestBuffer;

	float cameraMoveSpeed = 50;

	glm::vec2 mousePos = glm::vec2(0, 0); //used for camera
	float const mouseSensitivity = 0.004f;

	//walkable terrain
	CShapeTerrain* walkableTerrain;

	//ENGINE STUFF

	//Parameters used for linear fog on objects with Shader_Fog attached
	float linearFogStartDist = 5.0f;
	float linearFogDepth = 50.0f;

	//Lighting
	glm::vec3 worldLightColour = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec3 worlrdLightPosition = glm::vec3(0.0f, 200.0f, 3.0f);
	float worldLightStrength = 0.3f;

	float deltaTime = (float)glutGet(GLUT_ELAPSED_TIME);

	CGameClass() {};

	CShapesCubemap* CurrentCubemape;
	
	CCamera gameCamera;									//Game camera or viewport 
	CCamera UICamera;

	//Storage library for all excisting items to be tracked
	CShapes* ShapeList[MAX_SHAPES] = { nullptr };		//Contains shapes for drawing
	int activeShapes = 0;								//Number of currently active shapes

	CTextLable* TextLabels[MAX_TEXT_LABELS] = { nullptr };
	int activeTextLabels = 0;

	CGameObject* gameObjects[MAX_GAMEOBJECTS] = { nullptr };
	int activeGameObjects = 0;

	CButton* ButtonList[MAX_BUTTONS] = { nullptr };
	int activeButtons = 0;	

	Model* ModelList[MAX_MODELS] = { nullptr };
	int activeModels = 0;

	FMOD::Sound* BackgroundSound = nullptr;
};
