#include "CGeometryObject.h"
#include "ShaderLoader.h"
#include "soil.h"
#include "CGameClass.h"

CGeometryObject::CGeometryObject()
{
}

/*CGeometryObject::CGeometryObject(glm::vec3 position, glm::vec3 scale,std::string vertex, std::string fragment, std::string geometry, bool textured , std::string texturePath )
{

	if (texturePath != "\n")
	{	
		SetTexture(texturePath, texHieght, texWidth);
	}
	setShapePosition(position);
	setScalePosition(scale);


	//GLfloat points[6] = { 0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f };

	setMyProgram(ShaderLoader::getInstance()->CreateProgram(vertex.c_str(), fragment.c_str(), geometry.c_str()));

	SetDrawMode(GL_POINTS);

	if (textured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));

	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(drawPositions), &drawPositions, GL_STATIC_DRAW);


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glBindVertexArray(0);

	setBlending(true);
}
*/


CGeometryObject::~CGeometryObject()
{
}



void CGeometryObject::shutDown()
{
}

void CGeometryObject::SwapTexture(std::string _tex)
{
}

void CGeometryObject::ChangeColour(float newColour[3])
{
}

void CGeometryObject::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

int CGeometryObject::GetIndicesSize()
{
	return 0;
}

void CGeometryObject::PrepareGraphics(CCamera renderCam)
{
	matriceUpdates(renderCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	}

	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}

void CGeometryObject::FlipTextures(bool horizontal, bool vertical)
{
}

std::vector<std::pair<int, int>> CGeometryObject::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}

void CGeometryObject::init(glm::vec3 position, glm::vec3 scale, std::string vertex, std::string fragment, std::string geometry, bool textured, std::string texturePath)
{
	if (texturePath != "\n")
	{
		SetTexture(texturePath, texHieght, texWidth);
	}
	setShapePosition(position);
	setScalePosition(scale);


	//GLfloat points[6] = { 0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f };

	setMyProgram(ShaderLoader::getInstance()->CreateProgram(vertex.c_str(), fragment.c_str(), geometry.c_str()));

	SetDrawMode(GL_POINTS);

	if (textured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));

	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(drawPositions), &drawPositions, GL_STATIC_DRAW);


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glBindVertexArray(0);

	setBlending(true);
}

void CGeometryObject::addDrawPosition(glm::vec3 _position)
{
	drawPositions.push_back(_position);
}

void CGeometryObject::matriceUpdates(CCamera renderCam)
{
	//Stuff always passed to the shader
//Camera Pos
	glUniform3fv(glGetUniformLocation(myProgram, "cameraPos"), 1, glm::value_ptr(renderCam.getCamPos()));
	glEnable(GL_CULL_FACE);
	//Geometry
	glm::mat4 transMat = glm::translate(glm::mat4(), shapePosition);

	glm::mat4 roll = glm::rotate(glm::mat4(1.0f), -rotationPosition[2], glm::vec3(0.0f, 0.0f, 1.0f));
	glm::mat4 pitch = glm::rotate(glm::mat4(1.0f), -rotationPosition[0], glm::vec3(1.0f, 0.0f, 0.0f));
	glm::mat4 yaw = glm::rotate(glm::mat4(1.0f), -rotationPosition[1], glm::vec3(0.0f, 1.0f, 0.0f));

	glm::mat4 scalar = glm::scale(glm::mat4(), scale);
	glm::mat4 modelMatrix = transMat * (roll * pitch * yaw)  * scalar;
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
	glm::mat4 mv = renderCam.getViewMatr()* modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "ModView"), 1, GL_FALSE, glm::value_ptr(mv));
	glm::mat4 vp = renderCam.getProjMatr() * renderCam.getViewMatr();
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "ViewPro"), 1, GL_FALSE, glm::value_ptr(vp));
	glm::mat4 mvp = renderCam.getProjMatr() * renderCam.getViewMatr()* modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "mvp"), 1, GL_FALSE, glm::value_ptr(mvp));

	//Call rendering functions needed to draw the shape
	for (std::function<void()> renderFunc : myRenderingFunctions)
	{
		renderFunc();
	}
}
