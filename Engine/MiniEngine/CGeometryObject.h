#pragma once
#include "CShapes.h"

#include "glm.hpp"
class CGeometryObject :
	public CShapes
{
public:
	CGeometryObject();

	//Takes in the position, scale and names of shader files
	//CGeometryObject(glm::vec3 position, glm::vec3 scale,std::string vertex, std::string fragment, std::string geometry,bool textured = false, std::string texturePath = "");
	~CGeometryObject();

	void shutDown();
	void SwapTexture(std::string _tex) ;
	void ChangeColour(float newColour[3]);
	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	int GetIndicesSize();
	void PrepareGraphics(CCamera gameCam);
	void FlipTextures(bool horizontal, bool vertical) ;
	std::vector<std::pair<int, int>> GetCardinalPoints();

	void init(glm::vec3 position, glm::vec3 scale, std::string vertex, std::string fragment, std::string geometry, bool textured, std::string texturePath);

	void addDrawPosition(glm::vec3 _position);

	//Overloaded from shape
	void matriceUpdates(CCamera renderCam);

private:

	int texWidth, texHieght; // texture params
	GLuint myTexture;

	std::vector<glm::vec3> drawPositions;
	

};

