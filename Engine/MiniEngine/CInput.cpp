//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : CInput.cpp
// Description     : Class handles user input through keyboard and mouse
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
#include "Tools.h"
#include "CInput.h"

CInput& CInput::getInstance()
{
	static CInput inst ;
	return inst;
}

void CInput::Update()
{
	for (int x = 0; x < 255; x++)
	{
		if (KeyStates[x] == UP_FIRST)
		{
			KeyStates[x] = UP;
		}
		else if (KeyStates[x] == DOWN_FIRST)
		{
			KeyStates[x] = DOWN;
		}
	}
	for (int y = 0; y < 3; y++)
	{
		if (MouseState[y] == DOWN_FIRST)
		{
			MouseState[y] = DOWN;
		}
		if (MouseState[y] == UP_FIRST)
		{
			MouseState[y] = UP;
		}
	}

}

// Function Name   : KeyDown
// Description     : Sets state in keystates when key is pressed
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
void CInput::KeyDown(unsigned char newKey, int x, int y) // newKey: ascii character on keyboard which has been pressed
{														 // x, y  : Co-ordinates of mouse with origen at top left 0, 0

	KeyStates[newKey] = DOWN_FIRST;
	

	mouseX = x;
	mouseY = y;
}

// Function Name   : KeyUp
// Description     : Sets state in keystates when key is pressed
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
void CInput::KeyUp(unsigned char newKey, int x, int y)// newKey: ascii character on keyboard which has been pressed
{												   	  // x, y  : Co-ordinates of mouse with origen at top left 0, 0
	KeyStates[newKey] = UP_FIRST;


	mouseX = x;
	mouseY = y;
}

// Function Name   : KeyState
// Description     : Checks the key state
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
InputStates CInput::KeyState(unsigned char newKey)
{
	return (KeyStates[newKey]);
}

void CInput::MouseClicks(int button, int state, int x, int y)
{
	if (button >= 3)
	{
		return;
	}

	MouseState[button] = (state == GLUT_DOWN) ? DOWN_FIRST : UP_FIRST;

	mouseX = x;
	mouseY = y;
}

void CInput::MousePassiveMove(int x, int y)
{
	mouseX = x;
	mouseY = y;
}

void CInput::MouseMove(int x, int y)
{
	mouseX = x;
	mouseY = y;
}

std::pair<int, int> CInput::getMousePos()
{
	//convert to centre 0,0 coorrdinates
	return std::pair<int, int>(mouseX -(WINDOW_WIDTH*.5), -1*(mouseY - (WINDOW_HIEGHT*.5)));
}

InputStates CInput::MouseStates(InputMouse state)
{
	return (MouseState[state]);
}

std::pair<float, float> CInput::getNormalizedDeviceCoordinates()
{
	//If getting inconsistent results try useing result of getMousePos
	float NDCx = (2.0f*mouseX) / WINDOW_WIDTH - 1;
	float NDCy = 1.0f - (2.0f*mouseY) / WINDOW_HIEGHT;
	//std::cout << NDCx << " : " << NDCy << std::endl;
	return(std::make_pair(NDCx, NDCy));
}
