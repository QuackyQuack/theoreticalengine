#include <glew.h>
#include <freeglut.h>
#include <utility>



#pragma once

//Mouse
enum InputMouse
{
	MOUSE_LEFT, MOUSE_MIDDLE, MOUSE_RIGHT
};

enum InputStates
{
	UP, DOWN, UP_FIRST, DOWN_FIRST
};

class CInput 
{
public:

	static CInput& getInstance();
	
	~CInput() {};

	//KeyBoard
	void Update();
	void KeyDown(unsigned char newKey, int x,int y);
	void KeyUp(unsigned char newKey, int x, int y);
	InputStates KeyState(unsigned char newKey);


	void MouseClicks(int button, int state, int x, int y);
	void MousePassiveMove(int x, int y);
	void MouseMove(int x, int y);

	std::pair<int, int> getMousePos();
	//Returns a floating pair which represents the position of the mouse with in -1 -> 1 range
	std::pair<float, float> getNormalizedDeviceCoordinates();

	InputStates MouseStates(InputMouse state);


	
private:

	CInput() {};

	InputStates KeyStates[255];
	InputStates MouseState[3];

	int mouseX;
	int mouseY;

};