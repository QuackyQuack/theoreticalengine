#include "CLighting.h"



CLighting::CLighting()
{
}
CLighting::CLighting(float _ambientStr, glm::vec3 _ambientColor, glm::vec3 _lightColor, glm::vec3 _lightPos, float _lightSpecStr, float _shininess)
{
	ambientStr = _ambientStr;
	ambientColor = _ambientColor;

	lightColor = _lightColor;
	lightPos = _lightPos;

	lightSpecStr = _lightSpecStr;
	shininess = _shininess;
}

CLighting::~CLighting()
{
}
