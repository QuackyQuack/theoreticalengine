#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"


#pragma once


class CLighting
{
public:
	CLighting();
	CLighting(float _ambientStr, glm::vec3 _ambientColor, glm::vec3 _lightColor, glm::vec3 _lightPos, float _lightSpecStr, float _shininess);
	~CLighting();


	
	float ambientStr;
	glm::vec3 ambientColor;

	glm::vec3 lightColor;
	glm::vec3 lightPos;

	float lightSpecStr;
	float shininess;
};

