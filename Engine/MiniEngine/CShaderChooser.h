#pragma once

enum shaderFiles
{
	Shader_PureVertex,
	Shader_CubeMap,
	Shader_TextureOnly,
	Shader_BaseLightingNoTexture,
	Shader_GourandPhongLightingTextureOnly,
	Shader_PhongLigtingTextureOnly,
	Shader_RimLightingTextureOnly,
	Shader_Model,
	Shader_Reflections,
	Shader_TextLabels,
	Shader_Fog,
	Shader_GenericParticleSystem,
	Shader_FireParticleSystem,
	Shader_TerrainBlending,
};

class CShaderChooser
{
public:

	static CShaderChooser* getInstance();
	~CShaderChooser();




private:
	CShaderChooser();
};

