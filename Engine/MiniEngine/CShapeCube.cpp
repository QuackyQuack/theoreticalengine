#include "ShaderLoader.h"
#include "CShapeCube.h"
#include "soil.h"


CShapeCube::CShapeCube()
{
}

//Create cube
CShapeCube::CShapeCube(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath)
{
	//Load texture
	if (Texturepath != "\n")
	{
		SetTexture(Texturepath, texHieght, texWidth);
	}

	setScalePosition(glm::vec3(newXSize, newYSize, newZSize));
	setShapePosition(glm::vec3(CentreX, CentreY, CentreZ));

	bool isTextured = setShaderInformation(_myShader);
	SetDrawMode(GL_TRIANGLES);
	
	if (isTextured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		//unsigned char* texDisplay = SOIL_load_image("Resources/Images/sorrycody.png", &texWidth, &texHieght, 0, SOIL_LOAD_RGBA);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(8 * sizeof(GLfloat))));
	glEnableVertexAttribArray(3);

	setBlending(Blending);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

//New constructor which only changes the colour, uses vertex shader
CShapeCube::CShapeCube(glm::vec3 Colour)
{
	for (int x = 0; x < 24; x++)
	{
		verticesTex[3 + (11 * x)] = Colour[0];
		verticesTex[4 + (11 * x)] = Colour[1];
		verticesTex[5 + (11 * x)] = Colour[2];
	}

	setShaderInformation(Shader_PureVertex);
	SetDrawMode(GL_TRIANGLES);

	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(8 * sizeof(GLfloat))));
	glEnableVertexAttribArray(3);

	setBlending(false);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}



CShapeCube::~CShapeCube()
{
}

//Return cubes indices size
int CShapeCube::GetIndicesSize()
{
	return (sizeof(indices) / sizeof(indices[0]));
}


// Function Name   : PrepareGraphics
// Description     : Ensures shape draws ( Does background stuff )
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeCube::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
	}
	
	if (texture)
	{
		glUseProgram(getMyProgram());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, myTexture);
		glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);
	}
	//Alpha
	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}

// Function Name   : ChangeColourWithArray
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeCube::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

// Function Name   : ChangeColour
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeCube::ChangeColour(float newColour[3])
{
	for (int x = 0; x < 24; x++)
	{
		verticesTex[3 + (11 * x)] = newColour[0];
		verticesTex[4 + (11 * x)] = newColour[1];
		verticesTex[5 + (11 * x)] = newColour[2];
	}

	
	setMyProgram(myProgram);

	//glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	//glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(8 * sizeof(GLfloat))));
	glEnableVertexAttribArray(3);


}
// Function Name   : FlipTextures
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeCube::FlipTextures(bool horizontal, bool vertical)
{
}
void CShapeCube::shutDown()
{
}
// Function Name   : SwapTexture
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeCube::SwapTexture(std::string _tex)
{
}
// Function Name   : GetCardinalPoints
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
std::vector<std::pair<int, int>> CShapeCube::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
