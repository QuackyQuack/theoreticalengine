#pragma once
#include "CShapes.h"
class CShapeCube :
	public CShapes
{
public:
	CShapeCube();
	CShapeCube(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n");
	CShapeCube(glm::vec3 Colour);

	~CShapeCube();

	int GetIndicesSize();
	void PrepareGraphics(CCamera gameCam);

	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);

	void ChangeColour(float newColour[3]);

	void FlipTextures(bool horizontal, bool vertical);
	void shutDown();
	void SwapTexture(std::string _tex);
	std::vector<std::pair<int, int>> GetCardinalPoints();
	

private:

	float xSize;
	float ySize;

	GLuint myTexture;
	GLfloat verticesTex[264]
	{
	    //Position				       Colour			    Tex co0rds      Normals

		//Front
		-1.f,1.f,1.f,				0.0f,0.0f,1.0f,			0.0f,0.0f,    0.0f,0.0f,1.0f,
		-1.f,-1.f,1.f,				0.0f,0.0f,1.0f,			0.0f,1.0f,    0.0f,0.0f,1.0f,
		1.f,-1.f,1.f,				0.0f,0.0f,1.0f,			1.0f,1.0f,    0.0f,0.0f,1.0f,
		1.f,1.f,1.f,				0.0f,0.0f,1.0f,			1.0f,0.0f,    0.0f,0.0f,1.0f,
		//Back
		-1.f,1.f,-1.f,				0.0f,0.0f,1.0f,			1.0f,0.0f,    0.0f,0.0f,-1.0f,
		-1.f,-1.f,-1.f,				0.0f,0.0f,1.0f,			1.0f,1.0f,    0.0f,0.0f,-1.0f,
		1.f,-1.f,-1.f,				0.0f,0.0f,1.0f,			0.0f,1.0f,    0.0f,0.0f,-1.0f,
		1.f,1.f,-1.f,				0.0f,0.0f,1.0f,			0.0f,0.0f,    0.0f,0.0f,-1.0f,
		//Left
		-1.f,1.f,-1.f,				0.0f,1.0f,0.0f,			0.0f,0.0f,    -1.0f,0.0f,0.0f,
		-1.f,-1.f,-1.f,				0.0f,1.0f,0.0f,			0.0f,1.0f,    -1.0f,0.0f,0.0f,
		-1.f,-1.f,1.f,				0.0f,1.0f,0.0f,			1.0f,1.0f,    -1.0f,0.0f,0.0f,
		-1.f,1.f,1.f,				0.0f,1.0f,0.0f,			1.0f,0.0f,    -1.0f,0.0f,0.0f,
		//Right
		1.f,1.f,1.f,				0.0f,1.0f,0.0f,			0.0f,0.0f,    1.0f,0.0f,0.0f,
		1.f,-1.f,1.f,				0.0f,1.0f,0.0f,			0.0f,1.0f,    1.0f,0.0f,0.0f,
		1.f,-1.f,-1.f,				0.0f,1.0f,0.0f,			1.0f,1.0f,    1.0f,0.0f,0.0f,
		1.f,1.f,-1.f,				0.0f,1.0f,0.0f,			1.0f,0.0f,    1.0f,0.0f,0.0f,
		//Top
		-1.f,1.f,-1.f,				1.0f,0.0f,0.0f,			0.0f,0.0f,    0.0f,1.0f,0.0f,
		-1.f,1.f,1.f,				1.0f,0.0f,0.0f,			0.0f,1.0f,    0.0f,1.0f,0.0f,
		1.f,1.f,1.f,				1.0f,0.0f,0.0f,			1.0f,1.0f,    0.0f,1.0f,0.0f,
		1.f,1.f,-1.f,				1.0f,0.0f,0.0f,			1.0f,0.0f,    0.0f,1.0f,0.0f,
		//Bottom
		-1.f,-1.f,1.f,				1.0f,0.0f,0.0f,			0.0f,0.0f,    0.0f,-1.0f,0.0f,
		-1.f,-1.f,-1.f,				1.0f,0.0f,0.0f,			0.0f,1.0f,    0.0f,-1.0f,0.0f,
		1.f,-1.f,-1.f,				1.0f,0.0f,0.0f,			1.0f,1.0f,    0.0f,-1.0f,0.0f,
		1.f,-1.f,1.f,				1.0f,0.0f,0.0f,			1.0f,0.0f,    0.0f,-1.0f,0.0f,
	};

	int texWidth, texHieght; // texture params
	GLuint indices[36] =
	{
		//+z
		0,1,2,
		0,2,3,
		//-z
		7,6,5,
		7,5,4,
		//-x
		8,9,10,
		8,10,11,
		//++x
		12,13,14,
		12,14,15,
		//+y
		16,17,18,
		16,18,19,
		//-y
		20,21,22,
		20,22,23
	};
};