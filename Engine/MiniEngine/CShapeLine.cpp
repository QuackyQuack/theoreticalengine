//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : CShapesLine.cpp
// Description     : Class for creating 2D lines // Inherits from CShapes.h
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//

#include "CShapeLine.h"
#include "ShaderLoader.h"
#include "Tools.h"
CShapeLine::CShapeLine()
{
}

// Function Name   : CShapeLine
// Description     : Constructor for making lines
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
CShapeLine::CShapeLine(float startX, float startY, float endX, float endY) // startX, startY    : starting coordinate of the line 
{																		   // endX, endY		: ending coordinate of the line
	//Set the position of the line
	m_startingPos.first = startX;
	m_startingPos.second = startY;
	m_endingPos.first = endX;
	m_endingPos.second = endY;

	//setScalePosition(glm::vec3(WINDOW_WIDTH, WINDOW_HIEGHT, 0.0));

	//Set vertices to corilate
	vertices[0] = m_startingPos.first / 100;
	vertices[1] = m_startingPos.second / 100;
	vertices[6] = m_endingPos.first / 100;
	vertices[7] = m_endingPos.second / 100;
	vertices[12] = m_endingPos.first / 100;
	vertices[13] = m_endingPos.second / 100;
	vertices[18] = m_startingPos.first / 100;
	vertices[19] = m_startingPos.second / 100;

	//Create my program
	setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad UnTex.frg"));

	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	//Set vertix pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);

	SetDrawMode(GL_LINES);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}


CShapeLine::~CShapeLine()
{
}

// Function Name   : getPointPosition
// Description     : Returns coordinates of a given node
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : -Currently unusable for 3D-
std::pair<float, float> CShapeLine::getPointPosition(int positionalNode) //positionalNode - Which node's position is needed
{
	switch (positionalNode)
	{
	case(NODE_STARTING):
	{
		return(m_startingPos);
	}
	case(NODE_ENDING):
	{
		return(m_endingPos);
	}
	default:return(m_startingPos);
	}

	return std::pair<float, float>();
}

void CShapeLine::shutDown()
{
}

// Function Name   : GetIndicesSize
// Description     : Constructor for making lines
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Used in render functions
int CShapeLine::GetIndicesSize()
{
	return(sizeof(indices) / sizeof(int));
}

// Function Name   : PrepareGraphics
// Description     : Ensures shape can be drawn
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Used in render functions
void  CShapeLine::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);
}

// Function Name   : ChangeColour
// Description     : Changes colour of the shape outside of creation
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CShapeLine::ChangeColour(float newColour[3])
{
	for (int x = 0; x < 4; x++)
	{
		vertices[3 + (x * 6)] = newColour[0];
		vertices[4 + (x * 6)] = newColour[1];
		vertices[5 + (x * 6)] = newColour[2];
	}

}

// Function Name   : ChangeColourWithArray
// Description     : Changes specific colours of Chosen points.
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CShapeLine::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
//nodeNums    - Nodes to be changed, 
//_newColours - New colour of node
{
	float _newColours[4][3];

	_newColours[0][0] = { _newColour1[0] };
	_newColours[0][1] = { _newColour1[1] };
	_newColours[0][2] = { _newColour1[2] };

	_newColours[1][0] = { _newColour2[0] };
	_newColours[1][1] = { _newColour2[1] };
	_newColours[1][2] = { _newColour2[2] };

	_newColours[2][0] = { _newColour3[0] };
	_newColours[2][1] = { _newColour3[1] };
	_newColours[2][2] = { _newColour3[2] };

	_newColours[3][0] = { _newColour4[0] };
	_newColours[3][1] = { _newColour4[1] };
	_newColours[3][2] = { _newColour4[2] };

	for (int x = 0; x < 4; x++)
	{
		if (nodeNums[x])
		{
			vertices[3 + (x * 6)] = _newColours[x][0];
			vertices[4 + (x * 6)] = _newColours[x][1];
			vertices[5 + (x * 6)] = _newColours[x][2];
		}
	}
}

// Function Name   : FlipTextures
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CShapeLine::FlipTextures(bool horizontal, bool vertical)
{
	//Line has not textures so flipping is not implemented
}

void CShapeLine::SwapTexture(std::string _tex)
{
}

// Function Name   : GetCardinalPoints
// Description     : Returns points of a shape
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
std::vector<std::pair<int, int>> CShapeLine::GetCardinalPoints() //Fetch points used for collision calculations
{
	std::vector<std::pair<int, int>> temp;
	temp.push_back(m_startingPos);
	temp.push_back(m_endingPos);
	return temp;
}

void CShapeLine::changePointLocation(bool firstPoint, bool secondPoint, std::pair<float, float> locationOne, std::pair<float, float> OptionalLocationTwo)
{

	//Set vertices to corilate

	if (firstPoint)
	{
		m_startingPos = locationOne;
		vertices[0] = m_startingPos.first / WINDOW_WIDTH;
		vertices[1] = m_startingPos.second / WINDOW_HIEGHT;
		vertices[18] = m_startingPos.first / WINDOW_WIDTH;
		vertices[19] = m_startingPos.second / WINDOW_HIEGHT;
	}
	if (secondPoint)
	{
		if (OptionalLocationTwo.first != NULL)
		{
			m_endingPos = OptionalLocationTwo;
		}
		else
		{
			m_endingPos = locationOne;
		}
		vertices[6] = m_endingPos.first / WINDOW_WIDTH;
		vertices[7] = m_endingPos.second / WINDOW_HIEGHT;
		vertices[12] = m_endingPos.first / WINDOW_WIDTH;
		vertices[13] = m_endingPos.second / WINDOW_HIEGHT;
	}

	setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad UnTex.frg"));

	//Create my program


	//Set up our EBO

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO

	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	//Set vertix pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
}

