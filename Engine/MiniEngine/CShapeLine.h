#pragma once
#include <vector>
#include "CShapes.h"
class CShapeLine : public CShapes
{
public:
	CShapeLine();
	CShapeLine(float startX, float startY, float endX, float endY);
	~CShapeLine();

	enum NodePos
	{
		NODE_STARTING,
		NODE_ENDING
	};

	std::pair<float, float> getPointPosition(int positionalNode);

	void shutDown();
	int GetIndicesSize();
	void PrepareGraphics(CCamera gameCam);
	void ChangeColour(float newColour[3]);
	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	void FlipTextures(bool horizontal, bool vertical);
	std::vector<std::pair<int, int>> GetCardinalPoints();
	void SwapTexture(std::string _tex);

	/*
	Changes the location of one of the points of the line.
	Point changed decided by the bools
	Only one vector is needed, but if both points need to be changed to different places two can be used :)
	In which case if both are changed and no second point is passed both will be set to the same point which renders the line invisible
	*/
	void changePointLocation(bool firstPoint, bool secondPoint, std::pair<float, float> locationOne, std::pair<float, float> OptionalLocationTwo = std::make_pair(NULL, NULL));

private:

	std::pair<float, float> m_startingPos;
	std::pair<float, float> m_endingPos;

	GLfloat vertices[24] = {
		//Position					Colour     
	 -1.0f,1.0f,0.0f,		1.0f, 0.0f, 0.0f,			//Top
	 -1.0f,1.0f,0.0f,		1.0f, 0.0f, 0.0f,			//Top 2
	 -1.0f,-1.0f,0.0f,		1.0f, 0.0f, 0.0f,			//Bottom
	 -1.0f,-1.0f,0.0f,		1.0f, 0.0f, 0.0f,			//Bottom 2
	};


	GLuint indices[6] =
	{
		0,1,2,
		0,2,3
	};
};


