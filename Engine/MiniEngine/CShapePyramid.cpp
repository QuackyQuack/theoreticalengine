#include "ShaderLoader.h"
#include "CShapePyramid.h"
#include "soil.h"

CShapePyrmid::CShapePyrmid()
{
}

CShapePyrmid::CShapePyrmid(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath)
{
	//Load texture
	if (Texturepath != "\n")																														
	{																																				
		SetTexture(Texturepath, texHieght, texWidth);
	}

	setScalePosition(glm::vec3(newXSize, newYSize,newZSize));
	setShapePosition(glm::vec3(CentreX, CentreY, CentreZ));

	bool isTextured = setShaderInformation(_myShader);
	SetDrawMode(GL_TRIANGLES);

	if (isTextured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		//unsigned char* texDisplay = SOIL_load_image("Resources/Images/sorrycody.png", &texWidth, &texHieght, 0, SOIL_LOAD_RGBA);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	setBlending(Blending);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

CShapePyrmid::~CShapePyrmid()
{
}

// Function Name   : GetIndicesSize
// Description     : Get size of indices
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
int CShapePyrmid::GetIndicesSize()
{
	return (sizeof(indices) / sizeof(indices[0]));
}

// Function Name   : PrepareGraphics
// Description     : background rendering setup
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapePyrmid::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam); //Update matrices

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
	}

	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}

// Function Name   : ChangeColourWithArray
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapePyrmid::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}
// Function Name   : ChangeColour
// Description     : bN/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapePyrmid::ChangeColour(float newColour[3])
{
}
void CShapePyrmid::shutDown()
{
}
// Function Name   : FlipTextures
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapePyrmid::FlipTextures(bool horizontal, bool vertical)
{
}
// Function Name   : SwapTexture
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapePyrmid::SwapTexture(std::string _tex)
{
}
// Function Name   : GetCardinalPoints
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
std::vector<std::pair<int, int>> CShapePyrmid::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
