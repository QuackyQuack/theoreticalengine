#pragma once
#include <vector>
#include "CShapes.h"
class CShapePyrmid :public CShapes
{
public:
	CShapePyrmid();
	CShapePyrmid(float CentreX, float CentreY, float CentreZ, float newXSize, float newYSize, float newZSize, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n");
	~CShapePyrmid();
	enum NodePos
	{
		NODE_TOP_LEFT,
		NODE_TOP_RIGHT,
		NODE_BOTTOM_RIGHT,
		NODE_BOTTOM_LEFT
	};

	int GetIndicesSize();

	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	void PrepareGraphics(CCamera gameCam);
	void ChangeColour(float newColour[3]);
	void shutDown();
	void FlipTextures(bool horizontal, bool vertical);
	std::vector<std::pair<int, int>> GetCardinalPoints();
	void SwapTexture(std::string _tex);

	

private:

	float xSize;
	float ySize;

	GLfloat verticesTex[143]
	{
		//Position				Colour			    Tex co0rds				Normals

	-1.0f,0.0f,-1.0f,			1.0f,1.0f,0.0f,			0.0f,1.0f,			-1.0f,0.0f,-1.0f,
	-1.0f,0.0f,1.0f,			1.0f,1.0f,0.0f,			0.0f,0.0f,			-1.0f,0.0f,1.0f,
	 1.0f,0.0f,1.0f,			1.0f,1.0f,0.0f,			1.0f,0.0f,			 1.0f,0.0f,1.0f,
	 1.0f,0.0f,-1.0f,			1.0f,1.0f,0.0f,			1.0f,1.0f,			 1.0f,0.0f,-1.0f,

	-1.0f,0.0f,-1.0f,			1.0f,1.0f,0.0f,			1.0f,1.0f,			-1.0f,0.0f,-1.0f,
	 1.0f,0.0f,-1.0f,			1.0f,1.0f,0.0f,			0.0f,1.0f,			 1.0f,0.0f,-1.0f,

	 1.0f,0.0f,-1.0f,			1.0f,1.0f,0.0f,			1.0f,1.0f,			1.0f,0.0f,-1.0f,
	 1.0f,0.0f,1.0f,			1.0f,1.0f,0.0f,			0.0f,1.0f,			1.0f,0.0f,1.0f,

	 1.0f,0.0f,1.0f,			1.0f,1.0f,0.0f,			1.0f,1.0f,			 1.0f,0.0f,1.0f,
	-1.0f,0.0f,1.0f,			1.0f,1.0f,0.0f,			0.0f,1.0f,			-1.0f,0.0f,1.0f,

	-1.0f,0.0f,1.0f,			1.0f,1.0f,0.0f,			1.0f,1.0f,			-1.0f,0.0f,1.0f,
	-1.0f,0.0f,-1.0f,			1.0f,1.0f,0.0f,			0.0f,1.0f,			-1.0f,0.0f,-1.0f,

	0.0f,1.0f,0.0f,				1.0f,1.0f,0.0f,			1.0f,0.0f,			0.0f,1.0f,0.0f,
	};

	

	GLuint indices[18] =
	{
		1,0,3,
		1,3,2,

		4,12,5,
		6,12,7,
		8,12,9,
		10,12,11,
	};
};

