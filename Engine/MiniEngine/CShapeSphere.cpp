#include <math.h>
#include "CShapeSphere.h"
#include "ShaderLoader.h"
#include "soil.h"
#include "CGameClass.h"


CShapeSphere::CShapeSphere()
{

}
CShapeSphere::CShapeSphere(float CentreX, float CentreY, float CentreZ, float sizeX, float sizeY, float sizeZ, shaderFiles _myShader, bool blending, std::string texturePath)
{
	if (texturePath != "\n")
	{
		SetTexture(texturePath, texHieght, texWidth);
	}

	setScalePosition(glm::vec3(sizeX, sizeY, sizeZ));
	setShapePosition(glm::vec3(CentreX, CentreY, CentreZ));

	SetDrawMode(GL_TRIANGLES);

	bool textured = setShaderInformation(_myShader);

	if(textured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		//unsigned char* texDisplay = SOIL_load_image("Resources/Images/sorrycody.png", &texWidth, &texHieght, 0, SOIL_LOAD_RGBA);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	int offset = 0;
	for (int i = 0; i < sections; i++)
	{
		theta = 0;

		for (int j = 0; j < sections; j++)
		{
			float x = float(cos(phi) * sin(theta));
			float y = (float)cos(theta);
			float z = float(sin(phi) * sin(theta));

			//Position
			vertices[offset++] = x * radius;
			vertices[offset++] = y * radius;
			vertices[offset++] = z * radius;

			//Colour / Normal
			vertices[offset++] = x;
			vertices[offset++] = y;
			vertices[offset++] = z;

			//tex pos
			vertices[offset++] = (float)i / (sections - 1);
			vertices[offset++] = (float)j / (sections - 1);

			theta += ( M_PI / (sections - 1));
		}

		phi += (2* M_PI) / (sections - 1);
	}

	offset = 0;
	for (int i = 0; i < sections; i++)
	{
		for (int j = 0; j < sections; j++)
		{
			indices[offset++] = (((i + 1) % sections) * sections) + ((j + 1) % sections);
			indices[offset++] = (((i + 1) % sections) * sections) + (j);
			indices[offset++] = (i * sections) + (j);

			indices[offset++] = (i * sections) + ((j + 1) % sections);
			indices[offset++] = (((i + 1) % sections) * sections) + ((j + 1) % sections);
			indices[offset++] = (i * sections) + (j);

		}
	}

	glGenVertexArrays(1, &GetVertexAtrib(VAT_VAO));
	glBindVertexArray(GetVertexAtrib(VAT_VAO));

	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	//Normal
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);


	setBlending(blending);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
//Create sphere


CShapeSphere::~CShapeSphere()
{
}

// Function Name   : GetIndicesSize
// Description     : Get size of indices
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
int CShapeSphere::GetIndicesSize()
{
	return (sizeof(indices) / sizeof(indices[0]));;
}


// Function Name   : PrepareGraphics
// Description     : Prepare to draw
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSphere::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam); //Update matrices

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
	}

	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

	glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_CUBE_MAP,(CGameClass::getInstance().getCurrentSkyBox()->getCubemap()));
	//glUniform1i(glGetUniformLocation(getMyProgram(), "skyBox"), 1);

	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}

// Function Name   : ChangeColourWithArray
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSphere::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

// Function Name   : ChangeColour
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSphere::ChangeColour(float newColour[3])
{
}

// Function Name   : FlipTextures
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSphere::FlipTextures(bool horizontal, bool vertical)
{
}

void CShapeSphere::shutDown()
{
}

// Function Name   : SwapTexture
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSphere::SwapTexture(std::string _tex)
{
}

// Function Name   : GetCardinalPoints
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
std::vector<std::pair<int, int>> CShapeSphere::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
