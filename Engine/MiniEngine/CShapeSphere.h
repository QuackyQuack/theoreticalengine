
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>

#include "CShapes.h"

#pragma once
class CShapeSphere :public CShapes
{
public:
	CShapeSphere();
	CShapeSphere(float CentreX, float CentreY, float CentreZ, float sizeX, float sizeY, float sizeZ, shaderFiles _myShader, bool blending, std::string texturePath = "\n");
	~CShapeSphere();

	int GetIndicesSize();

	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);

	void ChangeColour(float newColour[3]);
	void PrepareGraphics(CCamera gameCam);
	void FlipTextures(bool horizontal, bool vertical);
	void shutDown();
	void SwapTexture(std::string _tex);
	std::vector<std::pair<int, int>> GetCardinalPoints();
	


private:

	float radius = 1.0f;

	static const int sections = 20;
	static const int vertexAttrib = 8;
	static const int indexPerQuad = 6;

	double phi = 0;
	double theta = 0;

	float vertices[(sections) * (sections)* vertexAttrib];
	GLuint indices[(sections) * (sections)* indexPerQuad];
};

