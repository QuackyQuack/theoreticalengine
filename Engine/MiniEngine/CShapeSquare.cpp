//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : CShapeSquare.cpp
// Description     : Class for creating 2D squares // Inherits from CShapes.h
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//

#include "CShapeSquare.h"
#include "ShaderLoader.h"
#include "soil.h"



CShapeSquare::CShapeSquare()
{
}

// Function Name   : CShapeSquare constructor
// Description     : Creates a 2D square
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
CShapeSquare::CShapeSquare(std::pair<float, float> CentrePos, float newXSize, float newYSize, shaderFiles _myShader, bool Blending, std::string Texturepath) // CentrePos          : 'Centre' of the square in screen space from -1.0 to 1.0 for each memebr of the pair
{																																					 // newXSize, newYSize : Distance corners will be placed from the aformentioned centre on the x and y plain respectivly																																			 // Blending           : is the shape transparent, need to be blended?
	if (Texturepath != "\n")																														 // Texturepath        : NAME OF THE FILE to be made into a texture, if file is outside of the set images folder then ,
	{																																				 //					  The CShapes.cpp SetTexture function must be used with the overwrite paramiter set to true			  
		SetTexture(Texturepath, texHieght,texWidth);
	}

	myShader = _myShader;

	setScalePosition(glm::vec3(newXSize, newYSize, 0.0));
	setShapePosition(glm::vec3(CentrePos.first, CentrePos.second, 0.0));

	bool isTextured = setShaderInformation(_myShader);
	SetDrawMode(GL_TRIANGLES);

	if (tesselated)
	{
		//Tesselation shaders
		GLuint controlShader = ShaderLoader::getInstance()->CreateShader(GL_TESS_CONTROL_SHADER, "Resources/ShaderFiles/triTesControl.tes");
		GLuint evalShader = ShaderLoader::getInstance()->CreateShader(GL_TESS_EVALUATION_SHADER, "Resources/ShaderFiles/triTesEval.tes");

		glAttachShader(myProgram, controlShader);
		glAttachShader(myProgram, evalShader);

		//tesselation
		glPatchParameteri(GL_PATCH_VERTICES, 3);
	}

	if (isTextured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		//unsigned char* texDisplay = SOIL_load_image("Resources/Images/sorrycody.png", &texWidth, &texHieght, 0, SOIL_LOAD_RGBA);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);


	setBlending(Blending);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}


CShapeSquare::~CShapeSquare()
{
	int test = 0;
}

void CShapeSquare::shutDown()
{
}

int CShapeSquare::GetIndicesSize()
{
	return (sizeof(indices)/sizeof(indices[0]));
}

// Function Name   : PrepareGraphics constructor
// Description     : Ensures shape can be drawn by doing all background stuff
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CShapeSquare::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	}

	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());


}

// Function Name   : ChangeColourWithArray
// Description     : Changes specific colours of Chosen points.
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void CShapeSquare::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
//nodeNums    - Nodes to be changed, 
//_newColours - New colour of node
{
	float _newColours[4][3];

	_newColours[0][0] = { _newColour1[0] };
	_newColours[0][1] = { _newColour1[1] };
	_newColours[0][2] = { _newColour1[2] };

	_newColours[1][0] = { _newColour2[0] };
	_newColours[1][1] = { _newColour2[1] };
	_newColours[1][2] = { _newColour2[2] };

	_newColours[2][0] = { _newColour3[0] };
	_newColours[2][1] = { _newColour3[1] };
	_newColours[2][2] = { _newColour3[2] };

	_newColours[3][0] = { _newColour4[0] };
	_newColours[3][1] = { _newColour4[1] };
	_newColours[3][2] = { _newColour4[2] };

	for (int x = 0; x < 4; x++)
	{
		if (nodeNums[x])
		{
			verticesTex[3 + (x * 8)] = _newColours[x][0];
			verticesTex[4 + (x * 8)] = _newColours[x][1];
			verticesTex[5 + (x * 8)] = _newColours[x][2];
		}
	}

	setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad UnTex.frg"));

	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);
}

// Function Name   : ChangeColour
// Description     : Changes the colour of the shape
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSquare::ChangeColour(float newColour[3]) //Must be untextured.
{
	//glUseProgram(getMyProgram());

	//Reset OGL information

	for (int x = 0; x < 4; x++)
	{
		verticesTex[3 + (8 * x)] = newColour[0];
		verticesTex[4 + (8 * x)] = newColour[1];
		verticesTex[5 + (8 * x)] = newColour[2];
	}

	setMyProgram(myProgram);

	//Set up our EBO
	//glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//Set up our VBO
	//glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);
}

// Function Name   : FlipTextures
// Description     : Flips the shape's texture's about an axis
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSquare::FlipTextures(bool horizontal, bool vertical) // horizontal, vertical axis' to flip the texture about
{
	if (horizontal)
	{
		setScalePosition(glm::vec3(getScalePosition().x * -1, getScalePosition().y, getScalePosition().z));
	}
	if (vertical)
	{
		setScalePosition(glm::vec3(getScalePosition().x , getScalePosition().y*-1, getScalePosition().z));
	}
}

// Function Name   : SwapTexture
// Description     : Swap out the current texture for another
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeSquare::SwapTexture(std::string _tex)
{
	//setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad Tex.frg"));

	SetTexture(_tex, texHieght, texWidth);
	glGenTextures(1, &myTexture);
	glBindTexture(GL_TEXTURE_2D, myTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(GetTexture());
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);
}

// Function Name   : GetCardinalPoints
// Description     : Return points in 2D space for collisions
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
std::vector<std::pair<int, int>> CShapeSquare::GetCardinalPoints() //Used to return points used for collision calculation
{
	std::vector < std::pair<int, int>> TempVec;
	TempVec.push_back(std::make_pair(getShapePosition()[0] - getScalePosition()[0], getShapePosition()[1] + getScalePosition()[1])); //Top left
	TempVec.push_back(std::make_pair(getShapePosition()[0] + getScalePosition()[0], getShapePosition()[1] + getScalePosition()[1])); //Top Right
	TempVec.push_back(std::make_pair(getShapePosition()[0] + getScalePosition()[0], getShapePosition()[1] - getScalePosition()[1])); //bottom Right
	TempVec.push_back(std::make_pair(getShapePosition()[0] - getScalePosition()[0], getShapePosition()[1] - getScalePosition()[1])); //bottom Left
	return TempVec;
}

