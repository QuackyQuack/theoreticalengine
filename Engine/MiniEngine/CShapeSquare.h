#pragma once
#include <vector>
#include "CShapes.h"


class CShapeSquare :public CShapes
{
public:
	CShapeSquare();
	CShapeSquare(std::pair<float,float> CentrePos,float newXSize, float newYSize, shaderFiles _myShader,bool Blending, std::string Texturepath = "\n");
	//CShapeSquare(std::pair<float, float> CentrePos, float newXSize, float newYSize, bool Textured, bool Blending);
	~CShapeSquare();
	enum NodePos
	{
		NODE_TOP_LEFT,
		NODE_TOP_RIGHT,
		NODE_BOTTOM_RIGHT,
		NODE_BOTTOM_LEFT
	};

	void shutDown();
	int GetIndicesSize();

	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);

	void ChangeColour(float newColour[3]);

	void FlipTextures(bool horizontal, bool vertical);
	void PrepareGraphics(CCamera gameCam);
	void SwapTexture(std::string _tex);
	std::vector<std::pair<int, int>> GetCardinalPoints();
	

private:



	float xSize;
	float ySize;

	GLfloat verticesTex[32]
	{
			//Position				Colour			    Tex co0rds
		1.0f,1.0f,0.0f,			0.0f,1.0f,0.0f,			0.0f,0.0f,//top - Left
		-1.0f,1.0f,0.0f,		0.0f,1.0f,0.0f,			1.0f,0.0f,//top- right
		-1.0f,-1.0f,0.0f,			0.0f,0.0f,1.0f,			1.0f,1.0f,//bot - right
		1.0f,-1.0f,0.0f,			1.0f,0.0f,1.0f,			0.0f,1.0f,//bot - left
	};


	GLuint indices[6] =
	{
		0,1,2,
		0,2,3,
	};
};

