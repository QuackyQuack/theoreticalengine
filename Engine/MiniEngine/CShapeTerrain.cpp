#include <fstream>
#include <sstream>
#include "CShapeTerrain.h"
#include "soil.h"
#include "CGameClass.h"


CShapeTerrain::CShapeTerrain()
{
}

CShapeTerrain::CShapeTerrain(float xStart, float yStart, float zStart, int xSize, float _yScale, int zSize, std::string _Texture1, std::string _Texture2, int tiling_Level, int perlinNoiseSeed)
{
	//_yScale = 1;
	yScale = _yScale;
	TextureBlending = true;
	numRows = xSize;
	numCols = zSize;

	this->tiling_Level = tiling_Level;

	//perlinNoiseSeed = 1 + rand() % (1000);
	this->perlinNoiseSeed = perlinNoiseSeed;

	setScalePosition(glm::vec3(1, 1, 1));
	setShapePosition(glm::vec3(xStart, yStart, zStart));


	heightMapData.resize(xSize * zSize);
	//Build height map
	for (int x = 0; x < xSize; x++)
	{
		for (int z = 0; z < zSize; z++)
		{
			heightMapData[x*numCols + z] = applyPerlinNoiseToPoint(x, z) * _yScale;
		}
	}
	
	//std::cout << tallest;
	

	smoothing();
	buildVertexBuffer();
	buildIndexBuffer();

	int texWidth1, texHieght1;
	int texWidth2, texHieght2;
	tex1 = SetTexture(_Texture1, texHieght1, texWidth1);
	tex2 = SetTexture(_Texture2, texHieght2, texWidth2);

	bool isTextured = setShaderInformation(Shader_TerrainBlending);
	SetDrawMode(GL_TRIANGLES);

	if (isTextured)
	{
		glGenTextures(1, &Texture1);
		glBindTexture(GL_TEXTURE_2D, Texture1);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth1, texHieght1, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex1);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(tex1);
		glBindTexture(GL_TEXTURE_2D, 0);

		glGenTextures(1, &Texture2);
		glBindTexture(GL_TEXTURE_2D, Texture2);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth2, texHieght2, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex2);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(tex2);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	generateBuffers();
}

CShapeTerrain::CShapeTerrain(float xStart, float yStart, float zStart, int xSize,float _yScale, int zSize, std::string texturePath)
{
	yScale = _yScale;
	numRows = xSize;
	numCols = zSize;

	//perlinNoiseSeed = 1 + rand() % (1000);
	perlinNoiseSeed = 52;
	setScalePosition(glm::vec3(1, 1, 1));
	setShapePosition(glm::vec3(xStart, yStart, zStart));


	heightMapData.resize(xSize * zSize);
	//Build height map
	for (int x = 0; x < xSize; x++)
	{
		for (int z = 0; z < zSize; z++)
		{
			heightMapData[x*numCols + z] = applyPerlinNoiseToPoint(x, z) * _yScale;
		}
	}

	smoothing();
	buildVertexBuffer();
	buildIndexBuffer();

	SetTexture(texturePath, texHieght, texWidth);
	bool isTextured = setShaderInformation(Shader_GourandPhongLightingTextureOnly);
	SetDrawMode(GL_TRIANGLES);

	if (isTextured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	generateBuffers();
}

CShapeTerrain::CShapeTerrain(float xStart, float yStart, float zStart, std::string heightMapPath, std::string Texturepath)
{
	setScalePosition(glm::vec3(1, 1, 1));
	setShapePosition(glm::vec3(xStart, yStart, zStart));

	loadHeightMap(heightMapPath);
	smoothing();
	buildVertexBuffer();
	buildIndexBuffer();

	SetTexture(Texturepath, texHieght, texWidth);
	bool isTextured = setShaderInformation(Shader_GourandPhongLightingTextureOnly);
	SetDrawMode(GL_TRIANGLES);

	if (isTextured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indicesTemp.size(), &indicesTemp[0], GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), &vertices[0], GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(8 * sizeof(GLfloat))));
	glEnableVertexAttribArray(3);

}

void CShapeTerrain::generateBuffers()
{
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indicesTemp.size(), &indicesTemp[0], GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), &vertices[0], GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), ((GLvoid*)(8 * sizeof(GLfloat))));
	glEnableVertexAttribArray(3);
}


void CShapeTerrain::loadHeightMap(std::string heightMapPath)
{
	std::vector<unsigned char> in(numRows * numCols);

	std::ifstream inFile;
	inFile.open(heightMapPath, std::ios_base::binary);

	if (inFile)
	{
		// Read the RAW bytes.
		inFile.read((char*)&in[0], (std::streamsize)in.size());

		// Done with file.
		inFile.close();
	}
	else
	{
		Tools::ErrorMessage("heightmap file path is invalid!");
	}

	// Copy the array data into a float array, and scale and offset the heights.
	//heightMapData.resize(numRows * numCols);
	//heightMapData.reserve(numRows * numCols);

	for (int i = 0; i < numRows * numCols; ++i)
	{
		//heightMapData[i] = (float)in[i] * heightScale + heightOffset;
		heightMapData.push_back((float)in[i] * heightScale + heightOffset);
	}
}

void CShapeTerrain::smoothing()
{
	std::vector<float> dest(heightMapData.size());

	for (int i = 0; i < numRows; ++i)
	{
		for (int j = 0; j < numCols; ++j)
		{
			dest[i*numCols + j] = average(i, j);
		}
	}

	// Replace the old heightmap with the filtered one.
	heightMapData = dest;
}

float CShapeTerrain::average(int i, int j)
{
	// Function computes the average height of the ij element.
	// It averages itself with its eight neighbor pixels.  Note
	// that if a pixel is missing neighbor, we just don't include it
	// in the average--that is, edge pixels don't have a neighbor pixel.
	//
	// ----------
	// | 1| 2| 3|
	// ----------
	// |4 |ij| 6|
	// ----------
	// | 7| 8| 9|
	// ----------

	float avg = 0.0f;
	float num = 0.0f;

	for (int m = i - 1; m <= i + 1; ++m)
	{
		for (int n = j - 1; n <= j + 1; ++n)
		{
			if (inBounds(m, n))
			{
				avg += heightMapData[m*numCols + n];
				num += 1.0f;
			}
		}
	}

	return avg / num;
}

bool CShapeTerrain::inBounds(int i, int j)
{
	return(i >= 0 && i < numRows && j >= 0 && j < numCols);
}

void CShapeTerrain::buildVertexBuffer()
{
	vertices.resize(numCols*numRows * 11);

	float halfWidth = (numCols - 1)*cellSpacing*0.5f;
	float halfDepth = (numRows - 1)*cellSpacing*0.5f;

	float invTwoDX = 1.0f / (2.0f*cellSpacing);
	float invTwoDZ = 1.0f / (2.0f*cellSpacing);

	float du = 1.0f / (numCols - 1);
	float dv = 1.0f / (numRows - 1);

	int position = 0;
	int zPos = 0;
	int xPos = 0;

	for (int i = 0; i < numRows; ++i)
	{
		float z = halfDepth - i * cellSpacing;
		zPos--;
		for (int j = 0; j < numCols; ++j)
		{
			float x = -halfWidth + j * cellSpacing;
			xPos--;
			float y = heightMapData[i*numCols + j];

			//Position                i*numCols + j
			vertices[position] = x;// D3DXVECTOR3(x, y, z);
			vertices[position + 1] = y;
			vertices[position + 2] = z;
			//Colour
			vertices[position + 3] = myColour.x;
			vertices[position + 4] = myColour.y;
			vertices[position + 5] = myColour.z;

			//Texture coords
			// Stretch texture over grid.

			vertices[position + 6] = j * du * tiling_Level;
			vertices[position + 7] = i * dv * tiling_Level;

			// temporaryNormals
			vertices[position + 8] = 0.0f;//D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			vertices[position + 9] = 1.0f;
			vertices[position + 10] = 0.0f;

			position += 11;
		}
	}


	rebuildNormals(vertices);

}

void CShapeTerrain::buildIndexBuffer()
{
	int numFaces = (numRows - 1) *(numCols - 1) * 2;

	indicesTemp.resize(numFaces * 3); // 3 indices per face

	// Iterate over each quad and compute indices.
	int k = 0;
	for (int i = 0; i < numRows - 1; ++i)
	{
		for (int j = 0; j < numCols - 1; ++j)
		{
			indicesTemp[k] = i * numCols + j;
			indicesTemp[k + 1] = i * numCols + j + 1;
			indicesTemp[k + 2] = (i + 1)*numCols + j;

			indicesTemp[k + 3] = (i + 1)*numCols + j;
			indicesTemp[k + 4] = i * numCols + j + 1;
			indicesTemp[k + 5] = (i + 1)*numCols + j + 1;

			k += 6; // next quad
		}
	}

}

void CShapeTerrain::rebuildNormals(std::vector<GLfloat> &vertices)
{
	float halfWidth = (numCols - 1)*cellSpacing*0.5f;
	float halfDepth = (numRows - 1)*cellSpacing*0.5f;

	int position = 0;

	for (int i = 0; i < numRows; ++i)
	{
		float z = halfDepth - i * cellSpacing;
		for (int j = 0; j < numCols; ++j)
		{
			float x = -halfWidth + j * cellSpacing;

			bool test;
			float hL = getHeight(x - 1, z, test);
			float hR = getHeight(x + 1, z, test);
			float hD = getHeight(x, z - 1, test);
			float hU = getHeight(x, z + 1, test);


			glm::vec3 newNormal = glm::vec3(hL - hR, 2.0f, hD - hU);
			newNormal = glm::normalize(newNormal);

			vertices[position + 8] = newNormal.x;
			vertices[position + 9] = newNormal.y;
			vertices[position + 10] = newNormal.z;

			position += 11;
		}
	}

}

float CShapeTerrain::getHeight(float x, float z, bool &outofRange)
{
	if (x >= (int)numRows / 2 || x < -(int)numRows / 2 || z >(int)numCols / 2 || z <= -(int)numCols / 2)
	{
		outofRange = true;
		return(0);
	}
	outofRange = false;

	// Transform from terrain local space to "cell" space.
	float c = (x + 0.5f* ((numCols - 1)*cellSpacing)) / cellSpacing;
	float d = (z - 0.5f*((numRows - 1)*cellSpacing)) / -cellSpacing;

	// Get the row and column we are in.
	int row = (int)floorf(d);
	int col = (int)floorf(c);

	// Grab the heights of the cell we are in.
	// A*--*B
	//  | /|
	//  |/ |
	// C*--*D
	if (row*numCols + col >= heightMapData.size() || row * numCols + col + 1 >= heightMapData.size() || (row + 1)*numCols + col >= heightMapData.size() || (row + 1)*numCols + col + 1 >= heightMapData.size())
	{
		outofRange = true;
		return(0);
	}

	float A = heightMapData[row*numCols + col];
	float B = heightMapData[row*numCols + col + 1];
	float C = heightMapData[(row + 1)*numCols + col];
	float D = heightMapData[(row + 1)*numCols + col + 1];

	// Where we are relative to the cell.
	float s = c - (float)col;
	float t = d - (float)row;

	// If upper triangle ABC.
	if (s + t <= 1.0f)
	{
		float uy = B - A;
		float vy = C - A;
		return A + s * uy + t * vy;
	}
	else // lower triangle DCB.
	{
		float uy = C - D;
		float vy = B - D;
		return D + (1.0f - s)*uy + (1.0f - t)*vy;
	}
}

//Assumes that the geometry map will be the same size as the height map itself
//Spacing denotes the number of pixels skipped after each geometry shader. Higher spacing denotes higher performance
void CShapeTerrain::generateGeometryOnTerrain(CGeometryObject &objectToPlace, int numOfObjects, float lowRange, float hiRange)
{
	int maxPositions = sizeof(vertices)/11;
	for (int x = 0; x < numOfObjects; x++)
	{
		int pos =   rand() % ((numRows + 1));
		float val = ((((vertices[(pos * 11) + 1] + 1) / 2) / yScale) / persistance);
		//((fragPos[1] + 1)/2)/terrainHeightVal)/perlinPersistance
		while (val < lowRange || val > hiRange)
		{
			pos = rand() % ((numRows + 1));
			val = ((((vertices[(pos * 11) + 1] + 1) / 2) / yScale) / persistance);
		}

		objectToPlace.addDrawPosition(glm::vec3(vertices[(pos * 11) + 0], vertices[(pos * 11) + 1], vertices[(pos * 11) + 2]));

	}


	/*
	for (int x_ = 0; x_ < numRows; x_ += 1)
	{
		for (int z_ = 0; z_ < numCols; z_++)
		{
			if (geoMap[x_*numCols + z_] == 255 && spaceCounter > spacing)
			{
				CGeometryObject *newGeo = new CGeometryObject(objectToPlace);
				newGeo->setShapePosition(glm::vec3((x_ - numRows / 2) + (shapePosition.x * scale.x), newGeo->getScalePosition().y + getHeight((x_ - numRows / 2), (z_ - numCols / 2), test) - (shapePosition.y * scale.y), (z_ - numCols / 2) + (shapePosition.z * scale.z)));

				spaceCounter = 0;

				if (!CGameClass::getInstance().addShape(newGeo))
				{
					delete newGeo;
					newGeo = 0;
				}
				//	z_ +=spacing;
			}
			else
			{
				spaceCounter++;
			}

		}
		spaceCounter = 0;

		//curx++;
	}
	*/

	//WORKS
	/*CGeometryObject *newGeo = new CGeometryObject(objectToPlace);
	newGeo->setShapePosition(glm::vec3(shapePosition.x * scalePosition.x + 0, shapePosition.y * scalePosition.y + getHeight(20, 20, test) + newGeo->getScalePosition().y, shapePosition.z * scalePosition.z + 0));

	//CGameClass::getInstance().createShapeCube(shapePosition.x * scalePosition.x + 0, shapePosition.y * scalePosition.y + getHeight(20, 20, test), shapePosition.z * scalePosition.z + 0, 10, 10, 10, Shader_PureVertex, false);
	std::cout << newGeo->getShapePosition().x << ", " << newGeo->getShapePosition().y << ", " << newGeo->getShapePosition().z << std::endl;
	CGameClass::getInstance().addShape(newGeo);*/

}

float CShapeTerrain::perlinRandomPoint(int x, int z)
{
	//Original
	//int n = x + z * 52;
	int n = x + z * perlinNoiseSeed;
	n = (n << 13) ^ n;
	int t = (n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff;
	return float(1.0 - double(t) * 0.931322574615478515625e-9);
}

float CShapeTerrain::perlinSmoothing(int x, int z)
{
	float corners;
	float sides;
	float center;
	corners = (perlinRandomPoint(x - 1, z - 1) + perlinRandomPoint(x + 1, z - 1) + perlinRandomPoint(x - 1, z + 1) + perlinRandomPoint(x + 1, z + 1)) / 16;
	sides = (perlinRandomPoint(x - 1, z) + perlinRandomPoint(x + 1, z) + perlinRandomPoint(x, z - 1) + perlinRandomPoint(x, z + 1)) / 8;
	center = perlinRandomPoint(x, z) / 4;
	return corners + sides + center;
}

float CShapeTerrain::perlinInterpolate(float a, float b, float x)
{
	return (a*(1 - x) + b * x);
}

float CShapeTerrain::perlinNoise(float x, float z)
{
	float fractional_X = x - int(x);
	float fractional_z = z - int(z);
	//smooths
	float v1 = perlinSmoothing(int(x), int(z));
	float v2 = perlinSmoothing(int(x) + 1, int(z));
	float v3 = perlinSmoothing(int(x), int(z) + 1);
	float v4 = perlinSmoothing(int(x) + 1, int(z) + 1);
	// interpolates
	float i1 = perlinInterpolate(v1, v2, fractional_X);
	float i2 = perlinInterpolate(v3, v4, fractional_X);
	return perlinInterpolate(i1, i2, fractional_z);
}

float CShapeTerrain::applyPerlinNoiseToPoint(int x, int z)
{
	/*int octaves = 8;
	float zoom = 20.0f;
	float persistance = 0.3f;
	float total = 0.0f;

	for (int i = 0; i < octaves - 1; i++) {
		float frequency = pow(2, i) / zoom;
		float amplitude = pow(persistance, i);
		total += perlinNoise(x * frequency, z * frequency) * amplitude;
	}
	return total;*/

	int octaves = 8;
	float zoom = 100.0f;
	
	float total = 0.0f;

	for (int i = 0; i < octaves - 1; i++) {
		float frequency = float(pow(2, i) / zoom);
		float amplitude = pow(persistance, i);
		total += perlinNoise(x * frequency, z * frequency) * amplitude;
	}

	/*if (total <= 0.0f)
	{
		std::cout << "YES" << std::endl;
	}*/

	
	return total;
}


void CShapeTerrain::SwapTexture(std::string _tex)
{
}

void CShapeTerrain::ChangeColour(float newColour[3])
{
}

void CShapeTerrain::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

int CShapeTerrain::GetIndicesSize()
{
	return (sizeof(GLuint) * indicesTemp.size());
}

int CShapeTerrain::getNoOfIndices()
{
	return indicesTemp.size();
}

void CShapeTerrain::passTextureData()
{
	if (!TextureBlending)
	{
		glUseProgram(getMyProgram());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, myTexture);
		glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);
	}
	else
	{
		glUseProgram(getMyProgram());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture1);
		glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, Texture2);
		glUniform1i(glGetUniformLocation(getMyProgram(), "tex2"), 1);

		glUniform1f(glGetUniformLocation(getMyProgram(), "terrainHeightVal"), yScale);
		glUniform1f(glGetUniformLocation(getMyProgram(), "perlinPersistance"), persistance);
		glUniform1f(glGetUniformLocation(getMyProgram(), "blendingWeight"), textureBlendingOffset);
		glUniform1f(glGetUniformLocation(getMyProgram(), "blendingSpeed"), blendingSpeed);
	}


	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());

}

void CShapeTerrain::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
	}

	glm::vec2 screenSize = glm::vec2(WINDOW_WIDTH, WINDOW_HIEGHT);

	float tempLODFactor = 4.0f;

	glUniform1f(glGetUniformLocation(myProgram, "lod_factor"), tempLODFactor);
	glUniformMatrix2fv(glGetUniformLocation(myProgram, "screen_size"), 1, GL_FALSE, glm::value_ptr(screenSize));

	/*glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);
	//pass in alpha value
	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());*/

	passTextureData();

}

void CShapeTerrain::FlipTextures(bool horizontal, bool vertical)
{
}

std::vector<std::pair<int, int>> CShapeTerrain::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
int CShapeTerrain::getNumRows()
{
	return numRows;
}
int CShapeTerrain::getNumCols()
{
	return numCols;
}
GLfloat CShapeTerrain::getVertices(int _position)
{
	return vertices[_position];
}
GLuint CShapeTerrain::getIndices(int _position)
{
	return indicesTemp[_position];
}
std::vector<GLfloat>& CShapeTerrain::returnVertices()
{
	return vertices;
}
std::vector<GLuint>& CShapeTerrain::returnIndices()
{
	return indicesTemp;
}
void CShapeTerrain::shutDown()
{
	heightMapData.clear();
}

CShapeTerrain::~CShapeTerrain()
{

}

