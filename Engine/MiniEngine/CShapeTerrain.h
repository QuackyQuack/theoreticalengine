#pragma once
#include <ostream>
#include "CShapes.h"
#include "CGeometryObject.h"

class CShapeTerrain : public CShapes
{
public:
	CShapeTerrain();
	//Generate heightmap with perlin noise
	CShapeTerrain(float xStart, float yStart, float zStart , int xSize, float yScale, int zSize, std::string texturePath);
	// load heightmap from greyscale image
	CShapeTerrain(float xStart, float yStart, float zStart, std::string heightMapPath, std::string texturePath);


	//Generate heightmap with perlin noise
	// Use that same perlin noise to change the texture of the terrain.
	CShapeTerrain(float xStart, float yStart, float zStart, int xSize, float yScale, int zSize, std::string Texture1, std::string Texture2,int tiling_Level, int perlinNoiseSeed);

	~CShapeTerrain();

	void generateBuffers();


	//Takes x and Y coords to return the height of the map at that point
	float getHeight(float x, float z, bool &outofRange);

	//Takes an object and set points to draw
	//Low range and highrange determin where the objects will be placed within the terrain
	// -1 is lowest possible point, 1 is highest possible point
	void generateGeometryOnTerrain( CGeometryObject &objectToPlace, int numOfObjects, float lowRange = -1.f, float hiRange = 1.0f);

	//Perlin noise functions
	float perlinRandomPoint(int x, int z);
	float perlinSmoothing(int x, int z);
	float perlinInterpolate(float a, float b, float x);
	float perlinNoise(float x, float z);
	float applyPerlinNoiseToPoint(int x, int z);

	//OverLoaded funcions
	void shutDown();
	void SwapTexture(std::string _tex);
	void ChangeColour(float newColour[3]);
	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]) ;
	int GetIndicesSize();
	int getNoOfIndices();
	
	//Overload to compensate for texture blending
	void passTextureData() override;


	void PrepareGraphics(CCamera gameCam);
	void FlipTextures(bool horizontal, bool vertical);
	std::vector<std::pair<int, int>> GetCardinalPoints();

	int getNumRows();
	int getNumCols();

	//Used for mouse picking
	GLfloat getVertices(int _position);
	GLuint getIndices(int _position);

	std::vector<GLfloat>& returnVertices();
	std::vector<GLuint>& returnIndices();
	


private:


	void loadHeightMap(std::string heightMapPath);
	void smoothing();
	float average(int i, int j);
	bool inBounds(int i, int j);
	void buildVertexBuffer();
	void buildIndexBuffer();

	void rebuildNormals(std::vector<GLfloat> &vertices);

	//Vars that need to be made dynamic eventually
	//Size of the image
	int numRows = 513;
	int numCols = 513;
	float cellSpacing = 1.f;
	glm::vec3 myColour = glm::vec3(0.f, 0.f, 1.f);

	//How much tiling the texture does (1 = Image covers the entire mesh)
	int tiling_Level = { 1 };

	int perlinNoiseSeed = 0;

	float heightScale = 0.35f;
	float heightOffset = -20.0f;

	std::vector<GLfloat> vertices;
	std::vector<GLuint> indicesTemp;

	//Actual positional data of the terrain
	std::vector<float> heightMapData;

	float heightMapWidth;
	float heightMapHeight;

	int texWidth, texHieght; // texture params

	GLuint myTexture;

	//Used for texture blending
	bool TextureBlending = { false };
	unsigned char* tex1;
	GLuint Texture1;
	unsigned char* tex2;
	GLuint Texture2;

	float heightMapImageSize;

	//Storeage of the actual height map
	unsigned char* bitmapImage;

	float yScale = 1;;
	float persistance = 0.3f;

	//0 = no change
	//0.5f = second texture has 50% more weightage
	//-0.5f = first texture has 50% more weightage
	float textureBlendingOffset = 0.90f;

	// 1.1025 = no change at all
	// 0.5 = change pushed up 

	//This value affects how quickly the blending between texture one and two takes place
	// 0 no change
	// 1 change happends instantly
	// 0.5 change is 50% faster
	float blendingSpeed = 0.0f;
	//NOTE SHADER IS CURRENTLY NOT USING BLEND SPEED BECAUSE SHADER NEEDS TWEAKING
};

