//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : CShapes.cpp
// Description     : Base class for all shapes
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//

#include <string>
#include "CShapes.h"
#include "soil.h"
#include "CGameClass.h"



static std::map<unsigned char*, std::string> textureMap;

CShapes::CShapes()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

}


CShapes::~CShapes()
{
	//shutDown();
}


// Function Name   : GetVertexAtrib
// Description     : Returns a user chosen vertex
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Used for openGL render functionality
//
GLuint& CShapes::GetVertexAtrib(VertixAtribType vertexType) //vertexType is an enum relating to the three types of atributes used
{
	switch (vertexType)
	{
		case(VAT_VAO):
		{
			return(VAO);
		}
		case(VAT_VBO):
		{
			return(VBO);
		}
		case(VAT_EBO):
		{
			return(EBO);
		}
		default:return(VAO);
	}
}

// Function Name   : GetVertexAtrib
// Description     : Returns a user chosen vertex
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Used for openGL render functionality
//
void CShapes::setMyProgram(GLuint newProgram) //Sets object program to passed in type
{
	myProgram = newProgram;
}

GLuint CShapes::getMyProgram()
{
	return (myProgram);
}

// Function Name   : SetTexture
// Description     : Sets the texture of an object
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : Currently only works as one texture per object, may change.

unsigned char* CShapes::SetTexture(std::string imageName, int &hieght, int &width, bool overwrite) // ImageName	      : string name of image to set as texture, see overwrite for more.
{																						 // hieght and width  : Both are used by SOIL to load images, passed in as refrence's and used later for OpenGL texturing
	if (overwrite)																		 // overwrite         : Boolean set to true when user wants to load an image from outside of the predifined images folder. If true user must provide whole filepath as apposed to only the file name if set to false. Defaults to false
	{
		TexturePath = imageName;
	}
	else
	{
		TexturePath += imageName;
	}


	texture = createTexture(TexturePath.c_str(), width, hieght); //SOIL_load_image(TexturePath.c_str() , &width, &hieght, 0, SOIL_LOAD_RGBA);
	
	currentImageName = imageName;
	TexturePath = "Resources/Images/";
	return(texture);
}

unsigned char* CShapes::GetTexture()
{
	return (texture);
}

int CShapes::GetDrawMode()
{
	return drawMode;
}

void CShapes::SetDrawMode(int newDrawMode)
{
	drawMode = newDrawMode;
}

// Function Name   : matriceUpdates
// Description     : Updates the trs matrices of each shape
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : On each update call for each object
void CShapes::matriceUpdates(CCamera renderCam) //gameCam : Camera used to render object
{
	//Stuff always passed to the shader
	//Camera Pos
	glUniform3fv(glGetUniformLocation(myProgram, "cameraPos"), 1, glm::value_ptr(renderCam.getCamPos()));
	glEnable(GL_CULL_FACE);
	//Geometry
	glm::mat4 transMat = glm::translate(glm::mat4(), shapePosition);

	glm::mat4 roll = glm::rotate(glm::mat4(1.0f), -rotationPosition[2], glm::vec3(0.0f, 0.0f, 1.0f));
	glm::mat4 pitch = glm::rotate(glm::mat4(1.0f), -rotationPosition[0], glm::vec3(1.0f, 0.0f, 0.0f));
	glm::mat4 yaw = glm::rotate(glm::mat4(1.0f), -rotationPosition[1], glm::vec3(0.0f, 1.0f, 0.0f));

	glm::mat4 scalar = glm::scale(glm::mat4(), scale);
	glm::mat4 modelMatrix = transMat * (roll * pitch * yaw)  * scalar;
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
	glm::mat4 mvp = renderCam.getProjMatr() * renderCam.getViewMatr()* modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "mvp"), 1, GL_FALSE, glm::value_ptr(mvp));

	//Call rendering functions needed to draw the shape
	for (std::function<void()> renderFunc : myRenderingFunctions)
	{
		renderFunc();
	}
}

//Sets the shapes new position
void CShapes::setShapePosition(glm::vec3 newVec)
{
	shapePosition = newVec;
}

//Sets the shapes new rotation axis
void CShapes::setRotationPosition(glm::vec3 newVec)
{
	rotationPosition = newVec;
}


//Sets the shapes new scale axis
void CShapes::setScalePosition(glm::vec3 newVec)
{
	scale = newVec;
}

//Gets the shapes position
glm::vec3 CShapes::getShapePosition()
{
	return shapePosition;
}


//Gets the shapes scale axis
glm::vec3 CShapes::getScalePosition()
{
	return scale;
}

//sets whether or not the shape has blending enabled
void CShapes::setBlending(bool _blend)
{
	m_blending = _blend;
}

//returns whether or not the shape has blending enabled
bool CShapes::isBlending()
{
	return m_blending;
}

//sets blending options of shape
void CShapes::setBlendingOptions(int Option1, int Option2)
{
	blendingOptions.first = Option1;
	blendingOptions.second = Option2;
}

//gets blending options of shape
std::pair<int, int> CShapes::getBlendingOptions()
{
	return blendingOptions;
}

//sets alpha value of shape
void CShapes::setAlpha(float _alpha)
{
	alphaValue = _alpha;
}

//gets alpha value of shape
float CShapes::getAlpha()
{
	return alphaValue;
}

bool CShapes::textureAlreadyLoaded(std::string _test)
{
	if (_test == currentImageName)
	{
		return(true);
	}
	return false;

}

void CShapes::setStencilInformation(bool _stencil, bool is_Hidden)
{
	stenciled = _stencil;
	stenciledOut = is_Hidden;

	if (stenciled)
	{
		myRenderingFunctions.push_back(std::bind(&CShapes::passStencilData, this));
	}
}

bool CShapes::setShaderInformation(shaderFiles _shaders)
{
	switch (_shaders)
	{
	case(Shader_PureVertex):
	{
		//Untextured square using the vertex information for colour
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad UnTex.frg"));
		return(false);
		break;
	}
	case(Shader_TextureOnly):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad Tex.frg"));

		return(true);
		break;
	}
	case(Shader_CubeMap):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad CubeMap.vs", "Resources/ShaderFiles/shad CubeMap.frg"));
		return(true);
		break;
	}
	case(Shader_BaseLightingNoTexture):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad lighting.vs", "Resources/ShaderFiles/shad UnTex.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));

		return(false);
		break;
	}
	case(Shader_GourandPhongLightingTextureOnly):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad lighting.vs", "Resources/ShaderFiles/shad lightingGouraudPhongTEX.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		return(true);
		break;
	}
	case(Shader_PhongLigtingTextureOnly):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad lighting.vs", "Resources/ShaderFiles/shad lightingPhongTEX.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		return(true);
		break;
	}
	case(Shader_RimLightingTextureOnly):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad lighting.vs", "Resources/ShaderFiles/shad lightingRimTEX.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		return(true);
		break;
	}
	case(Shader_Model):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Model.vs", "Resources/ShaderFiles/shad Model.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		return(true);
		break;
	}
	case(Shader_Reflections):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad lighting.vs", "Resources/ShaderFiles/shad Reflection.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		return(true);
		break;
	}
	case(Shader_TextLabels):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad TextLabel.vs", "Resources/ShaderFiles/shad TextLabel.frg"));
		return(true);
		break;
	}
	case(Shader_Fog):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Fog.vs", "Resources/ShaderFiles/shad Fog.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		myRenderingFunctions.push_back(std::bind(&CShapes::passFog, this));
		return(true);
		break;
	}
	case(Shader_GenericParticleSystem):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/ParticleShaders/particle_Generic.vs", "Resources/ShaderFiles/ParticleShaders/particle_Generic.frg", "Resources/ShaderFiles/ParticleShaders/particle_Generic.gs"));
		break;
	}
	case(Shader_TerrainBlending):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad lighting.vs", "Resources/ShaderFiles/shad TexBlendingWLight.frg"));
		myRenderingFunctions.push_back(std::bind(&CShapes::passLightingData, this));
		return(true);
		break;
	}
	case(Shader_FireParticleSystem):
	{
		setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/ParticleShaders/particle_Generic.vs", "Resources/ShaderFiles/ParticleShaders/particle_Fire.frg", "Resources/ShaderFiles/ParticleShaders/particle_Fire.gs"));
		break;
	}
	default:
	{
		Tools::ErrorMessage("Shader file is not loaded correctly in square!");
	}
	}
	return(false);
}

bool CShapes::isTesselated()
{
	return tesselated;
}

void CShapes::PrepareGraphics(CCamera renderCam)
{
	matriceUpdates(renderCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	}

	//this->passTextureData();
}

void CShapes::start()
{
	//Do nothing (for now)
	setPRStoGameObjectPRS();
}

void CShapes::update()
{
//	std::cout<<( "Shape is updating");
	if (myGameObject != nullptr && myGameObject->getPRS_Changed())
	{
		setPRStoGameObjectPRS();
	}
}

void CShapes::setPRStoGameObjectPRS()
{
	if (keepGameObjectShape && myGameObject != nullptr)
	{
		shapePosition = myGameObject->getPosition();
		rotationPosition = myGameObject->getObjRot();
		scale = myGameObject->getScale();
	}
}

void CShapes::Render()
{
	glUseProgram(this->getMyProgram()); //Fetch current shapes program
	glBindVertexArray(VAO);             // Bind vertex

	if (!dynamic_cast<CShapeSquare*>(this) && !dynamic_cast<CShapeTriangle*>(this) && !dynamic_cast<CShapeLine*>(this)) //Render 2d objects (used in UI) with orthagraphic camera
	{
		glDepthFunc(GL_LESS);
		PrepareGraphics(CGameClass::getInstance().getGameCam());							//Update textures ect..
	}
	else
	{
		glDepthFunc(GL_ALWAYS);
		PrepareGraphics(CGameClass::getInstance().getUICam());							//Update textures ect..
	}

	//Geometry shaders
	if (dynamic_cast<CGeometryObject*>(this))
	{
		glDrawArrays(GL_POINTS, 0, 1);
	}
	else
	{
		if (this->isTesselated())
		{
			glDrawArrays(GL_PATCHES, 0,this->GetIndicesSize());
		}
		else
		{
			glDrawElements(this->GetDrawMode(), this->GetIndicesSize(), GL_UNSIGNED_INT, 0); // Draw shape
		}
	}

	//Clear and prepair for next draw
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_BLEND);
	glDisable(GL_STENCIL_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glBlendEquation(GL_FUNC_ADD);
	glUseProgram(0);
}

void CShapes::passTextureData()
{
	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}

glm::vec3 CShapes::getRotation()
{
	return rotationPosition;
}

unsigned char* CShapes::createTexture(std::string texPath, int & width, int & height)
{
	//Check if texture already exsits somewhere
	std::map<unsigned char*, std::string>::iterator it;


	//TO DO fix this breaking :<
	/*for (it =textureMap.begin(); it !=textureMap.end(); ++it)
	{
		if (it->second == texPath)
		{
			return(it->first);
		}
	}*/
	//if not create texture and return it.

	unsigned char* newTexture = SOIL_load_image(TexturePath.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);

	textureMap[newTexture] = texPath;

	return(newTexture);
	//return GLuint();
}


void CShapes::passLightingData()
{
	//Lighting
	glUniform1f(glGetUniformLocation(myProgram, "ambientStr"), CGameClass::getInstance().getWorldLightStrength());

	glUniform3fv(glGetUniformLocation(myProgram, "lightColor"), 1, glm::value_ptr(CGameClass::getInstance().getWorldLightColour()));
	glUniform3fv(glGetUniformLocation(myProgram, "lightPos"), 1, glm::value_ptr(CGameClass::getInstance().getWorldLightPosition()));
	//This breaks the shader!!
	//glm::mat3 inverseModel = glm::mat3(glm::transpose(glm::inverse(modelMatrix)));
	//glUniform3fv(glGetUniformLocation(myProgram, "inversModel"), 1, glm::value_ptr(inverseModel));

}

//Global lighting must be added to shape's rendering function to be utilised.
void CShapes::passGlobalLighingData()
{
	//Global lighting 
	glUniform1i(glGetUniformLocation(myProgram, "totalLights"), CGameClass::getInstance().activeLights);
	for (int x = 0; x < CGameClass::getInstance().activeLights; x++)
	{
		glUniform3f(glGetUniformLocation(myProgram, ("worldLights[" + std::to_string(x) + "].ambientColor").c_str()), CGameClass::getInstance().LightLists[x]->ambientColor.x, CGameClass::getInstance().LightLists[x]->ambientColor.y, CGameClass::getInstance().LightLists[x]->ambientColor.z);
		glUniform3f(glGetUniformLocation(myProgram, ("worldLights[" + std::to_string(x) + "].lightColor").c_str()), CGameClass::getInstance().LightLists[x]->lightColor.x, CGameClass::getInstance().LightLists[x]->lightColor.y, CGameClass::getInstance().LightLists[x]->lightColor.z);
		glUniform3f(glGetUniformLocation(myProgram, ("worldLights[" + std::to_string(x) + "].lightPos").c_str()), CGameClass::getInstance().LightLists[x]->lightPos.x, CGameClass::getInstance().LightLists[x]->lightPos.y, CGameClass::getInstance().LightLists[x]->lightPos.z);
		glUniform1f(glGetUniformLocation(myProgram, ("worldLights[" + std::to_string(x) + "].lightSpecStr").c_str()), CGameClass::getInstance().LightLists[x]->lightSpecStr);
		glUniform1f(glGetUniformLocation(myProgram, ("worldLights[" + std::to_string(x) + "].shininess").c_str()), CGameClass::getInstance().LightLists[x]->shininess);
	}
}

void CShapes::passFog()
{
	//Linear fog
	glUniform1f(glGetUniformLocation(myProgram, "fogStart"), CGameClass::getInstance().getFogstart());
	glUniform1f(glGetUniformLocation(myProgram, "fogRange"), CGameClass::getInstance().getFogDepth());
}


void CShapes::passStencilData()
{
		//Stencling information
		glEnable(GL_STENCIL_TEST);
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		if (stenciledOut)
		{
			glStencilMask(0x00);
			glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
		}
		else
		{
			glStencilMask(0xFF);
			glStencilFunc(GL_ALWAYS, 1, 0xFF);
		}
}
