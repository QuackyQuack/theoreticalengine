#pragma once

#include <functional>
#include <string>
#include <vector>
#include <glew.h>
#include <freeglut.h>

//#include <map>

#include "Component.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "CCamera.h"
#include "CShaderChooser.h"


class CShapes : public component
{
public:
	enum VertixAtribType
	{
		VAT_VAO,
		VAT_VBO,
		VAT_EBO
	};

	CShapes();
	~CShapes();
	GLuint& GetVertexAtrib(VertixAtribType vertexType);

	//General Functions
	void setMyProgram(GLuint newProgram);
	GLuint getMyProgram();
	unsigned char* SetTexture(std::string imageName,int &height, int &width, bool overwrite = false);
	unsigned char* GetTexture();
	int GetDrawMode();
	void SetDrawMode(int newDrawMode);

	//Geometric matrices
	void matriceUpdates(CCamera renderCam);

	void setShapePosition(glm::vec3 newVec);
	void setRotationPosition(glm::vec3 newVec);
	void setScalePosition(glm::vec3 newVec);

	glm::vec3 getShapePosition();
	glm::vec3 getRotation();
	glm::vec3 getScalePosition();

	//Determins if the shape will run blending functions or not
	void setBlending(bool _blend);
	//Is this shape blending?
	bool isBlending();

	// takes two gl-blend enums as input to determin blending of shape
	void setBlendingOptions(int Option1, int Option2);
	//Returns gl-blend enums used on this shape
	std::pair<int, int> getBlendingOptions();

	//Sets alpha value of the shape
	void setAlpha(float _alpha);
	//Returns alpha value of the shape
	float getAlpha();
	//Checks if texture is already on the shape
	bool textureAlreadyLoaded(std::string _test);

	//Sets information regarding stenciling objects
	void setStencilInformation(bool stencil, bool is_Hidden);

	//Takes a shader file enum, sets the shapes program accordingly, returns a boolean regarding whether or not the shape has a texture
	bool setShaderInformation(shaderFiles _shaders);

	bool isTesselated();

	void Render();

	virtual void passTextureData();
	//Functions to be overidden

	virtual void shutDown() = 0;
	virtual void SwapTexture(std::string _tex) = 0;
	virtual void ChangeColour(float newColour[3]) = 0;
	virtual void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3],float _newColour2[3],float _newColour3[3],  float _newColour4[3]) = 0;
	virtual int GetIndicesSize() = 0;
	virtual void PrepareGraphics(CCamera gameCam) = 0;
	virtual void FlipTextures(bool horizontal, bool vertical) = 0;
	virtual std::vector<std::pair<int, int>> GetCardinalPoints() = 0;

	//Functions overloaded
	void start();
	void update();

	//Determins if prs is the same as the attached game object
	bool keepGameObjectShape = {true};
	void setPRStoGameObjectPRS();


protected:
	//Purely graphics

	//Graphics functions called during render
	std::vector<std::function<void()>> myRenderingFunctions;

	GLuint myProgram;
	GLuint VAO;
	GLuint VBO;
	GLuint EBO;

	shaderFiles myShader;

	bool stenciled = false; //Determins if 	glEnable(GL_STENCIL_TEST); will trigger on this object
	bool stenciledOut = false;// Determins whether this shape is masked out or the one which appears on top. The object with this enabled should be the larger of the two  ALSO very important, the shape with this option DISSABLED must be drawn FIRST

	float currentColour[3] = { 0.0f, 0.0f, 0.0f };

	glm::vec3 shapePosition = glm::vec3(0.0f, 0.0f, 0.0f);
	//Rotation
	glm::vec3 rotationPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 scale = glm::vec3(100.0f, 100.0f, 100.0f);

	float alphaValue = 1.0f; //Used for blending, ect

	GLuint myTexture;
	int texWidth, texHieght; // texture params
	std::string TexturePath = "Resources/Images/";
	std::string currentImageName;//Tracks which image is currently loaded

	unsigned char* texture;
	int drawMode; // How polygon is drawn, eg GL_TRIANGLE, GL_LINE ect

	//is the shape transparent
	bool m_blending = false; 
	std::pair<int, int> blendingOptions = {std::make_pair(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)}; //Parameters used for blending

	bool tesselated = false;

	//create texture, if texture has already been created, return it.
	unsigned char* createTexture(std::string texPath, int& width, int& height);


private:
	void passLightingData();
	void passGlobalLighingData();
	void passFog();
	void passStencilData();
};

