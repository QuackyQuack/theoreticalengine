#include "CShapesCubeMap.h"
#include "soil.h"
#include "ShaderLoader.h"
CShapesCubemap::CShapesCubemap()
{
}

CShapesCubemap::CShapesCubemap(CCamera * _gameCam, std::vector<std::string> filePaths)
{
	setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad CubeMap.vs", "Resources/ShaderFiles/shad CubeMap.frg"));

	setShapePosition(glm::vec3(0.f, 0.f, 0.f));
	setScalePosition(glm::vec3(5000.0f, 5000.0f, 5000.0f));

	SetDrawMode(GL_TRIANGLES);

	glGenTextures(1, &Texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, Texture);

	//Load textures
	for (GLuint i = 0; i < 6; i++)
	{
		SetTexture(filePaths[i],texHieght,texWidth);

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0
			, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);
		SOIL_free_image_data(texture);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_LINEAR);


	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);

	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

CShapesCubemap::~CShapesCubemap()
{
}

void CShapesCubemap::ChangeColour(float newColour[3])
{
}

void CShapesCubemap::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

int CShapesCubemap::GetIndicesSize()
{
	return sizeof(indices) / sizeof(indices[0]);
}

void CShapesCubemap::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	glUseProgram(getMyProgram());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, Texture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "cubeSampler"), 0);	
}

void CShapesCubemap::FlipTextures(bool horizontal, bool vertical)
{
}

void CShapesCubemap::shutDown()
{
}

std::vector<std::pair<int, int>> CShapesCubemap::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}

GLuint CShapesCubemap::getCubemap()
{
	return Texture;
}

void CShapesCubemap::SwapTexture(std::string _tex)
{
}
