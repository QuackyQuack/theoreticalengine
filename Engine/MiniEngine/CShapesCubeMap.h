#include <vector>

#include "CShapes.h"

#pragma once
class CShapesCubemap : public CShapes
{
public:
	CShapesCubemap();
	CShapesCubemap(CCamera* _gameCam, std::vector<std::string> filePaths);
	~CShapesCubemap();

	void ChangeColour(float newColour[3]);
	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	int GetIndicesSize();
	void PrepareGraphics(CCamera gameCam);
	void FlipTextures(bool horizontal, bool vertical);
	void shutDown();
	std::vector<std::pair<int, int>> GetCardinalPoints();

	GLuint getCubemap();
	void SwapTexture(std::string _tex);
private:
	GLuint Texture;
	GLfloat verticesTex[96]
	{
		//Position			

		//Front
		-1.f,1.f,1.f,				
		-1.f,-1.f,1.f,				
		1.f,-1.f,1.f,				
		1.f,1.f,1.f,				
		//Back
		-1.f,1.f,-1.f,				
		-1.f,-1.f,-1.f,				
		1.f,-1.f,-1.f,				
		1.f,1.f,-1.f,				
		//Left
		-1.f,1.f,-1.f,				
		-1.f,-1.f,-1.f,				
		-1.f,-1.f,1.f,				
		-1.f,1.f,1.f,				
		//Right
		1.f,1.f,1.f,				
		1.f,-1.f,1.f,				
		1.f,-1.f,-1.f,				
		1.f,1.f,-1.f,		
		
		//Top
		-1.f,1.f,-1.f,				
		-1.f,1.f,1.f,				
		1.f,1.f,1.f,				
		1.f,1.f,-1.f,				
		//Bottom
		-1.f,-1.f,1.f,				
		-1.f,-1.f,-1.f,				
		1.f,-1.f,-1.f,				
		1.f,-1.f,1.f,				
	};

	int texWidth, texHieght; // texture params
	GLuint indices[36] =
	{
		2,1,0,
		3,2,0,

		5,6,7,
		4,5,7,

		10,9,8,
		11,10,8,

		14,13,12,
		15,14,12,

		19,18,16,
		18,17,16,

		23,22,20,
		22,21,20,
	};
};