#include "CShapesTriangle.h"
#include "ShaderLoader.h"
#include "soil.h"

CShapeTriangle::CShapeTriangle()
{
}

// Function Name   : CShapeTriangle constructor
// Description     : Creates a 2D Triangle
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
CShapeTriangle::CShapeTriangle(std::pair<float, float> CentrePos, int xDist, int yDist, shaderFiles _myShader, bool Blending, std::string Texturepath)
// CentrePos          : 'Centre' of the square in screen space from -1.0 to 1.0 for each memebr of the pair
// xDist              : Size of shape in the xDirection
// yDist			  : Size of shape in the yDirection
// Textured			  : Bool as to whether the shape has a texture or not
// Blending           : Bool as to whether the shape should be blended or not
// Texturepath        : Path to a texture if present, defautts to a null value which is ignored
{

	if (Texturepath != "\n")																														 
	{																																				 		  
		SetTexture(Texturepath, texHieght, texWidth);
	}

	myShader = _myShader;

	setScalePosition(glm::vec3(xDist, yDist, 0.0));
	setShapePosition(glm::vec3(CentrePos.first, CentrePos.second, 0.0));

	bool isTextured = setShaderInformation(_myShader);
	SetDrawMode(GL_TRIANGLES);

	if (isTextured)
	{
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);

		//unsigned char* texDisplay = SOIL_load_image("Resources/Images/sorrycody.png", &texWidth, &texHieght, 0, SOIL_LOAD_RGBA);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(GetTexture());
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);

	setBlending(Blending);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

CShapeTriangle::~CShapeTriangle() 
{
}

void CShapeTriangle::shutDown()
{
}

// Function Name   : GetIndicesSize
// Description     : Returns size of shape's indices
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
int CShapeTriangle::GetIndicesSize()
{
	return (sizeof(indices) / sizeof(indices[0]));
}

// Function Name   : PrepareGraphics
// Description     : Makes sure shape can be drawn
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeTriangle::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		//glBlendFunc(GL_ONE, GL_ONE);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
	}

	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);

	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}
// Function Name   : ChangeColour
// Description     : Changes colour of the shape
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeTriangle::ChangeColour(float newColour[3])
{
	for (int x = 0; x < 3; x++)
	{
		verticesTex[3 + (8 * x)] = newColour[0];
		verticesTex[4 + (8 * x)] = newColour[1];
		verticesTex[5 + (8 * x)] = newColour[2];
	}

	//Reset OGL information

	setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad Basic.vs", "Resources/ShaderFiles/shad UnTex.frg"));

	//Set up our EBO
	glGenBuffers(1, &GetVertexAtrib(VAT_EBO));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetVertexAtrib(VAT_EBO));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTex), verticesTex, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(3 * sizeof(GLfloat))));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((GLvoid*)(6 * sizeof(GLfloat))));
	glEnableVertexAttribArray(2);
}

// Function Name   : ChangeColourWithArray
// Description     : Changes specific colours of Chosen points.
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz 
// Usage           : 
void CShapeTriangle::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
//nodeNums    - Nodes to be changed, 
//_newColours - New colour of node
{
	float _newColours[4][3];

	_newColours[0][0] = { _newColour1[0] };
	_newColours[0][1] = { _newColour1[1] };
	_newColours[0][2] = { _newColour1[2] };

	_newColours[1][0] = { _newColour2[0] };
	_newColours[1][1] = { _newColour2[1] };
	_newColours[1][2] = { _newColour2[2] };

	_newColours[2][0] = { _newColour3[0] };
	_newColours[2][1] = { _newColour3[1] };
	_newColours[2][2] = { _newColour3[2] };

	_newColours[3][0] = { _newColour4[0] };
	_newColours[3][1] = { _newColour4[1] };
	_newColours[3][2] = { _newColour4[2] };

	for (int x = 0; x < 3; x++)
	{
		if (nodeNums[x])
		{
			verticesTex[3 + (x * 8)] = _newColours[x][0];
			verticesTex[4 + (x * 8)] = _newColours[x][1];
			verticesTex[5 + (x * 8)] = _newColours[x][2];
		}
	}
}

// Function Name   : FlipTextures
// Description     : N/A
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
void CShapeTriangle::FlipTextures(bool horizontal, bool vertical)
{

}
 
// Function Name   : GetCardinalPoints
// Description     : Returns points in 2D space for collisions
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           :
std::vector<std::pair<int, int>> CShapeTriangle::GetCardinalPoints() //Get points used for collision detection
{
	std::vector<std::pair<int, int>> TempVec;
	return TempVec;
	/*std::vector<std::pair<int, int>> TempVec;

	float sinean = sin(getRotationAngle());
	float cosan = cos(getRotationAngle());

	float tempx = getShapePosition()[0];
	float tempy = getShapePosition()[1];

	float newXPoint = (getShapePosition()[0])*cosan - (sinean * (getShapePosition()[1] ));
	float newYPoint = (getShapePosition()[0] )*sinean + (cosan * (getShapePosition()[1] ));

	TempVec.push_back(std::make_pair(getShapePosition()[0], getShapePosition()[1])); //Centre
	TempVec.push_back(std::make_pair(newXPoint + tempx, newYPoint + tempy)); // Top End (Facing in original direction)

	newXPoint = (getShapePosition()[0] + getScalePosition()[0])*cosan - (sinean * (getShapePosition()[1] - getScalePosition()[1]));
	newYPoint = (getShapePosition()[0] + getScalePosition()[0])*sinean + (cosan * (getShapePosition()[1] - getScalePosition()[1]));

	return TempVec;*/
}

void CShapeTriangle::SwapTexture(std::string _tex)
{
}
