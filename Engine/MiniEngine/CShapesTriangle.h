#pragma once
#pragma once
#include <vector>
#include "CShapes.h"
class CShapeTriangle :public CShapes
{
public:
	CShapeTriangle();
	CShapeTriangle(std::pair<float, float> CentrePos, int xDist, int yDist, shaderFiles _myShader, bool Blending, std::string Texturepath = "\n");
	//CShapeSquare(std::pair<float, float> CentrePos, float newXSize, float newYSize, bool Textured, bool Blending);
	~CShapeTriangle();
	enum NodePos
	{
		NODE_TOP,
		NODE_BOTTOM_RIGHT,
		NODE_BOTTOM_LEFT
	};

	void shutDown();
	int GetIndicesSize();
	
	void PrepareGraphics(CCamera gameCam);
	void ChangeColour(float newColour[3]);
	void ChangeColourWithArray(bool nodeNums[4],  float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	std::vector<std::pair<int, int>> GetCardinalPoints();
	void FlipTextures(bool horizontal, bool vertical);

	

	void SwapTexture(std::string _tex);

private:

	float xSize;
	float ySize;

	GLfloat verticesTex[24]
	{
		//Position				Colour			    Tex co0rds
	0.0f,0.0f,0.0f,			0.5f, 0.5f, 0.2f,			0.0f,0.0f,//top - Left
	1.0f,-1.0f,0.0f,		0.f, 0.f, 0.0f,			    1.0f,1.0f,//bot - right
	1.0f, 1.0f,0.0f,		-0.0f, -.0f, -.0f,			0.0f,1.0f,//bot - left
	};

	int texWidth, texHieght; // texture params

	GLuint indices[3] =
	{
		0,1,2,
	};
};

