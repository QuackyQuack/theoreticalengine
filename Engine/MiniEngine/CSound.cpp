#include <iostream>
#include "CSound.h"

CSound * CSound::getInstance()
{
	static CSound* myInst;

	if (!myInst)
	{
		myInst = new CSound();

		//DeathMusic = addSound("Resources/Sounds/death_Music.wav",);
	}
	return(myInst);
}

CSound::CSound()
{
	FMOD_RESULT result;
	result = FMOD::System_Create(&audioSystem);
	if (result != FMOD_OK)
	{
		std::cout << "Fmod sytem failure";
	}
	
	result = audioSystem->init(100,FMOD_INIT_NORMAL | FMOD_INIT_3D_RIGHTHANDED,0);
	if (result != FMOD_OK)
	{
		std::cout << "Fmod sytem failure";
	}
}


CSound::~CSound()
{
	audioSystem->release();
}

FMOD::System * CSound::getAudioSys()
{
	return audioSystem;
}

FMOD::Sound* CSound::addSound(const char * _path, FMOD::Sound* mySound)
{
	FMOD_RESULT result = audioSystem->createSound(_path, FMOD_2D,0, &mySound);
	if (result != FMOD_OK)
	{
		std::cout << "Fmod sound Loading failed";
	}
	return(mySound);
}

void CSound::playSound(FMOD::Sound * mySound)
{
	//Does not work
	FMOD_RESULT result = audioSystem->playSound(mySound,0,false,0);
	if (result != FMOD_OK)
	{
		std::cout << "Fmod sound playing failed";
	}
}

void CSound::playSound(SoundChoice soundToPlay)
{
	switch (soundToPlay)
	{
	case(0):
		{
		DeathMusicTick = true;
		this->DeathMusic = this->addSound("Resources/Sounds/death_Music.wav", this->DeathMusic);
		playSound(DeathMusic);
		DeathMusic->setMode(FMOD_LOOP_NORMAL);
			break;
		}
	case(1):
	{
		EnemyAlarmedTick = true;
		this->EnemyAlarmed = this->addSound("Resources/Sounds/GhostAlarmed.mp3", this->EnemyAlarmed);

		playSound(EnemyAlarmed);
		break;
	}
	case(2):
	{
		EnemyDeathTick = true;
		this->EnemyDeath = this->addSound("Resources/Sounds/GhostDeath.mp3", this->EnemyDeath);

		playSound(EnemyDeath);
		break;
	}
	case(3):
	{
		EnemyHitTick = true;
		this->EnemyHit = this->addSound("Resources/Sounds/GhostHit.mp3", this->EnemyHit);

		playSound(EnemyHit);
		break;
	}
	case(4):
	{
		mainMusicTick = true;
		this->mainMusic = this->addSound("Resources/Sounds/main_Loop.wav", this->mainMusic);
		mainMusic->setMode(FMOD_LOOP_NORMAL);
		playSound(mainMusic);
	
		break;
	}
	case(5):
	{
		menuMusicTick = true;
		this->menuMusic = this->addSound("Resources/Sounds/mainMenu_Loop.wav", this->menuMusic);
		menuMusic->setMode(FMOD_LOOP_NORMAL);
		playSound(menuMusic);
		
		break;
	}
	case(6):
	{
		PlayerDeathTick = true;
		this->PlayerDeath = this->addSound("Resources/Sounds/PlayerDeath.mp3", this->PlayerDeath);

		playSound(PlayerDeath);
		break;
	}
	case(7):
	{
		PlayerShootTick = true;
		this->PlayerShoot = this->addSound("Resources/Sounds/PlayerShoot.mp3", this->PlayerShoot);

		playSound(PlayerShoot);
		break;
	}
	case(8):
	{
		StartingBellTick = true;
		this->StartingBell = this->addSound("Resources/Sounds/StartingBell.wav", this->StartingBell);

		playSound(StartingBell);
		break;
	}	
	case(9):
	{
		WallHitTick = true;
		this->WallHit = this->addSound("Resources/Sounds/WallHit.mp3", this->WallHit);
		playSound(WallHit);
		break;
	}
	default:break;
	}
}

void CSound::stopSound(SoundChoice soundToPlay)
{
	switch (soundToPlay)
	{
	case(0):
	{
		if (DeathMusicTick)
		{
			DeathMusicTick = false;
			DeathMusic->release();
		}
		break;
	}
	case(1):
	{
		if (EnemyAlarmedTick)
		{
			EnemyAlarmedTick = false;
			EnemyAlarmed->release();
		}
		break;
	}
	case(2):
	{
		if (EnemyDeathTick)
		{
			EnemyDeathTick = false;
			EnemyDeath->release();
		}
		break;
	}
	case(3):
	{
		if (EnemyHitTick)
		{
			EnemyHitTick = false;
			EnemyHit->release();
		}
		break;
	}
	case(4):
	{
		if (mainMusicTick)
		{
			mainMusicTick = false;
			mainMusic->release();
		}
		break;
	}
	case(5):
	{
		if (menuMusicTick)
		{
			menuMusicTick = false;
			menuMusic->release();
		}
		break;
	}
	case(6):
	{
		if (PlayerDeathTick)
		{
			PlayerDeathTick = false;
			PlayerDeath->release();
		}
		break;
	}
	case(7):
	{
		if (PlayerShootTick)
		{
			PlayerShootTick = false;
			PlayerShoot->release();
		}
		break;
	}
	case(8):
	{
		if (StartingBellTick)
		{
			StartingBellTick = false;
			StartingBell->release();
		}
		break;
	}
	case(9):
	{
		if (WallHitTick)
		{
			WallHitTick = false;
			WallHit->release();
		}
		break;
	}

	}
}

void CSound::destroySounds()
{
	if (DeathMusicTick)
	{
		DeathMusic->release();
	}
	if (EnemyAlarmedTick)
	{
		EnemyAlarmed->release();
	}
	if (EnemyDeathTick)
	{
		EnemyDeath->release();
	}
	if (EnemyHitTick)
	{
		EnemyHit->release();
	}
	if (mainMusicTick)
	{
		mainMusic->release();
	}
	if (menuMusicTick)
	{
		menuMusic->release();
	}
	if (PlayerDeathTick)
	{
		PlayerDeath->release();
	}
	if (PlayerShootTick)
	{
		PlayerShoot->release();
	}
	if (StartingBellTick)
	{
		StartingBell->release();
	}
	if (WallHitTick)
	{
		WallHit->release();
	}
}
