#include <fmod.hpp>
#include <vector>
#pragma once

enum SoundChoice
{
	DeathMusic,
	EnemyAlarmed,
	EnemyDeath,
	EnemyHit,
	mainLoop,
	mainMenu,
	playerDeath,
	playerShoot,
	StartingBell,
	WallHit,
};

class CSound
{
public:
	static CSound* getInstance();

	virtual ~CSound();

	FMOD::System* getAudioSys();

	FMOD::Sound* addSound(const char * _path,FMOD::Sound* mySound);
	void playSound(FMOD::Sound* mySound);
	void playSound(SoundChoice soundToPlay);
	void stopSound(SoundChoice soundToPlay);

	void destroySounds();

private:

	FMOD::Sound* DeathMusic;
	FMOD::Sound* EnemyAlarmed;
	FMOD::Sound* EnemyDeath;
	FMOD::Sound* EnemyHit;
	FMOD::Sound* mainMusic;
	FMOD::Sound* menuMusic;
	FMOD::Sound* PlayerDeath;
	FMOD::Sound* PlayerShoot;
	FMOD::Sound* StartingBell;
	FMOD::Sound* WallHit;

	//Sorry this is messy but its also 3am :)
	bool DeathMusicTick = false;
	bool EnemyAlarmedTick = false;
	bool EnemyDeathTick = false;
	bool EnemyHitTick = false;
	bool mainMusicTick = false;
	bool menuMusicTick = false;
	bool PlayerDeathTick = false;
	bool PlayerShootTick = false;
	bool StartingBellTick = false;
	bool WallHitTick = false;

	CSound();
	std::vector<FMOD::Sound*> SoundList;
	FMOD::System* audioSystem;
};