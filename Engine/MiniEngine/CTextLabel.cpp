#include "CTextLabel.h"
#include "CGameClass.h"
CTextLable::CTextLable()
{
}

CTextLable::CTextLable(std::string _text, std::string _font, glm::vec2 _pos, glm::vec3 _color, float _scale)
{

	text = (_text); 
	position = (_pos); 
	color = (_color);
	scale = (_scale);

	GLfloat halfwidth = (GLfloat)WINDOW_WIDTH * 0.5f;
	GLfloat halfhieght = (GLfloat)WINDOW_HIEGHT * 0.5f;

	proj = glm::ortho(-halfwidth, halfwidth, -halfhieght, halfhieght);
	//proj = CGameClass::getInstance().getOrthoProjections();
	program = ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/shad TextLabel.vs", "Resources/ShaderFiles/shad TextLabel.frg");

	FT_Library ft;
	FT_Face face;

	if (FT_Init_FreeType(&ft) != 0)
	{
		std::cout << "FreeType library failed     (TEXT LABEL)";
	}
	if (FT_New_Face(ft, _font.c_str(), 0, &face) != 0)
	{
		std::cout << "FreeType font loading failed    (TEXT LABEL)";
	}

	FT_Set_Pixel_Sizes(face, 0, 48); //If this fails, font is wrong

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (GLubyte character = 0; character < 128; character++)
	{
		if (FT_Load_Char(face, character, FT_LOAD_RENDER))
		{
			std::cout << "Freetype could not load glyph number" << character << "   (TEXT LABEL)";
			continue;
		}
		GLuint texture = GenerateTexture(face);

		FontChar fontChar = { texture, glm::ivec2(face->glyph->bitmap.width,face->glyph->bitmap.rows),glm::ivec2(face->glyph->bitmap_left,face->glyph->bitmap_top),(GLuint)face->glyph->advance.x };

		Characters.insert(std::pair<GLchar, FontChar>(character, fontChar));
	}

	//Destroy
	FT_Done_Face(face);
	FT_Done_FreeType(ft);


	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &(VBO));
	glBindBuffer(GL_ARRAY_BUFFER, (VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*6*4, NULL, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
}

CTextLable::~CTextLable()
{
	Characters.clear();
}

void CTextLable::Render()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(program);
	glUniform3f(glGetUniformLocation(program, "textColor"), color.x,color.y,color.z);
	glUniformMatrix4fv(glGetUniformLocation(program, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
	glUniform1f(glGetUniformLocation(program, "alpha"), alpha);
	glBindVertexArray(VAO);

	glm::vec2 textPos = position;

	for (std::string::const_iterator character = text.begin(); character != text.end(); character++)
	{
		FontChar fontChar = Characters[*character];
		GLfloat xpos = textPos.x + fontChar.Bearing.x * scale;
		GLfloat ypos = textPos.y - (fontChar.Size.y - fontChar.Bearing.y) * scale;

		GLfloat charWidth = fontChar.Size.x*scale;
		GLfloat charHieght = fontChar.Size.y*scale;

		GLfloat vertices[6][4] = { {xpos,ypos + charHieght,0.0,0.0}, {xpos,ypos,0.0,1.0 }, { xpos + charWidth,ypos,1.0,1.0 },
		{xpos,ypos + charHieght,0.0,0.0},{xpos + charWidth,ypos ,1.0,1.0},{xpos+charWidth,ypos + charHieght,1.0,0.0} };

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, fontChar.TextureID);
		glUniform1i(glGetUniformLocation(program, "tex"), 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		textPos.x += (fontChar.Advance >> 6) * scale; //bitwise shifting
	}

	glUseProgram(0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
}

void CTextLable::setText(std::string _text)
{
	text = _text;
}

void CTextLable::addToText(char newChar)
{
	if (newChar != '\b') //backSpace
	{
		text += newChar;
	}
	else
	{
		if (text.size() > 0)
		{
			text.pop_back();
		}
	}
}

void CTextLable::setColor(glm::vec3 _color)
{
	color = _color;
}

void CTextLable::setScale(GLfloat _scale)
{
	scale = _scale;
}

void CTextLable::setPosition(glm::vec2 _position)
{
	position = _position;
}

GLuint CTextLable::GenerateTexture(FT_Face face)
{
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RED,face->glyph->bitmap.width, face->glyph->bitmap.rows,0,GL_RED,GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return(texture); 
}

void CTextLable::setAlpha(float _alpha)
{
	alpha = _alpha;
}
float CTextLable::getAlpha()
{
	return(alpha);
}

std::string CTextLable::getText()
{
	return text;
}
