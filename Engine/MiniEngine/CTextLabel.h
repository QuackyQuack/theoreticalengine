#pragma once

//dependancies
#include <glew.h>
#include <freeglut.h>
#include <SOIL.h>
#include <glm.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

//Libraries
#include <map>
#include <string>
#include <iostream>

//Locals
#include "ShaderLoader.h"
#include "Tools.h"

struct FontChar
{
	GLuint TextureID;
	glm::ivec2 Size;
	glm::ivec2 Bearing;
	GLuint	  Advance;
};

class CTextLable
{
public:
	CTextLable();
	CTextLable(std::string _text, std::string _font, glm::vec2 _pos, glm::vec3 _color, float _scale = 1);
	~CTextLable();

	void Render();
	void setText(std::string _text);
	void addToText(char newChar);
	void setColor(glm::vec3 _color);
	void setScale(GLfloat _scale);
	void setPosition(glm::vec2 _position);

	void setAlpha(float _alpha);
	float getAlpha();
	std::string getText();

private:
	GLuint GenerateTexture(FT_Face face);

	std::string text;
	GLfloat scale;
	glm::vec3 color;
	glm::vec2 position;

	GLuint VAO, VBO, program;
	glm::mat4 proj;
	std::map<GLchar, FontChar> Characters;

	float alpha =  1.0f;

};