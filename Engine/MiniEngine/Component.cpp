#include "Component.h"

component::component()
{
}

component::~component()
{
}

void component::start()
{
}

void component::update()
{
}

void component::setMyGameObject(CGameObject* _target)
{
	myGameObject = _target;
}

CGameObject* component::getMyGameObject()
{
	return myGameObject;
}
