#pragma once
//#include "gameObject.h"

class CGameObject;

enum componentTypes
{
	PARENT,
	SPRITE,
	RIGIDODY

};

class component
{
public:
	component();
	~component();

	//Called when this object is initially loaded(and active in scene)
	virtual void start();
	virtual void update();

	void setMyGameObject(CGameObject* _target);
	CGameObject* getMyGameObject();

protected:

	//Who am I attached to?
	CGameObject* myGameObject = {nullptr};

};