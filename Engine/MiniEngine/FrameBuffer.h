#pragma once

#include <glew.h>
#include <freeglut.h>

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"

#include "CShapes.h"
#include "CShapeSquare.h"

class FrameBuffer
{
public:
	FrameBuffer();
	~FrameBuffer();



	void renderFirst();
	void renderSecond();

private:
	GLuint FramebufferName = 0;
	// The texture we're going to render to
	GLuint renderedTexture;
	// The depth buffer
	GLuint depthrenderbuffer;

	GLuint quad_vertexbuffer;

	GLuint quad_programID;
	GLuint texID;
	GLuint timeID;
};

