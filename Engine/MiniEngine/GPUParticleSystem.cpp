#include "GPUParticleSystem.h"
#include "ShaderLoader.h"


GPUParticleSystem::GPUParticleSystem()
{
}

GPUParticleSystem::GPUParticleSystem(glm::vec3 position, glm::vec3 scale, std::string texturePath, particleTypes _type)
	:myType(_type)
{
	setScalePosition(scale);
	setShapePosition(position);

	for (int x = 0; x < NUM_PARTICLES; x++)
	{
		initialPosition.push_back(glm::vec4(position, Tools::getRandNum() + 0.125f));
		initialVelocity.push_back(glm::vec4(0.25 * cos(x * .0167*0.5) + 0.25f * Tools::getRandNum() - 0.125f,
			2.0f+0.25f*Tools::getRandNum() - 0.125f,
			0.25 * sin(x * .0167*0.5) + 0.25f *Tools::getRandNum() - 0.125f
			, Tools::getRandNum() + 0.125f));
	}

	
	computeProgram = ShaderLoader::getInstance()->CreateComputeProgram("Resources/ShaderFiles/ComputeShaders/GPUParticle_waterFall.cmp");
	setMyProgram(ShaderLoader::getInstance()->CreateProgram(           "Resources/ShaderFiles/ComputeShaders/GPUParticle_waterFall.vs", "Resources/ShaderFiles/ComputeShaders/GPUParticle_waterFall.frg"));


	//Set up position VBO
	glGenBuffers(1, &posVBO);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, posVBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4) * initialPosition.size(), &initialPosition[0],
		GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, posVBO);

	glGenBuffers(1, &velVBO);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, velVBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4) * initialVelocity.size(), &initialVelocity[0],
		GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, velVBO);

	glGenBuffers(1, &initVelVBO);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, initVelVBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4) * initialVelocity.size(), &initialVelocity[0],
		GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, initVelVBO);

	glGenVertexArrays(1, &particleVAO);
	glBindVertexArray(particleVAO);
	


/*	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * particlePositions.size(), &particlePositions[0],
		GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), ((GLvoid*)0));
	glEnableVertexAttribArray(0);*/

	glBindBuffer(GL_ARRAY_BUFFER, posVBO);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE,NULL, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	setBlending(true);
}


GPUParticleSystem::~GPUParticleSystem()
{
}

void GPUParticleSystem::shutDown()
{
}

void GPUParticleSystem::SwapTexture(std::string _tex)
{
}

void GPUParticleSystem::ChangeColour(float newColour[3])
{
}

void GPUParticleSystem::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

int GPUParticleSystem::GetIndicesSize()
{
	return 0;
}

void GPUParticleSystem::PrepareGraphics(CCamera gameCam)
{
	glUseProgram(computeProgram);

	glDispatchCompute(NUM_PARTICLES / 128, 1, 1);

	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER,0);



	glUseProgram(getMyProgram());

	glm::vec3 change = glm::vec3(initialPosition[0].x, initialPosition[0].y, initialPosition[0].z);
	change -= shapePosition;

	glUniform3f(glGetUniformLocation(getMyProgram(), "change"), change.x, change.y, change.z);

	glm::mat4 vp = gameCam.getProjMatr() * gameCam.getViewMatr();
	glUniformMatrix4fv(glGetUniformLocation(getMyProgram(), "vp"),1,GL_FALSE,glm::value_ptr(vp));

	glBindBuffer(GL_ARRAY_BUFFER, posVBO);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, NULL, 0);
	glEnableVertexAttribArray(0);


	glDrawArrays(GL_POINTS, 0, NUM_PARTICLES);

	glDisableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);

}

void GPUParticleSystem::FlipTextures(bool horizontal, bool vertical)
{
}

std::vector<std::pair<int, int>> GPUParticleSystem::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
