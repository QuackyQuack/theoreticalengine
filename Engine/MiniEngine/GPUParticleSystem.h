#pragma once
#include "CShapes.h"
#include <vector>


class GPUParticleSystem :
	public CShapes
{
public:
	GPUParticleSystem();
	GPUParticleSystem(glm::vec3 position, glm::vec3 scale, std::string texturePath, particleTypes _type);
	
	~GPUParticleSystem();



	 void shutDown();
	 void SwapTexture(std::string _tex) ;
	 void ChangeColour(float newColour[3]) ;
	 void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	 int GetIndicesSize();
	 void PrepareGraphics(CCamera gameCam);
	 void FlipTextures(bool horizontal, bool vertical);
	 std::vector<std::pair<int, int>> GetCardinalPoints();
	


private:

	GLuint computeProgram;


	GLuint particleVAO;
	GLuint posVBO;
	GLuint velVBO;
	GLuint initVelVBO;

	std::vector<glm::vec4> initialPosition;
	std::vector<glm::vec4> initialVelocity;

	particleTypes myType;
};

