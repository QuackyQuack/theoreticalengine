#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
using namespace std;
// GL Includes

#include <glew.h>
#include <freeglut.h>
#include <SOIL.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <gtc/quaternion.hpp>
#include <gtx/quaternion.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "ShaderLoader.h"
#include "ModelMesh.h"
#include "CCamera.h"

class Model
{
public:
	
	GLuint program;
	CCamera* camera;
	
	/*  Functions   */
	// Constructor, expects a filepath to a 3D model.
	Model();

	Model(float xPos, float yPos, float zPos, float xScale, float yScale, float zScale, std::string path, CCamera* camera, bool _tessalated);

	// Draws the model, and thus all its meshes
	void Render();


	glm::vec3 getPos();
	glm::vec3 getRot();
	glm::vec3 getScale();
	float getRotAngle();

	void setScale(glm::vec3 _scale);
	void setPos(glm::vec3 _pos);
	void movePos(glm::vec3 _updatedPos);
	void rotateModel(glm::vec3 _axis, float _angle);

private:
	/*  Model Data  */
	vector<ModelMesh> meshes;
	string directory;
	vector<MeshTexture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.

	bool tesselated = true;

	glm::vec3 myPos;
	glm::vec3 myRot;
	float rotAngle;
	glm::vec3 myScale;

	/*  Functions   */
	// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
	void loadModel(string path);

	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene);

	ModelMesh processMesh(aiMesh* mesh, const aiScene* scene);

	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	vector<MeshTexture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);

	GLint TextureFromFile(const char* path, string directory);
};

