#include <map>
#include "ModelLoader.h"

ModelLoad::ModelLoad()
{

}

ModelLoad * ModelLoad::getInstance()
{
	static ModelLoad* inst;
	if (!inst)
	{
		inst = new ModelLoad();
	}
	return inst;
}

ModelLoad::~ModelLoad()
{
}

// Function Name   : addModel
// Description     : add model to the model map, so it can be loaded quicker
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
void ModelLoad::addModel(std::string _path, Model _model)
//_path  : the filepath of the model
//_model : the model   :-)
{
	modelMap.insert(std::make_pair(_path, _model));
}
// Function Name   : checkModelPath
// Description     : Checks if model is already in the model map, if so return the model to be used, else return nullptr
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
// Usage           : 
bool ModelLoad::checkModelPath(std::string _path)
{
	std::map<std::string,Model>::iterator it;
	for (it = modelMap.begin(); it != modelMap.end(); it++)
	{
		if (it->first == _path)
		{
			return(true);
			//return(it->second);
		}
	}
	return(false);
	//return NULL;
}

Model ModelLoad::getModel(std::string _path)
{
	std::map<std::string, Model>::iterator it;
	for (it = modelMap.begin(); it != modelMap.end(); it++)
	{
		if (it->first == _path)
		{
			return(it->second);
			//return(it->second);
		}
	}
	return(modelMap.begin()->second);
}
