//#include "CGameClass.h"
#include "Model.h"
#pragma once
class ModelLoad
{
public:
	static ModelLoad* getInstance();
	virtual ~ModelLoad();

	void addModel(std::string _path, Model _model);
	bool checkModelPath(std::string _path);
	Model getModel(std::string _path);

private:
	ModelLoad();

	std::map<std::string,Model> modelMap;
};
