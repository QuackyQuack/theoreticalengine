#version 450 core

out vec4 color;
in float outVSLifetime;

void main()
{
	color = vec4(0.0f,0.48f,0.58f, outVSLifetime);
} 