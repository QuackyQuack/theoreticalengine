#version 450 core

layout (location = 0) in vec4 position;

uniform mat4 vp;
uniform vec3 change;

out float outVSLifetime;

void main(void)
{
	// movement is in world space
	gl_Position = vp * vec4(position.xyz - change.xyz ,1.0f);
	outVSLifetime = position.w;
} 