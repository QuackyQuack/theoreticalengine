#version 450 core

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTexCoords;

out vec2 fragTexCoord;
out vec2 UV;

void main()
{
    gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0); 
    fragTexCoord = aTexCoords;
	UV = (aPos.xy+vec2(1,1))/2.0;
}  