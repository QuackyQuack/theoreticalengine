#version 450 core

in vec3 fragPos;
in vec3 fragColor;
in vec2 fragTexCoord;

in vec3 fragNormal;

out vec4 color;

uniform float TimeChange;
uniform sampler2D tex;

//Lighting
uniform float ambientStr   =0.09f;
uniform vec3 ambientColor  =vec3(1.0f,1.0f,1.0f);

uniform vec3 lightColor;
uniform vec3 lightPos;

uniform vec3 cameraPos;

uniform float lightSpecStr = 1.0f;
uniform float shininess = 256.f;

void main()
{
	//color = texture(tex,fragTexCoord);
	color = vec4(0.0,0.0,1.0,1.0);
}