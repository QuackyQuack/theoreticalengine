#version 450 core

//Works with fire gs and generic vs  particle shaders


in GS_FS_VERTEX{
vec2 texcoord;
}fs_in;

in float distToCentre;


uniform sampler2D Texture;
out vec4 color;

void main()
{
	

	//Pretty good
	//color =texture(Texture, vec2(fs_in.texcoord.x , fs_in.texcoord.y))* vec4( 0.5f/distToCentre +.5f , 0.3f,0.f,1.f);


	color =texture(Texture, vec2(fs_in.texcoord.x , fs_in.texcoord.y))* vec4( 0.8f, distToCentre /5.0f ,0.f,1.f);
} 