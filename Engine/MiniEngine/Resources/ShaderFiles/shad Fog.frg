#version 450 core

in vec3 fragColor;
in vec2 fragTexCoord;
in vec4 mWorldPos;

out vec4 color;

uniform float alpha;
uniform vec3 cameraPos;
uniform sampler2D tex;

uniform float fogStart;
uniform float fogRange;

void main()
{
	float dist = distance(mWorldPos.xyz, cameraPos);
	float lerp = (dist-fogStart)/fogRange;
	lerp = clamp(lerp,0.0f,1.0f);
	
	vec4 vFogColor = vec4 (0.5f,0.5f,0.5f,1.0f);

	

	color = texture(tex,fragTexCoord);
	color = mix(color,vFogColor,lerp);



	//color = vec4(fragColor,alpha);
}