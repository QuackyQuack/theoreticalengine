#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec2 texCoord;

out vec3 fragColor;
out vec2 fragTexCoord;
out vec4 mWorldPos;

uniform mat4 model;
uniform mat4 projecMatr;
uniform mat4 viewMatr;

uniform vec3 cameraPos;

uniform mat4 mvp;

void main()
{
	mWorldPos = model * vec4(position,1.0f);
	
	gl_Position = mvp * vec4(position, 1.0);

	fragColor = color;
	fragTexCoord = texCoord;

}