#version 450 core

layout (points) in; 
layout (triangle_strip, max_vertices = 3) out;

in VS_GS_VERTEX
{
	in vec4 position;
	in vec3 color;
	in mat4 mvp;
}gs_in[];

out vec3 fragColor;

uniform float alpha;
uniform mat4 ModView;
uniform mat4 ViewPro;


void main()
{
	fragColor = gs_in[0].color;


	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(-2.0f, 0.0f, 0.0f, 0.0f); EmitVertex(); 
	gl_Position = gs_in[0].position +  gs_in[0].mvp * vec4(2.0f, 0.0f, 0.0f, 0.0f);  EmitVertex();
	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(0.0f, 2.0f, 0.0f, 0.0f);  EmitVertex();
	EndPrimitive();

  /*mat4 MV = ModView;

  vec3 right = vec3(MV[0][0], 
                    MV[1][0], 
                    MV[2][0]);

  vec3 up = vec3(MV[0][1], 
                 MV[1][1], 
                 MV[2][1]);
  
  vec3 P = gl_in[0].gl_Position.xyz;

  mat4 VP = ViewPro;
 
  vec3 va = P - (right + up) * size;
  gl_Position = VP * vec4(va, 1.0);
  Vertex_UV = vec2(0.0, 0.0);
  Vertex_Color = vertex[0].color;
  EmitVertex();  
  
  vec3 vb = P - (right - up) * size;
  gl_Position = VP * vec4(vb, 1.0);
  //Vertex_UV = vec2(0.0, 1.0);
 // Vertex_Color = vertex[0].color;
  EmitVertex();  

  vec3 vd = P + (right - up) * size;
  gl_Position = VP * vec4(vd, 1.0);
 // Vertex_UV = vec2(1.0, 0.0);
 // Vertex_Color = vertex[0].color;
  EmitVertex();  

  vec3 vc = P + (right + up) * size;
  gl_Position = VP * vec4(vc, 1.0);
 // Vertex_UV = vec2(1.0, 1.0);
 // Vertex_Color = vertex[0].color;
  EmitVertex();  
  
  EndPrimitive();  */

}