#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;


out VS_GS_VERTEX
{
	out vec4 position;
	out vec3 color; 
	out mat4 mvp; 
} vs_out;

out vec3 fragColor;
out vec2 fragTexCoord;

uniform mat4 mvp;
uniform mat4 model;

void main()
{

	//gl_Position = model * vec4(position, 1.0f);
	//vs_out.color = color; 	
	//vs_out.position = gl_Position;
	//vs_out.mvp = mvp;


	// Old stuff
	gl_Position = mvp * vec4(position, 1.0f); 

	vs_out.color = color; 
		
	vs_out.position = gl_Position;
	vs_out.mvp = mvp;
}