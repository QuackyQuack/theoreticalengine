#version 450 core

layout (points) in; 
layout (triangle_strip, max_vertices = 9) out;

in VS_GS_VERTEX
{
	in vec4 position;
	in vec3 color;
	in mat4 mvp;
}gs_in[];

out vec3 fragColor;
out vec2 fragTexCoord;

uniform float alpha;

void main()
{
	fragColor = gs_in[0].color;


	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(0.25f, 0.75f, 0.00f, 0.0f); 
	fragTexCoord = vec2(1,1);
	EmitVertex(); 
	
	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(.250f, 0.0f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,1);
	EmitVertex(); 

	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(1.0f, 0.750f, 0.0f, 0.0f); 
	fragTexCoord = vec2(1,0);
	EmitVertex(); 
	 

	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(1.0f, .750f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,0);
 	EmitVertex(); 
	
	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(0f, .750f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,0);
 	EmitVertex(); 

	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(.750f, 0.0f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,0);
 	EmitVertex(); 


	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(.50f, 1.0f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,0);
 	EmitVertex(); 

	gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(.250f, .750f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,0);
 	EmitVertex(); 

		gl_Position = gs_in[0].position + gs_in[0].mvp * vec4(.750f, .750f, 0.0f, 0.0f);
	fragTexCoord = vec2(0,0);
 	EmitVertex(); 

	EndPrimitive();

}