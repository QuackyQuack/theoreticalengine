#version 450 core

in vec3 fragColor;
in vec2 fragTexCoord;

out vec4 color;

uniform float alpha;
uniform sampler2D tex;

void main()
{
	//color = vec4(fragColor,alpha);

	if(fragTexCoord == vec2(0,0))
	{
		color = vec4(0,0,1,1);
	}
	else
	{
		color = texture(tex,fragTexCoord);
	}
}