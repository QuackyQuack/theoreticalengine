#version 450 core

in vec3 fragPos;
in vec3 fragColor;
in vec2 fragTexCoord;
in vec3 fragNormal;

out vec4 color;

//Reflections
uniform vec3 camPos;
uniform sampler2D tex;
uniform samplerCube skyBox;

//Lighting
uniform float ambientStr   =0.05f;
uniform vec3 ambientColor  =vec3(1.0f,1.0f,1.0f);
uniform vec3 lightColor = vec3(1.0f,1.0f,1.0f);
uniform vec3 lightPos = vec3(0.0f,4.0f,3.0f);
uniform vec3 cameraPos;
uniform float lightSpecStr = 1.0f;
uniform float shininess = 256.f;

void main()
{
	vec3 norm = normalize(fragNormal);
	vec3 viewDir = normalize(fragPos-camPos);
	vec3 reflectDir = reflect(viewDir, norm);

	vec3 ambient = ambientStr * ambientColor;

	//Light direction
	
	vec3 lightDir = normalize(fragPos-lightPos);

	//Diffusing
	float diffuseStr = max(dot(norm,-lightDir),0.0f);
	vec3 diffuse = diffuseStr*lightColor;

	//Specular highlight
	vec3 negVeiwDir = normalize(cameraPos-fragPos);

	vec3 halfwayVec = normalize(-lightDir-negVeiwDir);

	//float spec = pow(max(dot(negVeiwDir,reflectDir),0.0f),shininess);

	float spec =  pow(max(dot(norm,halfwayVec),0.0f),shininess);
	vec3 specular = lightSpecStr * spec * lightColor;

	color = vec4(ambient + diffuse + specular,1.0f) * vec4(texture(skyBox,reflectDir).rgb,1.0f);

	//color = vec4(texture(skyBox,reflectDir).rgb,1.0f);
}