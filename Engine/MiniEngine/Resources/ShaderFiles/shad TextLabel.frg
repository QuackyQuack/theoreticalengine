#version 450 core

in vec2 fragTexCoord;

out vec4 color;

uniform vec3 textColor;
uniform sampler2D tex;
uniform float alpha;

void main()
{
    vec4 sampled = vec4(1.0f,1.0f,1.0f,texture(tex,fragTexCoord).r);
	color = vec4(textColor,alpha) * sampled;
}