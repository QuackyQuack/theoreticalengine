#version 450 core

in vec3 fragPos;
in vec3 fragColor;
in vec2 fragTexCoord;

in vec3 fragNormal;

out vec4 color;

uniform float TimeChange;
uniform sampler2D tex;

//Lighting
uniform float ambientStr   =0.09f;
uniform vec3 ambientColor  =vec3(1.0f,1.0f,1.0f);

uniform vec3 lightColor;
uniform vec3 lightPos;

uniform vec3 cameraPos;

uniform float lightSpecStr = 1.0f;
uniform float shininess = 256.f;

void main()
{
	//Ambient light
	vec3 ambient = ambientStr * ambientColor;

	//Light direction
	vec3 norm = normalize(fragNormal);
	vec3 lightDir = normalize(fragPos-lightPos);

	//Diffusing
	float diffuseStr = max(dot(norm,-lightDir),0.0f);
	vec3 diffuse = diffuseStr*lightColor;

	//Specular highlight
	vec3 negVeiwDir = normalize(cameraPos-fragPos);
//	vec3 reflectDir = reflect(lightDir,norm);

	vec3 halfwayVec = normalize(-lightDir-negVeiwDir);

	//float spec = pow(max(dot(negVeiwDir,reflectDir),0.0f),shininess);

	float spec =  pow(max(dot(norm,halfwayVec),0.0f),shininess);
	vec3 specular = lightSpecStr * spec * lightColor;


	//color = vec4(ambient + diffuse,1.0f) * (texture(tex,fragTexCoord));

	color = vec4(ambient + diffuse + specular,1.0f) * (texture(tex,fragTexCoord));
	
}