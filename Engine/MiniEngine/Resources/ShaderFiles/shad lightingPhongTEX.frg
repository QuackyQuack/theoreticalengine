#version 450 core

in vec3 fragPos;
in vec3 fragColor;
in vec2 fragTexCoord;

in vec3 fragNormal;

out vec4 color;

uniform float TimeChange;
uniform sampler2D tex;

//Lighting
uniform float ambientStr   =0.09f;
uniform vec3 ambientColor  =vec3(1.0f,1.0f,1.0f);

uniform vec3 lightColor = vec3(1.0f,1.0f,1.0f);
uniform vec3 lightPos = vec3(0.0f,4.0f,3.0f);

uniform vec3 cameraPos;

uniform float lightSpecStr = 1.0f;
uniform float shininess = 256.f;

//Testing
struct WorldLights
{
	float ambientStr;
	vec3 ambientColor;

	vec3 lightColor;
	vec3 lightPos;

	float lightSpecStr;
	float shininess;
};

uniform WorldLights worldLights[100];
uniform int totalLights;

void main()
{
	//Ambient light
	vec3 ambient = ambientStr * ambientColor;

	//Light direction
	vec3 norm = normalize(fragNormal);
	vec3 lightDir = normalize(fragPos-lightPos);

	//Diffusing
	float diffuseStr = max(dot(norm,-lightDir),0.0f);
	vec3 diffuse = diffuseStr*lightColor;

	//Specular highlight
	vec3 negVeiwDir = normalize(cameraPos-fragPos);
	vec3 reflectDir = reflect(lightDir,norm);

	float spec = pow(max(dot(negVeiwDir,reflectDir),0.0f),shininess);
	vec3 specular = lightSpecStr * spec * lightColor;

	vec3 ambient2 = vec3(0,0,0);
	vec3 norm2 = vec3(0,0,0);
	vec3 lightDir2 = vec3(0,0,0);
	float diffuseStr2 = 0.0;
	vec3 diffuse2 = vec3(0,0,0);
	vec3 negVeiwDir2 = vec3(0,0,0);
	vec3 reflectDir2 = vec3(0,0,0);
	float spec2 = 0.0;
	vec3 specular2 = vec3(0,0,0);

	for(int i = 0; i < totalLights; i++)
	{
		ambient2 += worldLights[i].ambientStr * worldLights[i].ambientColor;

			//Light direction
		norm2 += normalize(fragNormal);
		lightDir2 += normalize(fragPos-worldLights[i].lightPos);

			//Diffusing
		diffuseStr2 += max(dot(norm2,-lightDir2),0.0f);
		diffuse2 += diffuseStr2*worldLights[i].lightColor;

			//Specular highlight
		negVeiwDir2 += normalize(cameraPos-fragPos);
		reflectDir2 += reflect(lightDir2,norm2);

		spec2 += pow(max(dot(negVeiwDir2,reflectDir2),0.0f),worldLights[i].shininess);
		specular2 += worldLights[i].lightSpecStr * spec2 * worldLights[i].lightColor;
	}

	color = vec4(ambient +ambient2 + diffuse +diffuse2 + specular + specular2,1.0f) * (texture(tex,fragTexCoord));
	
}