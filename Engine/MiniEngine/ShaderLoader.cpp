#include<iostream>
#include<fstream>
#include<vector>

#include "ShaderLoader.h" 


ShaderLoader::ShaderLoader()
{}
ShaderLoader * ShaderLoader::getInstance()
{
	static ShaderLoader* myInst;

	if (!myInst)
	{
		myInst = new ShaderLoader();
	}
	return(myInst);
}
ShaderLoader::~ShaderLoader()
{
	//ShaderMap.clear();
	//ProgramMap.clear();
	//delete &ShaderLoader::getInstance();
}

GLuint ShaderLoader::CreateProgram(const char* vertexShaderFilename, const char* fragmentShaderFilename)
{
	//static std::map<GLuint, std::pair<const char*, const char*>> ProgramMap;

	std::map<GLuint, std::pair<const char*, const char*>>::iterator it;
	it = ProgramMap.begin();
	//Check if this program has already been loaded.
	while (it != ProgramMap.end())
	{
		if (it->second.first == vertexShaderFilename && it->second.second == fragmentShaderFilename)
		{
			//return the program ID
			return(it->first);
		}
		it++;
	}

	GLuint VertShader = CreateShader(GL_VERTEX_SHADER,vertexShaderFilename);
	GLuint FragShader = CreateShader(GL_FRAGMENT_SHADER,fragmentShaderFilename);

	GLuint program = glCreateProgram();

	glAttachShader(program, VertShader);
	glAttachShader(program, FragShader);

	glLinkProgram(program);

	// Check for link errors
	int link_result = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &link_result);
	if (link_result == GL_FALSE)
	{
		std::string programName = vertexShaderFilename + *fragmentShaderFilename;
		PrintErrorDetails(false, program, programName.c_str());
		return 0;
	}
	//Add program to map
	ProgramMap[program] = std::pair<const char*, const char*>(vertexShaderFilename, fragmentShaderFilename);

	return program;
}

GLuint ShaderLoader::CreateProgram(const char * vertexShaderFilename, const char * fragmentShaderFilename, const char * geometryShaderFileName)
{
	static std::map<GLuint, std::pair<const char*, std::pair<const char*, const char*>>> ProgramMap;

	std::map<GLuint, std::pair<const char*, std::pair<const char*, const char*>>>::iterator it;
	it = ProgramMap.begin();
	//Check if this program has already been loaded.
	while (it != ProgramMap.end())
	{
		if (it->second.first == vertexShaderFilename && it->second.second.first == fragmentShaderFilename && it->second.second.second == geometryShaderFileName)
		{
			//return the program ID
			return(it->first);
		}
		it++;
	}


	GLuint VertShader = CreateShader(GL_VERTEX_SHADER, vertexShaderFilename);
	GLuint FragShader = CreateShader(GL_FRAGMENT_SHADER, fragmentShaderFilename);
	GLuint GeoShader = CreateShader(GL_GEOMETRY_SHADER, geometryShaderFileName);

	GLuint program = glCreateProgram();

	glAttachShader(program, VertShader);
	glAttachShader(program, GeoShader);
	glAttachShader(program, FragShader);



	glLinkProgram(program);

	// Check for link errors
	int link_result = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &link_result);
	if (link_result == GL_FALSE)
	{
		std::string programName = vertexShaderFilename + *fragmentShaderFilename;
		PrintErrorDetails(false, program, programName.c_str());
		return 0;
	}
	//Add program to map
	std::pair<const char*, const char*> fragAndGeo = std::pair<const char*, const char*>(vertexShaderFilename, fragmentShaderFilename);
	ProgramMap[program] = std::pair<const char*, std::pair<const char*, const char*>>(vertexShaderFilename, fragAndGeo);

	return program;
}

GLuint ShaderLoader::CreateProgram(const char * vertexShaderFilename, const char * fragmentShaderFilename, const char * tessalationControl, const char * tessalationEval)
{
	struct mapStruct
	{
	public:
		mapStruct()
		{
		
		}
		mapStruct(const char * _vertex, const char * _fragmet, const char * _tessalationControl, const char * _tessalationEval)
		{
			vertex = _vertex;
			fragment = _fragmet;
			tessalationControl = _tessalationControl;
			tessalationEval = _tessalationEval;
		}



		const char * vertex;
		const char * fragment;
		const char * tessalationControl;
		const char * tessalationEval;

	};

	static std::map < GLuint, mapStruct> ProgramMap;

	std::map < GLuint, mapStruct>::iterator it;

	it = ProgramMap.begin();
	//Check if this program has already been loaded.
	while (it != ProgramMap.end())
	{
		if (it->second.vertex == vertexShaderFilename && it->second.fragment == fragmentShaderFilename && it->second.tessalationControl == tessalationControl && it->second.tessalationEval == tessalationEval)
		{
			//return the program ID
			return(it->first);
		}
		it++;
	}


	GLuint VertShader = CreateShader(GL_VERTEX_SHADER, vertexShaderFilename);
	GLuint FragShader = CreateShader(GL_FRAGMENT_SHADER, fragmentShaderFilename);
	//GLuint GeoShader = CreateShader(GL_GEOMETRY_SHADER, geometryShaderFileName);
	GLuint controlShader = ShaderLoader::getInstance()->CreateShader(GL_TESS_CONTROL_SHADER, "Resources/ShaderFiles/quadTesControl.tes");
	GLuint evalShader = ShaderLoader::getInstance()->CreateShader(GL_TESS_EVALUATION_SHADER, "Resources/ShaderFiles/quadTesEval.tes");


	GLuint program = glCreateProgram();

	glAttachShader(program, VertShader);
	
	glAttachShader(program, FragShader);

	glAttachShader(program, controlShader);
	glAttachShader(program, evalShader);

	glLinkProgram(program);

	// Check for link errors
	int link_result = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &link_result);

	if (link_result == GL_FALSE)
	{
		std::string programName = vertexShaderFilename + *fragmentShaderFilename;
		PrintErrorDetails(false, program, programName.c_str());
		return 0;
	}
	//Add program to map
	//std::pair<const char*, const char*> fragAndGeo = std::pair<const char*, const char*>(vertexShaderFilename, fragmentShaderFilename);
	mapStruct mem(vertexShaderFilename, vertexShaderFilename, tessalationControl, tessalationEval);

	ProgramMap[program] = (mem);

	return program;
}

GLuint ShaderLoader::CreateComputeProgram(const char * computeShaderFilename)
{
	std::map<GLuint, const char*>::iterator it;
	it = computeMap.begin();
	//Check if this program has already been loaded.
	while (it != computeMap.end())
	{
		if (it->second == computeShaderFilename)
		{
			//return the program ID
			return(it->first);
		}
		it++;
	}

	/*GLuint comp = glCreateShader(GL_COMPUTE_SHADER);
	glShaderSource(comp, 1, &computeShaderFilename, NULL);
	glCompileShader(comp);*/

	GLuint compShader = CreateShader(GL_COMPUTE_SHADER, computeShaderFilename);

	GLuint ray_program = glCreateProgram();
	glAttachShader(ray_program, compShader);
	glLinkProgram(ray_program);

	// Check for link errors
	int link_result = 0;
	glGetProgramiv(ray_program, GL_LINK_STATUS, &link_result);
	if (link_result == GL_FALSE)
	{
		std::string programName = computeShaderFilename ;
		PrintErrorDetails(false, ray_program, programName.c_str());
		return 0;
	}
	//Add program to map
	computeMap[ray_program] = computeShaderFilename;

	return ray_program;
}


GLuint ShaderLoader::CreateShader(GLenum shaderType, const char* shaderName)
{
	//static std::map<const char*, GLuint> ShaderMap; // Store all ID's here, paired with filepath.


	std::map<const char*, GLuint>::iterator it;
	it = ShaderMap.begin();
	//Check if this shader has already been loaded.
	while (it != ShaderMap.end())
	{
		if (it->first == shaderName)
		{
			return(it->second);
		}
		it++;
	}
	  
	//Create a placeholder ID
	GLuint myID = glCreateShader(shaderType);

	//Get the raw text of the file
	std::string shaderText;
	shaderText = ReadShaderFile(shaderName);
	
	//Check that this is the best solution

	//Convert to GUchar
	const char *ConvertedToGLchar = shaderText.c_str();
	//Populate the ID and ??bake??
	glShaderSource(myID, 1, &ConvertedToGLchar, NULL);
	glCompileShader(myID);

	//Add new shader to map
	ShaderMap.insert(std::pair<const char*, GLuint>(shaderName, myID));

	// Check for errors
	int compile_result = 0;
	glGetShaderiv(myID, GL_COMPILE_STATUS, &compile_result);
	if (compile_result == GL_FALSE)
	{
		PrintErrorDetails(true, myID, shaderName);
		return 0;
	}
	return myID;
}

std::string ShaderLoader::ReadShaderFile(const char *filename)
{
	// Open the file for reading
	std::ifstream file(filename, std::ios::in);
	std::string shaderCode;

	// Ensure the file is open and readable
	if (!file.good()) {
		std::cout << "Cannot read file:  " << filename << std::endl;
		return "";
	}

	// Determine the size of of the file in characters and resize the string variable to accomodate
	file.seekg(0, std::ios::end);
	shaderCode.resize((unsigned int)file.tellg());

	// Set the position of the next character to be read back to the beginning
	file.seekg(0, std::ios::beg);
	// Extract the contents of the file and store in the string variable
	file.read(&shaderCode[0], shaderCode.size());
	file.close();
	return shaderCode;
}

void ShaderLoader::PrintErrorDetails(bool isShader, GLuint id, const char* name)
{
	int infoLogLength = 0;
	// Retrieve the length of characters needed to contain the info log
	(isShader == true) ? glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogLength) : glGetProgramiv(id, GL_INFO_LOG_LENGTH, &infoLogLength);
	std::vector<char> log(infoLogLength);

	// Retrieve the log info and populate log variable
	(isShader == true) ? glGetShaderInfoLog(id, infoLogLength, NULL, &log[0]) : glGetProgramInfoLog(id, infoLogLength, NULL, &log[0]);		
	std::cout << "Error compiling " << ((isShader == true) ? "shader" : "program") << ": " << name << std::endl;
	std::cout << &log[0] << std::endl;
}

void ShaderLoader::ShutDown()
{
	ShaderMap.clear();
	ProgramMap.clear();
}
