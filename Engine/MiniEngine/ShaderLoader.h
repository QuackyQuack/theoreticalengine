#pragma once

// Dependency Includes
#include <glew.h>
#include <freeglut.h>
#include<map>

// Library Includes
#include <iostream>

class ShaderLoader
{
	
public:

	static ShaderLoader* getInstance();

	virtual ~ShaderLoader();

	GLuint CreateProgram(const char* VertexShaderFilename, const char* FragmentShaderFilename);

	GLuint CreateProgram(const char* VertexShaderFilename, const char* FragmentShaderFilename, const char* GeometruShaderFileName);

	GLuint CreateProgram(const char * vertexShaderFilename, const char * fragmentShaderFilename, const char * tessalationControl, const char * tessalationEval);

	GLuint CreateComputeProgram(const char * computeShaderFilename);

	GLuint CreateShader(GLenum shaderType, const char* shaderName);

	void ShutDown();

private:

	ShaderLoader();

	
	std::string ReadShaderFile(const char *filename);
	void PrintErrorDetails(bool isShader, GLuint id, const char* name);

	std::map<const char*, GLuint> ShaderMap;							// Store all ID's here, paired with filepath
	std::map<GLuint, std::pair<const char*, const char*>> ProgramMap;
	std::map<GLuint, const char*> computeMap;
};
