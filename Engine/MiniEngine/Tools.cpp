//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School.
//
// File Name       : tools.cpp
// Description     : useful functions for quality of life
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
#include "Tools.h"

/*
***************REMINDER OF THINGS TO BE ERROR MESSAGED ************************
SHADER LOADER
TEXTURE LOADING
ALL GETTER METHODS FOR SHAPE CLASSES

*/




Tools::Tools()
{
}


Tools::~Tools()
{
}


// Function Name   : ErrorMessage
// Description     : creates an error box displaying a user defined error
// Author		   : Joshua Dormer
// Mail            : joshua.dor7855@mediadesign.school.nz
//
void Tools::ErrorMessage(std::string errorMSG) //errorMSG: takes a L"" string type (const wchar_t* for the intelectuals) with the error message to display
{
	//MessageBox(NULL, errorMSG, NULL, NULL);
	std::cout << "*******ERROR********" << std::endl;
	std::cout << errorMSG.c_str() << std::endl;
}

const char * Tools::genericError() const throw()
{
	return "There was an error, please check the consol for details";
}

float Tools::getRandNum()
{
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

