#pragma once
#include <iostream>
#include <vector>
#include <map>

//#include <windows.h>

static float COLOUR_WHITE[3] = { 1.0f, 1.0f, 1.0f };
static float COLOUR_PURPLE[3] = { 1.0f, 0.0f, 1.0f };
static float COLOUR_BLACK[3] = { 0.0f, 0.0f, 0.0f };
static float COLOUR_ORANGE[3] = { 1.0f, 0.5f, 0.0f };
static float COLOUR_DARKBLUE[3] = { 0.137255f, 0.137255f, 0.556863f };
static float COLOUR_YELLOW[3] = { 1.0f, 1.0f, 0.0f };
static float COLOUR_TORCH[3] = { 0.8f, 0.8f, 0.4f };
static float COLOUR_DARKNESS[3] = { 0.0f, 0.0f, 0.05f };
static float COLOUR_SILVER[3] = {0.439f,0.502f,0.565f};

//Physics
const int Gravity = 1;
const int Friction = 1;

//Game
static const float GridSquareSize = 30;
static const int sizeOfGrid = 12;


static const float sizeOfTowerSprite = 15.0f;

//Networking consts

unsigned const DEFAULT_SERVER_PORT = 60012;
unsigned const DEFAULT_CLIENT_PORT = 60013;
unsigned int const MAX_MESSAGE_LENGTH = 256;
unsigned const MAX_ADDRESS_LENGTH = 32;


enum particleTypes
{
	PARTICLE_FOUNTAIN,
	PARTICLE_FIRE,
	PARTICLE_SMOKE,
};


class Tools
{
public:

	#define WINDOW_WIDTH   1600
	#define WINDOW_HIEGHT  800

	//Number of particles used in compute particle effects (GPGPU)
	#define NUM_PARTICLES 128 * 20000 

	Tools();
	~Tools();

	static void ErrorMessage(std::string errorMSG);
	virtual  const char*  genericError() const throw();
	static float getRandNum();

	//static std::map<unsigned char*, std::string> textureMap;


};

