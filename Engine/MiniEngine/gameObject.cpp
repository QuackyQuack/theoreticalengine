#include "gameObject.h"
#include "Tools.h"
#include "CGameClass.h"

CGameObject::CGameObject()
{
}

CGameObject::CGameObject(glm::vec3 _position, glm::vec3 _rotation, glm::vec3 _scale)
{
	objectPos = _position;
	objectScale = _scale;
	objectRot = _rotation;
}


CGameObject::~CGameObject()
{
}

void CGameObject::Update()
{
}

void CGameObject::shutdown()
{
	//CGameClass::getInstance().SafeDestroyShape(mySprite);
}




#pragma region PRS_Setters_and_Getters

void CGameObject::setPosition(float _x, float _y, float _z)
{
	prsChanged = true;
	objectPos = glm::vec3(_x, _y, _z);
}

void CGameObject::setPosition(glm::vec3 _new)
{
	prsChanged = true;
	objectPos = _new;
}

void CGameObject::changePosition(float _x, float _y, float _z)
{
	prsChanged = true;
	objectPos += glm::vec3(_x, _y, _z);
}

void CGameObject::changePosition(glm::vec3 _new)
{
	prsChanged = true;
	objectPos += _new;
}

glm::vec3 CGameObject::getPosition() const
{
	return objectPos;
}

void CGameObject::setRotation(float _x, float _y, float _z)
{
	prsChanged = true;
	objectRot = glm::vec3(_x, _y, _z);
}

void CGameObject::changeRotation(float _x, float _y, float _z)
{
	prsChanged = true;
	objectRot += glm::vec3(_x, _y, _z);
}

glm::vec3 CGameObject::getObjRot()
{
	return objectRot;
}


void CGameObject::setScale(float _x, float _y, float _z)
{
	prsChanged = true;
	objectScale = glm::vec3(_x, _y, _z);
}

void CGameObject::setScale(glm::vec3 _new)
{
	prsChanged = true;
	objectScale = _new;
}

void CGameObject::changeScale(float _x, float _y, float _z)
{
	prsChanged = true;
	objectScale += glm::vec3(_x, _y, _z);
}

void CGameObject::changeScale(glm::vec3 _scale)
{
	prsChanged = true;
	objectScale += _scale;
}

glm::vec3 CGameObject::getScale()
{
	return objectScale;
}

void CGameObject::addComponent(component * _newComponent)
{
	_newComponent->setMyGameObject(this);
	componentList.push_back(_newComponent);
}

bool CGameObject::removeComponent(component * _newComponent)
{
	for (int x = 0; x < componentList.size(); x++)
	{
		if (componentList[x] == _newComponent)
		{
			_newComponent->setMyGameObject(nullptr);
			componentList.erase(componentList.begin() + x);
			return true;
		}
	}
	return false;
}

void CGameObject::updateComponents()
{
	for (int x = 0; x < componentList.size(); x++)
	{
		componentList[x]->update();
	}
}

void CGameObject::initializeComponents()
{	
	for (int x = 0; x < componentList.size(); x++)
	{
		componentList[x]->start();
	}
}

bool CGameObject::isActive()
{
	return active;
}

bool CGameObject::isActive(bool _newVal)
{
	active = _newVal;
	return active;
}

bool CGameObject::getPRS_Changed()
{
	return prsChanged;
}




#pragma endregion