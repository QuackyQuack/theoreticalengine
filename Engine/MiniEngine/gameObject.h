#pragma once

#include <vector>

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"

#include "Component.h"
//class component;

class CGameObject
{
	public:
	CGameObject();
	CGameObject(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);
	~CGameObject();
	void Update();
	void shutdown();

	//Setters and getters for position, rotation and scale of gameObjects
#pragma region PRS_Setters_and_Getters

	void setPosition(float _x, float _y, float _z);
	void setPosition(glm::vec3);
	//changePosition: Alters the position of the sprite from the orginal
	void changePosition(float _x, float _y, float _z);
	void changePosition(glm::vec3);
	//Returns a vector to the position array
	glm::vec3 getPosition() const;

	void setRotation(float _x, float _y, float _z);
	//changePosition: Alters the rotation of the sprite from the original
	void changeRotation(float _x, float _y, float _z);
	//Returns object rotation
	glm::vec3 getObjRot();

	void setScale(float _x, float _y, float _z);
	void setScale(glm::vec3);
	//changeScale: Alters the scale of the sprite by
	void changeScale(float _x, float _y, float _z);
	void changeScale(glm::vec3 _scale);
	//Returns a vector to the scale array
	glm::vec3 getScale();

#pragma endregion

	void addComponent(component* _newComponent);
	//Returns true and removes target
	//else returns false.
	bool removeComponent(component* _newComponent);

	void updateComponents();
	void initializeComponents();

	bool isActive();
	bool isActive(bool _newVal);

	bool getPRS_Changed();

	protected:

	//If the object position/rotation/scale is changed, this is set to true
	bool prsChanged = { false };

	std::vector<component*> componentList;

	//Pos
	glm::vec3 objectPos = { 0,0,0 };
	//Rotation
	glm::vec3 objectRot = { 0,0,0 };
	//Scale
	glm::vec3 objectScale = { 0,0,0 };

	
	bool active = true;

};