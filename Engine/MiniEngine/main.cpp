#include <glew.h>
#include <freeglut.h>
#include <time.h>
#include <cassert>
#include <thread>
//#include <vld.h>

#include "CGameClass.h"
#include "Tools.h"
#include "CInput.h"

void Render();
void Update();
void KeyDown(unsigned char newKey, int x, int y);
void KeyUp(unsigned char newKey, int x, int y);
void MouseClicks(int button, int state, int x, int y);
void MousePassiveMove(int x, int y);
void MouseMove(int x, int y);
void ShutDown();

int main(int argc, char **argv)
{
	srand((unsigned)time(NULL));
	//Set up for glut
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE );
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HIEGHT);

	glutCreateWindow("Josh Dormer - Tech demo");
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Glew Initialition failed, all the world is failing" << std::endl;
		system("pause");
	}

	glClearColor(1.0, 0.0, 1.0, 1.0);

	CGameClass::getInstance().InitializeGame();

	//Anit-aliasing
	glutSetOption(GLUT_MULTISAMPLE, 8);
	glEnable(GL_MULTISAMPLE);

	glutDisplayFunc(Render);
	glutIdleFunc(Update);

	glutKeyboardFunc(KeyDown); //Input catcher object stored in game manager used for all game input
	glutKeyboardUpFunc(KeyUp);

	glutMouseFunc(MouseClicks);
	glutMotionFunc(MouseMove);
	glutPassiveMotionFunc(MousePassiveMove);
	glutCloseFunc(ShutDown);
	//glutSetCursor(GLUT_CURSOR_NONE); 

	//Enable 3D
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glutMainLoop();
	return(0);
}

void Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	CGameClass::getInstance().Render();
	glutSwapBuffers();
}

void Update()
{
	CGameClass::getInstance().Update();
	glutPostRedisplay();		//Flag for redraw
}

void KeyDown(unsigned char newKey, int x, int y)
{
	CInput::getInstance().KeyDown(newKey,  x,  y);
}

void KeyUp(unsigned char newKey, int x, int y)
{
	CInput::getInstance().KeyUp(newKey, x,y );
}

void MouseClicks(int button, int state, int x, int y)
{
	CInput::getInstance().MouseClicks(button, state, x, y);
}

void MousePassiveMove(int x, int y)
{
	CInput::getInstance().MousePassiveMove(x, y);
}

void MouseMove(int x, int y)
{
	CInput::getInstance().MouseMove(x, y);
}

void ShutDown()
{
	CGameClass::getInstance().shutdown();
}
