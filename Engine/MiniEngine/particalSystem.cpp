#include "particalSystem.h"
#include "particle.h"
#include "ShaderLoader.h"
#include "soil.h"
#include "CGameClass.h"

particalSystem::particalSystem()
{
}

particalSystem::particalSystem(int numberOfParticles, glm::vec3 position, glm::vec3 scale, std::string texturePath, particleTypes _type)
	:myType(_type)
{
	setScalePosition(scale);
	setShapePosition(position);

	numParticles = numberOfParticles;


	for (int x = 0; x < numberOfParticles; x++)
	{
		particle newParticle = particle(shapePosition, position, x, _type);
		particlePositions.push_back(position);
		particles.push_back(newParticle);
	}
	if (texturePath != "\n")
	{
		SetTexture(texturePath, texHieght, texWidth);
	}


	switch (_type)
	{
		case(PARTICLE_FIRE):
		{
			setShaderInformation(Shader_FireParticleSystem);
			break;
		}
		case(PARTICLE_FOUNTAIN):
		{
			setShaderInformation(Shader_GenericParticleSystem);
			break;
		}
	default: Tools::ErrorMessage("Trying to create a particle system without a defined shader for it's type!"); break;
	}

	//SetDrawMode(GL_POINTS);
	glGenTextures(1, &myTexture);
	glBindTexture(GL_TEXTURE_2D, myTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHieght, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetTexture());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(GetTexture());
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Set up our VBO
	glGenBuffers(1, &GetVertexAtrib(VAT_VBO));
	glBindBuffer(GL_ARRAY_BUFFER, GetVertexAtrib(VAT_VBO));
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * particlePositions.size(), &particlePositions[0],
		GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,  sizeof(glm::vec3), ((GLvoid*)0));
	glEnableVertexAttribArray(0);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	setBlending(true);

}
particalSystem::~particalSystem()
{
}

bool closerParticle(particle a, particle b)
{
	return(a.getDistToCamera() > b.getDistToCamera());
}


//Rendering
void particalSystem::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
	}
	//Texturing
	if (texture)
	{
		glUseProgram(getMyProgram());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, myTexture);
		glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);
	}
	//Alpha
	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());

	float dt = CGameClass::getInstance().getDeltaTime();

	//particles
	glm::vec3 cameraPos = CGameClass::getInstance().getGameCamPosition();
	for (int x = 0; x < numParticles; x++)
	{
		particlePositions[x]  = particles[x].particleUpdate(dt, cameraPos);
	}

	//Re order particles
	std::sort(particles.begin(), particles.end(), closerParticle);


	glm::vec3 Quad1, Quad2;
	glm::vec3 vView = gameCam.getCamLookDir();
	vView = glm::normalize(vView);

	Quad1 = glm::cross(vView, gameCam.getCamUp());
	Quad1 = glm::normalize(Quad1);
	Quad2 = glm::cross(vView, Quad1);
	Quad2 = glm::normalize(Quad2);

	glUseProgram(getMyProgram());

	glUniform3f(glGetUniformLocation(getMyProgram(), "vQuad1"), Quad1.x, Quad1.y, Quad1.z);
	glUniform3f(glGetUniformLocation(getMyProgram(), "vQuad2"), Quad2.x, Quad2.y, Quad2.z);
	glm::mat4 vp = CGameClass::getInstance().getGameCam().getProjMatr() * CGameClass::getInstance().getGameCam().getViewMatr();
	glUniformMatrix4fv(glGetUniformLocation(myProgram, "vp"), 1, GL_FALSE, glm::value_ptr(vp));
	//glUniformMatrix4fv(glGetUniformLocation(getMyProgram(), "vp"), 1, GL_FALSE,glm::value_ptr(vp));

	if (myType == (PARTICLE_FIRE))
	{
		glUniform3f(glGetUniformLocation(getMyProgram(), "centrePos"), shapePosition.x, shapePosition.y, shapePosition.z);
	}


	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * particlePositions.size(), &particlePositions[0], GL_STATIC_DRAW);
	
	glBindVertexArray(VAO);

	glDrawArrays(GL_POINTS, 0, numParticles);

	glBindVertexArray(0);
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

}


//Overloaded CShapes function
void particalSystem::shutDown()
{
}

void particalSystem::SwapTexture(std::string _tex)
{
}

void particalSystem::ChangeColour(float newColour[3])
{
}

void particalSystem::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

int particalSystem::GetIndicesSize()
{
	return 0;
}

void particalSystem::FlipTextures(bool horizontal, bool vertical)
{
}

std::vector<std::pair<int, int>> particalSystem::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
