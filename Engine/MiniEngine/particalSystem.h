#pragma once

#include <vector>

#include "CShapes.h"
//#include "particle.h"

//Foreward decl
class particle;

class particalSystem : public CShapes
{
public:
	particalSystem();
	particalSystem(int numberOfParticles, glm::vec3 position, glm::vec3 scale, std::string texturePath, particleTypes _type);
	~particalSystem();

	//Overloaded function
	void shutDown();
	void SwapTexture(std::string _tex);
	void ChangeColour(float newColour[3]) ;
	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	int GetIndicesSize();
	void PrepareGraphics(CCamera gameCam);
	void FlipTextures(bool horizontal, bool vertical);
	std::vector<std::pair<int, int>> GetCardinalPoints();


private:
	int numParticles = 0;
	std::vector<particle> particles;
	std::vector<glm::vec3> particlePositions;
	particleTypes myType;

	float tempval = 1.0f;
};

