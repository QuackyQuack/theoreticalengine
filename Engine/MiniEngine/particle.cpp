#include <algorithm>

#include "particle.h"
#include "Tools.h"


particle::particle()
{
}

//generic
particle::particle(glm::vec3 startingPoint, int _id, particleTypes _type)
	:originPoint(startingPoint), myID(_id), elapsedTime(static_cast <float> (rand()) / static_cast <float> (RAND_MAX) + 0.05f), myType(_type)
{
	switch (_type)
	{
	case(PARTICLE_FOUNTAIN):
	{
		currPoint = glm::vec3(0.25 * cos(_id * .0167) + 0.25f * Tools::getRandNum() - 0.125f, // vel
			2.0f + 0.25f * Tools::getRandNum() - 0.125f,
			0.25 * sin(_id* .0167) + 0.25f * Tools::getRandNum() - 0.125f);
		currPoint += originPoint;
		velocity = glm::vec3(0.f, 0.f, 0.f);
		break;
	}

	case(PARTICLE_SMOKE):
	{
		break;
	}
	default: Tools::ErrorMessage("Invalid particle constructor"); break;
	}

}

particle::particle(glm::vec3 startingPoint,glm::vec3 particleSystemPoint, int _id, particleTypes _type)
	:originPoint(startingPoint), myID(_id), elapsedTime(static_cast <float> (rand()) / static_cast <float> (RAND_MAX) + 0.05f), myType(_type), particleSystemPos(particleSystemPoint)
{
	switch (_type)
	{

	case(PARTICLE_FIRE):
	{
		currPoint = originPoint;
		velocity = glm::vec3(0.f, 2.f, 0.f);
		break;
	}
	case(PARTICLE_SMOKE):
	{
		break;
	}
	default: Tools::ErrorMessage("Invalid particle constructor"); break;
	}

}


particle::~particle()
{
}


float particle::getDistToCamera()
{
	return distToCamera;
}

glm::vec3 particle::particleUpdate(float dt, glm::vec3 camPos)
{
	
	switch (myType)
	{
	case(PARTICLE_FOUNTAIN):
	{
		return (fountainParticle(dt, camPos));
		break;
	}	
	case(PARTICLE_FIRE):
	{
		return (fireParticle(dt, camPos));
		break;
	}
	default:return glm::vec3(0,0,0);
	}
}


//Behaviour for particles in a fountain
glm::vec3 particle::fountainParticle(float dt, glm::vec3 camPos)
{
	velocity.y += -.2f * dt;
	currPoint += velocity;
	elapsedTime -= dt;

	if (elapsedTime <= 0.0f)
	{
		float xRand = Tools::getRandNum();
		float yRand = Tools::getRandNum();
		float zRand = Tools::getRandNum();

		currPoint = originPoint;

		velocity =
			glm::vec3(0.25 * cos(myID * .0167) + 0.25f * xRand - 0.125f,
				1.5f + 0.25f * yRand - 0.125f,
				0.25 * sin(myID * .0167) + 0.25f * zRand - 0.125f);

		velocity *= 0.18f;
		elapsedTime = Tools::getRandNum();
	}
	glm::vec3 dist = camPos - currPoint;
	distToCamera = sqrt(pow(dist.x, 2) + pow(dist.y, 2) + pow(dist.z, 2));

	return(currPoint);
}

glm::vec3 particle::fireParticle(float dt, glm::vec3 camPos)
{
	currPoint += velocity;
	elapsedTime -= dt;

	if (elapsedTime <= 0.0f)
	{
		xSin = Tools::getRandNum() ;
		ySpeed = Tools::getRandNum();
		zSin = Tools::getRandNum();
		myAmplitude = Tools::getRandNum();

		currPoint = originPoint;

		velocity = glm::vec3(0,0.5, 0 );
		velocity *= 0.18f;
		
		elapsedTime = Tools::getRandNum();
	}

	/*velocity =
		glm::vec3( sin(2*3.14 *dt ),
			0.001,
			0);*/
	//

	currPoint.x = originPoint.x + (myAmplitude * sin(2 * 3.14 *elapsedTime + (xSin*20)));
	currPoint.z = originPoint.z + (myAmplitude * sin(2 * 3.14 *elapsedTime + (zSin * 20)));

	glm::vec3 dist = camPos - currPoint;
	distToCamera = sqrt(pow(dist.x, 2) + pow(dist.y, 2) + pow(dist.z, 2));
	return glm::vec3(currPoint);
}
