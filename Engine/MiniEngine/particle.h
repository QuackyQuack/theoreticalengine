#pragma once
#include <glm.hpp>
#include <functional>

#include "Tools.h"


class particle
{
public:
	particle();
	particle(glm::vec3 startingPoint, int _id, particleTypes _type);
	particle::particle(glm::vec3 startingPoint, glm::vec3 particleSystemPoint, int _id, particleTypes _type);
	~particle();

	//Update function called from the particle system
	glm::vec3 particleUpdate(float dt,glm::vec3 camPos);

	float getDistToCamera();

private:
	particleTypes myType;

	float elapsedTime ;
	int myID;
	glm::vec3 originPoint;
	glm::vec3 currPoint;
	glm::vec3 velocity;

	//Position of the paricle system
	//Normaly usefull for shaders that change depending on distance from centre
	glm::vec3 particleSystemPos;

	float distToCamera = 0.0f;

	//Used by the fire particle to find when at half life
	float totalLifetime = 0.0f;
	//Also used by fire for my direction calculations
	float xSin = 0;
	float ySpeed = 0;
	float zSin = 0;
	float myAmplitude = 0;

	//Type of particle behaviour
	glm::vec3 particle::fountainParticle(float dt, glm::vec3 camPos);
	glm::vec3 particle::fireParticle(float dt, glm::vec3 camPos);

};

