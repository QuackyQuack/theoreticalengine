//#include "glm.hpp"

#include "playerLogic.h"
#include "gameObject.h"
#include "CInput.h"
#include "CGameClass.h"


//Constructors
Player::Player()
{
}



Player::~Player()
{
}


//Component overloads
void Player::start()
{
	//myGameObject->setPosition(spawnLocation);
	//myGameObject->setRotation(0, 0, 0);

	//CGameClass::getInstance().getGameCam().setCameraAxis(0, 0, 1);
}

void Player::update()
{
	CCamera& camera = CGameClass::getInstance().getGameCam();
	float deltaTime = CGameClass::getInstance().getDeltaTime();

	//PlayerMovements
	if (CInput::getInstance().KeyState('w') == DOWN)
	{
		glm::vec3 camLookDir = camera.getCamLookDir();
		camLookDir *= playerWalkSpeed * deltaTime;
		myGameObject->changePosition(camLookDir.x, 0, camLookDir.z);
	}
	if (CInput::getInstance().KeyState('s') == DOWN)
	{
		glm::vec3 camLookDir = camera.getCamLookDir();
		camLookDir *= -playerWalkSpeed * deltaTime;
		myGameObject->changePosition(camLookDir.x, 0, camLookDir.z);
	}
	if (CInput::getInstance().KeyState('d') == DOWN)
	{
		//Get the camera's right component assuming camera up is always 0,1,0
		glm::vec3 camRight = glm::cross(camera.getCamLookDir(), glm::vec3(0, 1, 0));
		camRight *= playerWalkSpeed * deltaTime;
		myGameObject->changePosition(camRight.x, 0, camRight.z);
	}
	if (CInput::getInstance().KeyState('a') == DOWN)
	{
		//Get the camera's right component assuming camera up is always 0,1,0
		glm::vec3 camRight = glm::cross(camera.getCamLookDir(), glm::vec3(0, 1, 0));
		camRight *= -playerWalkSpeed * deltaTime;
		myGameObject->changePosition(camRight.x, 0, camRight.z);
	}

	glm::vec3 position = myGameObject->getPosition();

	bool outOfRange = false;
	float yPos = CGameClass::getInstance().getWalkableTerrain()->getHeight(position.x, position.z, outOfRange);
	yPos += playerHeight;
	if (!outOfRange)
	{
		myGameObject->setPosition(position.x, yPos, position.z);
	}

	camera.RotateCamera(
		CInput::getInstance().getMousePos().second * cameraSensitivity,  // X
		-CInput::getInstance().getMousePos().first * cameraSensitivity,   // Y
		CInput::getInstance().getMousePos().second * cameraSensitivity   // Z
	);
	glutWarpPointer(WINDOW_WIDTH / 2, WINDOW_HIEGHT / 2);

	camera.setCameraPos(myGameObject->getPosition());
}
