#pragma once

#include "Component.h"
#include "glm.hpp"

class Player : public component
{
public:
	//Constructor
	Player();

	~Player();

	//Component overides
	void start();
	void update();

private:

	float playerWalkSpeed = 5.0f;

	float playerHeight = 4.0f;

	float cameraSensitivity = 0.004f;
};