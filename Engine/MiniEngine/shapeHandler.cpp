#include <iostream>

#include "shapeHandler.h"
#include "CShapeCube.h"


shapeHandler::shapeHandler()
{
	//Initilise all the shapes to be stored;
	shapeHolder["villager1"] = new CShapeCube(glm::vec3(0.1f,0.2f,0.3f));
}


shapeHandler::~shapeHandler()
{
}
shapeHandler& shapeHandler::getInstance()
{
	static shapeHandler myInstance;
	return(myInstance);
}

//Fetches the shape based off of the ID
CShapes * shapeHandler::getShape(std::string shapeID)
{

	std::map< std::string, CShapes* >::iterator it;
	it = shapeHolder.begin();

	while (it != shapeHolder.end())
	{
		if (it->first == shapeID)
		{
			return it->second;
		}
		it++;
	}


	Tools::ErrorMessage("Shape ID: " + shapeID + " not found, ensure shapeID has been entered is typed correctly.  ");
	
	return nullptr;
}

void shapeHandler::shutDown()
{
	std::map< std::string, CShapes* >::iterator it;

	it = shapeHolder.begin();
	while (it != shapeHolder.end())
	{
		delete it->second;
		it++;
	}
	shapeHolder.clear();
}
