#pragma once
#include <iostream>
#include <map>

#include "CShapes.h"
class shapeHandler
{
public:
	~shapeHandler();
	static shapeHandler& getInstance();

	CShapes* getShape(std::string shapeID);

	void shutDown();

private:
	shapeHandler();

	std::map< std::string, CShapes* > shapeHolder;


};