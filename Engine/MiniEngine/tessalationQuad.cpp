#include "tessalationQuad.h"
#include "ShaderLoader.h"
#include "CGameClass.h"

tessalationQuad::tessalationQuad()
{
}

tessalationQuad::tessalationQuad(std::pair<float, float> CentrePos, float newXSize, float newYSize, shaderFiles _myShader)
{
	
	GLfloat points[] =
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0, 1.0, 0.0f
	};
	setShapePosition(glm::vec3(CentrePos.first, CentrePos.second,0));

	setScalePosition(glm::vec3(newXSize, newYSize,0));
	
	glPatchParameteri(GL_PATCH_VERTICES, 4); //comment for tri patch

	//setShaderInformation(Shader_PureVertex);
	setMyProgram(ShaderLoader::getInstance()->CreateProgram("Resources/ShaderFiles/Tessalation_Basic.vs", "Resources/ShaderFiles/Tessalation_Basic.frg","Resources/ShaderFiles/quadTesControl.tes", "Resources/ShaderFiles/quadTesEval.tes"));

	tesselated = true;
	

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
	glBindVertexArray(0);
}


tessalationQuad::~tessalationQuad()
{
}

void tessalationQuad::shutDown()
{
}

void tessalationQuad::SwapTexture(std::string _tex)
{
}

void tessalationQuad::ChangeColour(float newColour[3])
{
}

void tessalationQuad::ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3])
{
}

int tessalationQuad::GetIndicesSize()
{
	return 0;
}

void tessalationQuad::PrepareGraphics(CCamera gameCam)
{
	matriceUpdates(gameCam);

	if (isBlending())
	{
		glEnable(GL_BLEND);
		glBlendFunc(getBlendingOptions().first, getBlendingOptions().second);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	}

	glUseProgram(getMyProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, myTexture);
	glUniform1i(glGetUniformLocation(getMyProgram(), "tex"), 0);
	glUniform1i(glGetUniformLocation(getMyProgram(), "dist"), glm::distance(CGameClass::getInstance().getGameCamPosition(),shapePosition) );
	//std::cout << glm::distance(CGameClass::getInstance().getGameCamPosition(), shapePosition) << std::endl;
	glUniform1f(glGetUniformLocation(getMyProgram(), "alpha"), getAlpha());
}

void tessalationQuad::FlipTextures(bool horizontal, bool vertical)
{
}

std::vector<std::pair<int, int>> tessalationQuad::GetCardinalPoints()
{
	return std::vector<std::pair<int, int>>();
}
