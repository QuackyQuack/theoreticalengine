#pragma once
#include "CShapes.h"
class tessalationQuad :
	public CShapes
{
public:
	tessalationQuad();
	tessalationQuad(std::pair<float, float> CentrePos, float newXSize, float newYSize, shaderFiles _myShader);
	~tessalationQuad();

	void shutDown();
	void SwapTexture(std::string _tex);
	void ChangeColour(float newColour[3]);
	void ChangeColourWithArray(bool nodeNums[4], float _newColour1[3], float _newColour2[3], float _newColour3[3], float _newColour4[3]);
	int GetIndicesSize();
	void PrepareGraphics(CCamera gameCam);
	void FlipTextures(bool horizontal, bool vertical);
	std::vector<std::pair<int, int>> GetCardinalPoints();

private:



};

